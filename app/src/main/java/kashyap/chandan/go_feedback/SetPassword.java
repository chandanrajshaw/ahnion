package kashyap.chandan.go_feedback;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.go_feedback.MainActivity;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.ResetPasswordResponse;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class SetPassword extends AppCompatActivity implements View.OnClickListener {
TextView setPwd;
Intent intent;
String id;
ImageView iv_back;
TextInputEditText et_Con_new_pass,et_new_pass;
Dialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);
        init();
        intent=getIntent();
        id=intent.getStringExtra("id");
        setPwd.setOnClickListener(this);
    }

    private void init() {
        et_Con_new_pass=findViewById(R.id.et_Con_new_pass);
        setPwd=findViewById(R.id.setPwd);
        et_new_pass=findViewById(R.id.et_new_pass);
        iv_back=findViewById(R.id.iv_back);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.setPwd:
               setSetPwd();
                break;
            case R.id.iv_back:
               finish();
                break;
        }
    }
    private void setSetPwd()
    {
        String new_pass=et_new_pass.getText().toString();
        String con_pass=et_Con_new_pass.getText().toString();
        if (new_pass.isEmpty()&&con_pass.isEmpty())
            Toast.makeText(this, "Fill All Fields", Toast.LENGTH_SHORT).show();
        else if (new_pass.isEmpty())
            Toast.makeText(this, "Enter New Pin", Toast.LENGTH_SHORT).show();
        else if (con_pass.isEmpty())
            Toast.makeText(this, "Re-Enter New Pin", Toast.LENGTH_SHORT).show();
        else if (!new_pass.equalsIgnoreCase(con_pass))
            Toast.makeText(this, "Re-check Pin You entered", Toast.LENGTH_SHORT).show();
        else
        {
            progressDialog=new Dialog(SetPassword.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Call<ResetPasswordResponse>call=apiInterface.resetPassword(id,con_pass);
            call.enqueue(new Callback<ResetPasswordResponse>() {
                @Override
                public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                    if (response.code()==200)
                    {
                        progressDialog.dismiss();
                        Toast.makeText(SetPassword.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(SetPassword.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                    else
                    {
                        progressDialog.dismiss();
                        Converter<ResponseBody, ApiError> converter =
                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            Toast.makeText(SetPassword.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
progressDialog.dismiss();
                    Toast.makeText(SetPassword.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}