package kashyap.chandan.go_feedback;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.ResponseClasses.DeleteImageResponse;
import kashyap.chandan.go_feedback.customerPannel.ContactUs;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ImageAdaptercross extends RecyclerView.Adapter<ImageAdaptercross.MyViewHolder> {
    Context context;
    List<String> images=new ArrayList<>();
    Dialog progressDialog;
    SharedPreferenceData sharedPreferenceData;
    public ImageAdaptercross(Context context, List<String> images) {
        this.context=context;
        this.images=images;
        progressDialog=new Dialog(context);
        sharedPreferenceData=new SharedPreferenceData(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.rec_image,parent,false));
    }
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
    Picasso.get().load(ApiClient.IMAGE_URL+"uploads/"+images.get(position)).placeholder(R.drawable.loading).error(R.drawable.terms).into(holder.imageView);
    System.out.println(images.get(position));
    holder.imageView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final Dialog dialog=new Dialog(context);
            dialog.setContentView(R.layout.image_dialog);

            dialog.setCancelable(false);
            Activity activity=(Activity)context;
            if (!activity.isFinishing())
                dialog.show();
            ImageView imageView=dialog.findViewById(R.id.imageView);
            ImageView close=dialog.findViewById(R.id.close);
            Picasso.get().load(ApiClient.IMAGE_URL+"uploads/"+images.get(position)).placeholder(R.drawable.loading).error(R.drawable.terms).into(imageView);
            DisplayMetrics metrics=context.getResources().getDisplayMetrics();
            int width=metrics.widthPixels;
            dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Window window = dialog.getWindow();
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
    });
    holder.remove.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Call<DeleteImageResponse>call=apiInterface.deleteImage(sharedPreferenceData.getId(),String.valueOf(holder.getAdapterPosition()));
            call.enqueue(new Callback<DeleteImageResponse>() {
                @Override
                public void onResponse(Call<DeleteImageResponse> call, Response<DeleteImageResponse> response) {
                    if (response.code()==200)
                    {
                        progressDialog.dismiss();
                        images.remove(holder.getAdapterPosition());
                        notifyDataSetChanged();
                    }
                    else
                    {
                        progressDialog.dismiss();
                        Converter<ResponseBody, ApiError> converter =
                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<DeleteImageResponse> call, Throwable t) {

                }
            });
        }
    });
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView,remove;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            remove=itemView.findViewById(R.id.remove);
remove.setVisibility(View.GONE);
            imageView=itemView.findViewById(R.id.imageView);
        }
    }
}
