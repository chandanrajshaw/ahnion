package kashyap.chandan.go_feedback;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.go_feedback.ResponseClasses.ForgetPasswordResponse;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ForgetPassword extends AppCompatActivity implements View.OnClickListener {
TextView tvVerify;
TextInputEditText et_email_phone;
    Dialog progressDialog;
    ImageView iv_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        init();
        tvVerify.setOnClickListener(this);
    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        tvVerify=findViewById(R.id.tvVerify);
        et_email_phone=findViewById(R.id.et_email_phone);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.tvVerify :
               forgetPassword();
               break;
            case R.id.iv_back :
               finish();
                break;

        }
    }
    private void forgetPassword()
    {
        final String id=et_email_phone.getText().toString();
        if (id.isEmpty())
            Toast.makeText(this, "Enter Email/Phone", Toast.LENGTH_SHORT).show();
        else
        {
            progressDialog=new Dialog(ForgetPassword.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Call<ForgetPasswordResponse>call=apiInterface.forgetPassword(id);
            call.enqueue(new Callback<ForgetPasswordResponse>() {
                @Override
                public void onResponse(Call<ForgetPasswordResponse> call, Response<ForgetPasswordResponse> response) {
                    if (response.code()==200)
                    {
                       progressDialog.dismiss();
                        Toast.makeText(ForgetPassword.this, "Otp has been Sent to your Email Address", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(ForgetPassword.this,OtpAuthentication.class);
                        intent.putExtra("flow","forgetPassword");
                        intent.putExtra("id",String.valueOf(response.body().getUser_id()));
                        startActivity(intent);
                    }
                    else
                    {
                        progressDialog.dismiss();
                        Converter<ResponseBody, ApiError> converter =
                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            Toast.makeText(ForgetPassword.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<ForgetPasswordResponse> call, Throwable t) {

                }
            });
        }

    }
}