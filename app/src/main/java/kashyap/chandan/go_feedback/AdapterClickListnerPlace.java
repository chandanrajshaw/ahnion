package kashyap.chandan.go_feedback;

import android.view.View;

import com.google.android.libraries.places.api.model.Place;

import kashyap.chandan.go_feedback.customerPannel.maps.Result;

public interface AdapterClickListnerPlace {
    public void onClicked(View view, Place.Field place);
}
