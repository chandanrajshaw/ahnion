package kashyap.chandan.go_feedback;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKey;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class SharedPreferenceData {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    public SharedPreferenceData(Context context) {
        this.context = context;
        try {
            MasterKey masterKey=new MasterKey.Builder(context)
                    .setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build();
            this.sharedPreferences= EncryptedSharedPreferences.create(
                    context,
                    "Login",
                    masterKey,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM

            );
        } catch (GeneralSecurityException e) { e.printStackTrace(); } catch (IOException e) { e.printStackTrace(); }
//        this.sharedPreferences= this.context.getSharedPreferences("Login",MODE_PRIVATE);
        this.editor=sharedPreferences.edit();

    }
    public void putSharedPreference(String id,String first_name,String last_name,String phone,String role,String email,String image,String date)
    {
        editor.putString("id",id);
        editor.putString("fName",first_name);
        editor.putString("lName",last_name);
        editor.putString("phone",phone);
        editor.putString("role",role);
        editor.putString("email",email);
        editor.putString("image",image);
        editor.putString("date",date);
        editor.commit();
    }
    public void sessionEnd()
    {
        editor.clear();
        editor.commit();
    }
    public String getDate()
    {
        String date=sharedPreferences.getString("date",null);
        return date ;
    }
    public String getId()
    {
        String id=sharedPreferences.getString("id",null);
        return id ;
    }
    public String getImage()
    {
        String image=sharedPreferences.getString("image",null);
        return image ;
    }
    public String getEmail()
    {
        String email=sharedPreferences.getString("email",null);
        return email ;
    }
    public String getRole()
    {
        String role=sharedPreferences.getString("role",null);
        return role ;
    }
    public String getPhone()
    {
        String phone=sharedPreferences.getString("phone",null);
        return phone ;
    }
    public String getFName()
    {
        String fname=sharedPreferences.getString("fName",null);
        return fname ;
    }
    public String getLName()
    {
        String lName=sharedPreferences.getString("lName",null);
        return lName ;
    }
}
