package kashyap.chandan.go_feedback.EnterprizePannel.enterprizeFragment;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PublishCountDataItem;
import kashyap.chandan.go_feedback.EnterprizePannel.MyPurchaseAdapter;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.MyPurchaseDataItem;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.MyPurchaseResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.countresponses.EnterPrizePurchaseCountResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.countresponses.EnterprizeCountDataItem;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class MyOrderFragment extends Fragment {
    SharedPreferenceData sharedPreferenceData;
    RecyclerView recyclerview;
    Dialog progressDialog;
    List<EnterprizeCountDataItem> countDataItems=new ArrayList<>();
    EnterPrizePurchaseAdapter purchaseAdapter;
    EditText search_location_box;
    List<EnterprizeCountDataItem> myList;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_orders,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        recyclerview.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
myAllPurchase();
search_location_box.addTextChangedListener(new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        String userInput=charSequence.toString();
        myList=new ArrayList<>();
        for (EnterprizeCountDataItem data:countDataItems)
        {
            if (data.getName().toLowerCase().startsWith(userInput))
            {
               myList.add(data);
            }
        }
        purchaseAdapter.updateList(myList);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
});
    }
    private void init(View view) {
        sharedPreferenceData=new SharedPreferenceData(getContext());
        recyclerview=view.findViewById(R.id.enterprizeFeedbackrecycler);
        progressDialog=new Dialog(getActivity());
        search_location_box=view.findViewById(R.id.search_location_box);
    }
    private  void myAllPurchase()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<EnterPrizePurchaseCountResponse> call=apiInterface.purchasecount(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<EnterPrizePurchaseCountResponse>() {
                    @Override
                    public void onResponse(Call<EnterPrizePurchaseCountResponse> call, Response<EnterPrizePurchaseCountResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            countDataItems=response.body().getData();
                            purchaseAdapter=new EnterPrizePurchaseAdapter(getActivity(),countDataItems);
                            recyclerview.setAdapter(purchaseAdapter);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<EnterPrizePurchaseCountResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}
