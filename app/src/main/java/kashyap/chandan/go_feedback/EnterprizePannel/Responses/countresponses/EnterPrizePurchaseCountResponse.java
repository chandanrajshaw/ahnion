package kashyap.chandan.go_feedback.EnterprizePannel.Responses.countresponses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class EnterPrizePurchaseCountResponse{

	@SerializedName("data")
	private List<EnterprizeCountDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<EnterprizeCountDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}