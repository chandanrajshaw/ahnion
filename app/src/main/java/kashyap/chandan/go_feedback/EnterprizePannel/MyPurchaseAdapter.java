package kashyap.chandan.go_feedback.EnterprizePannel;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import kashyap.chandan.go_feedback.DetailData;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.MyPurchaseDataItem;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.MyPurchaseListDataItem;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ViewDetailResponse;
import kashyap.chandan.go_feedback.ViewReviewDetail;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class MyPurchaseAdapter extends RecyclerView.Adapter<MyPurchaseAdapter.MyViewHolder> {
    Context context;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    List<MyPurchaseListDataItem> myPurchaseDataItems;
    Dialog dialog;
    public MyPurchaseAdapter(Context context, List<MyPurchaseListDataItem> myPurchaseDataItems) {
        this.context=context;
        this.myPurchaseDataItems=myPurchaseDataItems;
        dialog=new Dialog(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.purchased_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        try {
            holder.date.setText(formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,myPurchaseDataItems.get(position).getPDatetime().substring(0,10)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.businessName.setText(myPurchaseDataItems.get(position).getName());
        holder.comment.setText(myPurchaseDataItems.get(position).getCustomerReview());
        holder.price.setText("$ "+myPurchaseDataItems.get(position).getPrice());
        if (myPurchaseDataItems.get(position).getPType().equalsIgnoreCase("1"))
            holder.status.setText("Non-exclusive");
        else if (myPurchaseDataItems.get(position).getPType().equalsIgnoreCase("2"))
            holder.status.setText("Exclusive");
        else  holder.status.setText("Competitive");
        holder.rating.setText(myPurchaseDataItems.get(position).getRating());
        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.setContentView(R.layout.loadingdialog);
                dialog.setCancelable(false);
                Activity activity=(Activity)context;
                if (!activity.isFinishing()) {
                    dialog.show();
                }
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                Call<ViewDetailResponse> call=apiInterface.getDetail(myPurchaseDataItems.get(position).getFeedbackId());
                call.enqueue(new Callback<ViewDetailResponse>() {
                    @Override
                    public void onResponse(Call<ViewDetailResponse> call, Response<ViewDetailResponse> response) {
                        if (response.code()==200)
                        {
                            dialog.dismiss();
                            DetailData detailData=response.body().getData();
                            Bundle bundle=new Bundle();
                            bundle.putSerializable("data",detailData);
                            Intent intent=new Intent(context, ViewReviewDetail.class);
                            intent.putExtra("bundle",bundle);
                            context.startActivity(intent);
                        }
                        else
                        {
                            dialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<ViewDetailResponse> call, Throwable t) {

                    }
                });
            }
        });
holder.id.setText("GDS"+myPurchaseDataItems.get(position).getBId());
    }

    @Override
    public int getItemCount() {
        return myPurchaseDataItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView id,businessName,date,vincity,rating,comment,price,status;
        LinearLayout detail;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            id=itemView.findViewById(R.id.id);
            detail=itemView.findViewById(R.id.detail);
                    businessName=itemView.findViewById(R.id.businessName);
                    date=itemView.findViewById(R.id.date);
                    vincity=itemView.findViewById(R.id.vincity);
                    rating=itemView.findViewById(R.id.rating);
                    comment=itemView.findViewById(R.id.comment);
                    price=itemView.findViewById(R.id.price);
                    status=itemView.findViewById(R.id.status);
        }
    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }
}
