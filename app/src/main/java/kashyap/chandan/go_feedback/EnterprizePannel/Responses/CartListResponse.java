package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CartListResponse{

	@SerializedName("data")
	private List<CartListDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<CartListDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}