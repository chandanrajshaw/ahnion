package kashyap.chandan.go_feedback.EnterprizePannel.enterprizeFragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.go_feedback.EnterprizePannel.EnterprizeProfile;
import kashyap.chandan.go_feedback.EnterprizePannel.Purchased;
import kashyap.chandan.go_feedback.IntroSreen;
import kashyap.chandan.go_feedback.MainActivity;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.customerPannel.ChangeNumber;
import kashyap.chandan.go_feedback.customerPannel.ChangePassword;
import kashyap.chandan.go_feedback.customerPannel.PersonalDetail;
import kashyap.chandan.go_feedback.retrofit.ApiClient;

public class EnterPrizeMoreFragment extends Fragment implements View.OnClickListener {
    TextView btnSignOut,tvEmail, tvPhone,tvFname,tvdate,notificationCount;
    RelativeLayout editProfile,changeMobileNo,changePassword,my_orders,introScreen;
    SharedPreferenceData sharedPreferenceData;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    Dialog progressDialog;
    CircleImageView profileImage;;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.enterprize_more_fragment,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        tvEmail.setText(sharedPreferenceData.getEmail());
        tvFname.setText(sharedPreferenceData.getFName()+" "+sharedPreferenceData.getLName());
        tvPhone.setText(sharedPreferenceData.getPhone());
        try {
            tvdate.setText("Active Since "+formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,sharedPreferenceData.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        btnSignOut.setOnClickListener(this);
        editProfile.setOnClickListener(this);
        changeMobileNo.setOnClickListener(this);
        changePassword.setOnClickListener(this);
        my_orders.setOnClickListener(this);
        introScreen.setOnClickListener(this);
        Picasso.get().load(ApiClient.IMAGE_URL+sharedPreferenceData.getImage()).placeholder(R.drawable.loading).error(R.drawable.terms).into(profileImage);

    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }
    private void init(View view) {
        introScreen=view.findViewById(R.id.introScreen);
        my_orders=view.findViewById(R.id.my_orders);
        profileImage=view.findViewById(R.id.profileImage);
        progressDialog=new Dialog(getContext());
        tvdate=view.findViewById(R.id.tvdate);
        tvFname=view.findViewById(R.id.tvFname);
        sharedPreferenceData=new SharedPreferenceData(getContext());
        tvPhone =view.findViewById(R.id.tvPhone);
        tvEmail=view.findViewById(R.id.tvEmail);

        editProfile=view.findViewById(R.id.editProfile);
        changeMobileNo=view.findViewById(R.id.changeMobileNo);
        changePassword=view.findViewById(R.id.changePassword);
        btnSignOut=view.findViewById(R.id.btnSignOut);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.editProfile:
                Intent profileIntent=new Intent(getContext(), PersonalDetail.class);
                startActivity(profileIntent);
                break;
            case R.id.changeMobileNo:
                Intent changeMobileNoIntent=new Intent(getContext(), ChangeNumber.class);
                startActivity(changeMobileNoIntent);
                break;
            case R.id.changePassword:
                Intent changePasswordIntent=new Intent(getContext(), ChangePassword.class);
                startActivity(changePasswordIntent);
                break;
            case R.id.btnSignOut:
                Intent signOutIntent=new Intent(getContext(), MainActivity.class);
                sharedPreferenceData.sessionEnd();
                signOutIntent.putExtra("key","enterprize");
                signOutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(signOutIntent);
                getActivity().finish();
                break;

            case R.id.my_orders:
                Intent intent=new Intent(getContext(), Purchased.class);
                startActivity(intent);
                break;
            case R.id.introScreen:
                Intent intro=new Intent(getContext(), IntroSreen.class);
                startActivity(intro);
                break;
        }
    }
}
