package kashyap.chandan.go_feedback.EnterprizePannel;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.EnterprizePannel.Responses.CartListDataItem;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.CartListResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.CartTotalResponse;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.PurchaseCartListResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class Cart extends AppCompatActivity {
ImageView iv_back;
    RecyclerView recyclerview;
    RecyclerAdapterCart recyclerAdapter;
    private Dialog progressDialog;
    List<CartListDataItem> cartListDataItems=new ArrayList<>();
    SharedPreferenceData sharedPreferenceData;
TextView purchaseAmt,cartCount;
List<String> listId=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        iv_back=findViewById(R.id.iv_back);
        cartCount=findViewById(R.id.cartCount);
        purchaseAmt=findViewById(R.id.purchaseAmt);
        progressDialog=new Dialog(Cart.this);
        sharedPreferenceData=new SharedPreferenceData(Cart.this);
        recyclerview=findViewById(R.id.enterprizeFeedbackrecycler);
        recyclerview.setLayoutManager(new LinearLayoutManager(Cart.this,LinearLayoutManager.VERTICAL,false));

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        purchaseAmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                purchaseCartItems();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        cartTotal();
        myCart();
    }
  //  CartListResponse ,,,, CartListDataItem
    private  void myCart()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<CartListResponse> call=apiInterface.myCartList(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<CartListResponse>() {
                    @Override
                    public void onResponse(Call<CartListResponse> call, Response<CartListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            cartListDataItems=response.body().getData();
                            recyclerAdapter=new RecyclerAdapterCart(Cart.this,cartListDataItems);
                            recyclerview.setAdapter(recyclerAdapter);
                            for (CartListDataItem item:cartListDataItems)
                            {
                                listId.add(item.getId());
                            }
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(Cart.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<CartListResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }

    private  void cartTotal()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<CartTotalResponse> call=apiInterface.cartPrice(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<CartTotalResponse>() {
                    @Override
                    public void onResponse(Call<CartTotalResponse> call, Response<CartTotalResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            cartCount.setText(response.body().getData().getCount());
                            purchaseAmt.setText("Purchase :$"+response.body().getData().getTotalAmount());
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(Cart.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<CartTotalResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }

    private  void purchaseCartItems()
    {
        if (listId.isEmpty())
        {
            Toast.makeText(this, "Nothing In Cart!!First Add In Cart", Toast.LENGTH_SHORT).show();
        }
        else
        {
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            for (String id:listId) {
                System.out.println(id);
            }
            final Call<PurchaseCartListResponse> call=apiInterface.purchaseCartList(listId);
            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    call.enqueue(new Callback<PurchaseCartListResponse>() {
                        @Override
                        public void onResponse(Call<PurchaseCartListResponse> call, Response<PurchaseCartListResponse> response) {
                            if (response.code()==200)
                            {
                                progressDialog.dismiss();
                                Toast.makeText(Cart.this, "Purchase Successfull", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else
                            {
                                progressDialog.dismiss();
                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                    Toast.makeText(Cart.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }
                            }
                        }

                        @Override
                        public void onFailure(Call<PurchaseCartListResponse> call, Throwable t) {

                        }
                    });
                }
            };
            Thread thread=new Thread(runnable);
            thread.start();
        }

    }
}