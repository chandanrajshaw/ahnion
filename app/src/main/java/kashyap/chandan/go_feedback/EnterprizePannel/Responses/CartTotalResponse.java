package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import com.google.gson.annotations.SerializedName;

public class CartTotalResponse{

	@SerializedName("data")
	private CartData data;

	@SerializedName("status")
	private RejectionStatus status;

	public CartData getData(){
		return data;
	}

	public RejectionStatus getStatus(){
		return status;
	}
}