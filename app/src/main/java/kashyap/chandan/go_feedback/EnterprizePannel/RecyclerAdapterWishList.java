package kashyap.chandan.go_feedback.EnterprizePannel;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import kashyap.chandan.go_feedback.DetailData;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.PriceListResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.PurchaseResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.WishListDataItem;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.countresponses.RemoveWishListResponse;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.ViewDetailResponse;
import kashyap.chandan.go_feedback.ViewReviewDetail;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class RecyclerAdapterWishList extends RecyclerView.Adapter<RecyclerAdapterWishList.ViewHolder>{

    Context context;
    String price1,price2,price3;
    String ptype;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    SharedPreferenceData sharedPreferenceData;
    Dialog dialog,progressDialog;
    List<WishListDataItem> wishList;
    public RecyclerAdapterWishList(WishList dashBoard, List<WishListDataItem> wishList) {
        this.context=dashBoard;
        progressDialog=new Dialog(context);
        this.wishList=wishList;
        sharedPreferenceData=new SharedPreferenceData(context);
    }


    @NonNull
    @Override
    public RecyclerAdapterWishList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_wishlist, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterWishList.ViewHolder holder, final int position) {
holder.businessName.setText(wishList.get(position).getName());
holder.id.setText("GDS"+wishList.get(position).getBId());
holder.vincity.setText(wishList.get(position).getVicinity());
        try {
            holder.date.setText(formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,wishList.get(position).getDateandtime().substring(0,10)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.comment.setText(wishList.get(position).getCustomerReview());
        holder.purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);
                final Activity activity=(Activity)context;
                if (!activity.isFinishing())
                    progressDialog.show();
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                Call<PriceListResponse> call=apiInterface.getPrice();
                call.enqueue(new Callback<PriceListResponse>() {
                    @Override
                    public void onResponse(Call<PriceListResponse> call, final Response<PriceListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            dialog = new Dialog(context);
                            dialog.setContentView(R.layout.purchase);
                            TextView btnpurchase=dialog.findViewById(R.id.btnpurchase);
                            final TextView exclusive=dialog.findViewById(R.id.exclusive);
                            exclusive.setText("Exclusive-$"+response.body().getData().getExclusivePrice());
                            final TextView nonexclusive=dialog.findViewById(R.id.nonexclusive);
                            nonexclusive.setText("Non-Exclusive-$"+response.body().getData().getNonExclusivePrice());
                            final TextView competitive=dialog.findViewById(R.id.competitive);
                            competitive.setText("competitive-$"+response.body().getData().getComptitivePrice());
                            TextView businessName=dialog.findViewById(R.id.businessName);
                            businessName.setText(wishList.get(position).getName());
                            TextView vincity=dialog.findViewById(R.id.vincity);
                            vincity.setText(wishList.get(position).getVicinity());
                            TextView rating=dialog.findViewById(R.id.rating);
                            rating.setText(wishList.get(position).getRating());
                            TextView id=dialog.findViewById( R.id.id);
                            id.setText(wishList.get(position).getId());
                            TextView comment=dialog.findViewById(R.id.comment);
                            comment.setText(wishList.get(position).getCustomerReview());

                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            if (!activity.isFinishing()) {
                                dialog.show();
                                exclusive.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        exclusive.setBackground(context.getDrawable(R.drawable.exclusivebackground));
                                        exclusive.setTextColor(context.getResources().getColor(R.color.txtcolorwhite));
                                        price2=response.body().getData().getExclusivePrice();
                                        price1="";
                                        price3="";
                                        ptype="2";
                                        nonexclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        nonexclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                        competitive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        competitive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                    }
                                });
                                nonexclusive.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        nonexclusive.setBackground(context.getDrawable(R.drawable.exclusivebackground));
                                        nonexclusive.setTextColor(context.getResources().getColor(R.color.txtcolorwhite));
                                        price1=response.body().getData().getNonExclusivePrice();
                                        price3="";
                                        price2="";
                                        ptype="1";
                                        exclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        exclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                        competitive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        competitive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                    }
                                });
                                competitive.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        competitive.setBackground(context.getDrawable(R.drawable.exclusivebackground));
                                        competitive.setTextColor(context.getResources().getColor(R.color.txtcolorwhite));
                                        price3=response.body().getData().getComptitivePrice();
                                        price2="";
                                        price1="";
                                        ptype="3";
                                        exclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        exclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                        nonexclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        nonexclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                    }
                                });
                                btnpurchase.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        progressDialog.setContentView(R.layout.loadingdialog);
                                        progressDialog.setCancelable(false);
                                        final Activity activity=(Activity)context;
                                        if (!activity.isFinishing())
                                            progressDialog.show();
                                        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                                        Call<PurchaseResponse>call1=apiInterface.requestPurchase(wishList.get(position).getFeedbackId(),ptype,price1,price2,price3,sharedPreferenceData.getId(),"0");
                                        call1.enqueue(new Callback<PurchaseResponse>() {
                                            @Override
                                            public void onResponse(Call<PurchaseResponse> call, Response<PurchaseResponse> response) {
                                                if (response.code()==200)
                                                {
                                                    progressDialog.dismiss();
                                                    Toast.makeText(context, "Purchased", Toast.LENGTH_SHORT).show();
                                                    final Dialog purchasedialog=new Dialog(context);
                                                    purchasedialog.setContentView(R.layout.custommessagedialog);
                                                    purchasedialog.show();
                                                    purchasedialog.setCancelable(false);
                                                    TextView ok=purchasedialog.findViewById(R.id.ok);
                                                    TextView message=purchasedialog.findViewById(R.id.message);
                                                    message.setText("Purchase Successfull");
                                                    ok.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            purchasedialog.dismiss();
                                                            Intent intent=new Intent(context,WishList.class);
                                                            context.startActivity(intent);
                                                            ((Activity) context).finish();
                                                        }
                                                    });
                                                    dialog.dismiss();

                                                }
                                                else
                                                {
                                                    progressDialog.dismiss();
                                                    Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<PurchaseResponse> call, Throwable t) {

                                            }
                                        });
                                    }
                                });
                            }
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(context, "Error Goes!! Try After Some time", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<PriceListResponse> call, Throwable t) {

                    }
                });


            }
        });
holder.rating.setText(String.valueOf(Float.parseFloat(wishList.get(position).getRating())));
        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);
                Activity activity=(Activity)context;
                if (!activity.isFinishing()) {
                    progressDialog.show();
                }
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                Call<ViewDetailResponse> call=apiInterface.getDetail(wishList.get(position).getId());
                call.enqueue(new Callback<ViewDetailResponse>() {
                    @Override
                    public void onResponse(Call<ViewDetailResponse> call, Response<ViewDetailResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            DetailData detailData=response.body().getData();
                            Bundle bundle=new Bundle();
                            bundle.putSerializable("data",detailData);
                            Intent intent=new Intent(context, ViewReviewDetail.class);
                            intent.putExtra("bundle",bundle);
                            context.startActivity(intent);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<ViewDetailResponse> call, Throwable t) {

                    }
                });
            }
        });
        holder.removewishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);
                Activity activity=(Activity)context;
                if (!activity.isFinishing()) {
                    progressDialog.show();
                }
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                Call<RemoveWishListResponse> call=apiInterface.removeWishlist(sharedPreferenceData.getId(),wishList.get(position).getFeedbackId());
                call.enqueue(new Callback<RemoveWishListResponse>() {
                    @Override
                    public void onResponse(Call<RemoveWishListResponse> call, Response<RemoveWishListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            wishList.remove(holder.getAdapterPosition());
                            notifyItemRemoved(holder.getAdapterPosition());
                            notifyDataSetChanged();

                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<RemoveWishListResponse> call, Throwable t) {

                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        return wishList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id,businessName,date,vincity,rating,comment,purchase;
LinearLayout detail;
ImageView removewishList;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            removewishList=itemView.findViewById(R.id.removewishList);
            detail=itemView.findViewById(R.id.linear);
            id=itemView.findViewById(R.id.id);
            businessName=itemView.findViewById(R.id.businessName);
            date=itemView.findViewById(R.id.date);
            vincity=itemView.findViewById(R.id.vincity);
            rating=itemView.findViewById(R.id.rating);
            comment=itemView.findViewById(R.id.comment);
            purchase=itemView.findViewById(R.id.purchase);
        }
    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }
}
