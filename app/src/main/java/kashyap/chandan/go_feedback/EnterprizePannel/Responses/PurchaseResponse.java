package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import com.google.gson.annotations.SerializedName;

public class PurchaseResponse{

	@SerializedName("status")
	private PurchaseStatus status;

	public PurchaseStatus getStatus(){
		return status;
	}
}