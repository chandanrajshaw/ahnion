package kashyap.chandan.go_feedback.EnterprizePannel;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import kashyap.chandan.go_feedback.DetailData;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.CartListDataItem;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.DeleteCartResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.ViewDetailResponse;
import kashyap.chandan.go_feedback.ViewReviewDetail;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;


public class RecyclerAdapterCart extends RecyclerView.Adapter<RecyclerAdapterCart.ViewHolder>{

    Context context;
    String price1,price2,price3;
    String ptype;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    SharedPreferenceData sharedPreferenceData;
    Dialog dialog,progressDialog;
    List<CartListDataItem> cartListDataItems;


    public RecyclerAdapterCart(Context context, List<CartListDataItem> cartListDataItems) {
        this.context=context;
        progressDialog=new Dialog(context);
        this.cartListDataItems=cartListDataItems;
        sharedPreferenceData=new SharedPreferenceData(context);
    }


    @NonNull
    @Override
    public RecyclerAdapterCart.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.row_card_cart, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterCart.ViewHolder holder, final int position) {
holder.businessName.setText(cartListDataItems.get(position).getName());
holder.comment.setText(cartListDataItems.get(position).getCustomerReview());
holder.id.setText("GDS"+cartListDataItems.get(position).getId());
holder.rating.setText(String.valueOf(Float.parseFloat(cartListDataItems.get(position).getRating())));
holder.vincity.setText(cartListDataItems.get(position).getVicinity());
if (cartListDataItems.get(position).getPType().equalsIgnoreCase("1"))
    holder.payType.setText("$"+cartListDataItems.get(position).getPrice()+"Non-exclusive");
else if (cartListDataItems.get(position).getPType().equalsIgnoreCase("2"))
    holder.payType.setText("$"+cartListDataItems.get(position).getPrice()+"Exclusive");
else
    holder.payType.setText("$"+cartListDataItems.get(position).getPrice()+"Competitive");
        try {
            holder.date.setText(formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,cartListDataItems.get(position).getDateandtime().substring(0,10)));
        } catch (ParseException e) {
            e.printStackTrace();
        }

//                                exclusive.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        exclusive.setBackground(context.getDrawable(R.drawable.exclusivebackground));
//                                        exclusive.setTextColor(context.getResources().getColor(R.color.txtcolorwhite));
//                                        price2=response.body().getData().getExclusivePrice();
//                                        price1="";
//                                        price3="";
//                                        ptype="2";
//                                        nonexclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
//                                        nonexclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
//                                        competitive.setBackground(context.getDrawable(R.drawable.nonexclusive));
//                                        competitive.setTextColor(context.getResources().getColor(R.color.txtcolor));
//                                    }
//                                });
//                                nonexclusive.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        nonexclusive.setBackground(context.getDrawable(R.drawable.exclusivebackground));
//                                        nonexclusive.setTextColor(context.getResources().getColor(R.color.txtcolorwhite));
//                                        price1=response.body().getData().getNonExclusivePrice();
//                                        price3="";
//                                        price2="";
//                                        ptype="1";
//                                        exclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
//                                        exclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
//                                        competitive.setBackground(context.getDrawable(R.drawable.nonexclusive));
//                                        competitive.setTextColor(context.getResources().getColor(R.color.txtcolor));
//                                    }
//                                });
//                                competitive.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        competitive.setBackground(context.getDrawable(R.drawable.exclusivebackground));
//                                        competitive.setTextColor(context.getResources().getColor(R.color.txtcolorwhite));
//                                        price3=response.body().getData().getComptitivePrice();
//                                        price2="";
//                                        price1="";
//                                        ptype="3";
//                                        exclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
//                                        exclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
//                                        nonexclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
//                                        nonexclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
//                                    }
//                                });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);
                Activity activity=(Activity)context;
                if (!activity.isFinishing()) {
                    progressDialog.show();
                }
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                Call<DeleteCartResponse> call=apiInterface.deleteCart(cartListDataItems.get(position).getId());
                call.enqueue(new Callback<DeleteCartResponse>() {
                    @Override
                    public void onResponse(Call<DeleteCartResponse> call, Response<DeleteCartResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            Toast.makeText(context, "Deleted From cart", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(context, Cart.class);
                            context.startActivity(intent);
                            ((Activity) context).finish();
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteCartResponse> call, Throwable t) {

                    }
                });
            }
        });

        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);
                Activity activity=(Activity)context;
                if (!activity.isFinishing()) {
                    progressDialog.show();
                }
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                Call<ViewDetailResponse> call=apiInterface.getDetail(cartListDataItems.get(position).getId());
                call.enqueue(new Callback<ViewDetailResponse>() {
                    @Override
                    public void onResponse(Call<ViewDetailResponse> call, Response<ViewDetailResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            DetailData detailData=response.body().getData();
                            Bundle bundle=new Bundle();
                            bundle.putSerializable("data",detailData);
                            Intent intent=new Intent(context, ViewReviewDetail.class);
                            intent.putExtra("bundle",bundle);
                            context.startActivity(intent);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<ViewDetailResponse> call, Throwable t) {

                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        return cartListDataItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id,businessName,payType,date,vincity,rating,comment,exclusive,nonexclusive,competitive;
        RelativeLayout delete;
        LinearLayout detail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            payType=itemView.findViewById(R.id.payType);
            detail=itemView.findViewById(R.id.linear);
            exclusive=itemView.findViewById(R.id.exclusive);
            nonexclusive=itemView.findViewById(R.id.nonexclusive);
            competitive=itemView.findViewById(R.id.competitive);
            id=itemView.findViewById(R.id.id);
            businessName=itemView.findViewById(R.id.businessName);
            date=itemView.findViewById(R.id.date);
            vincity=itemView.findViewById(R.id.vincity);
            rating=itemView.findViewById(R.id.rating);
            comment=itemView.findViewById(R.id.comment);
            delete=itemView.findViewById(R.id.delete);


        }
    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }
}
