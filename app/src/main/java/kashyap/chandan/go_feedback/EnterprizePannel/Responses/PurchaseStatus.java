package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import com.google.gson.annotations.SerializedName;

public class PurchaseStatus {

	@SerializedName("code")
	private int code;

	@SerializedName("message")
	private String message;

	public int getCode(){
		return code;
	}

	public String getMessage(){
		return message;
	}
}