package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class EnterprizeAllResponse{

	@SerializedName("data")
	private List<EnterprizeAllFeedbackData> data;

	@SerializedName("status")
	private PurchaseStatus status;

	public List<EnterprizeAllFeedbackData> getData(){
		return data;
	}

	public PurchaseStatus getStatus(){
		return status;
	}
}