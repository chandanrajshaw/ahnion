package kashyap.chandan.go_feedback.EnterprizePannel;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.EnterprizePannel.Responses.MyWishListResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.WishListDataItem;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;


public class WishList extends AppCompatActivity {
ImageView iv_back;
    RecyclerView recyclerview;
    RecyclerAdapterWishList recyclerAdapter;
    SharedPreferenceData sharedPreferenceData;
    private Dialog progressDialog;
    List<WishListDataItem> wishList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
init();

        recyclerview.setLayoutManager(new LinearLayoutManager(WishList.this,LinearLayoutManager.VERTICAL,false));
//        recyclerAdapter=new RecyclerAdapterWishList(WishList.this, wishList);
//        recyclerview.setAdapter(recyclerAdapter);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        myWish();
    }

    private  void myWish()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<MyWishListResponse> call=apiInterface.myWishList(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<MyWishListResponse>() {
                    @Override
                    public void onResponse(Call<MyWishListResponse> call, Response<MyWishListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            wishList=response.body().getData();
                            recyclerAdapter=new RecyclerAdapterWishList(WishList.this,wishList);
                            recyclerview.setAdapter(recyclerAdapter);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(WishList.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<MyWishListResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
    private void init() {
        progressDialog=new Dialog(WishList.this);
        iv_back=findViewById(R.id.iv_back);
        sharedPreferenceData=new SharedPreferenceData(WishList.this);
        recyclerview=findViewById(R.id.enterprizeFeedbackrecycler);
    }
}