package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import com.google.gson.annotations.SerializedName;

public class PriceListResponse{

	@SerializedName("data")
	private PriceData data;

	@SerializedName("status")
	private PurchaseStatus status;

	public PriceData getData(){
		return data;
	}

	public PurchaseStatus getStatus(){
		return status;
	}
}