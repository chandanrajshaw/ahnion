package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import com.google.gson.annotations.SerializedName;

public class AddToCartResponse{

	@SerializedName("status")
	private AddCartStatus status;

	public AddCartStatus getStatus(){
		return status;
	}
}