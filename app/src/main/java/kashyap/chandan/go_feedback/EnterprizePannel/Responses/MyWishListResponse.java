package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class MyWishListResponse{

	@SerializedName("data")
	private List<WishListDataItem> data;

	@SerializedName("status")
	private RejectionStatus status;

	public List<WishListDataItem> getData(){
		return data;
	}

	public RejectionStatus getStatus(){
		return status;
	}
}