package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class EnterprizeBusinessWiseFeedbckListResponse{

	@SerializedName("data")
	private List<EnterpriseFeedbackDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<EnterpriseFeedbackDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}