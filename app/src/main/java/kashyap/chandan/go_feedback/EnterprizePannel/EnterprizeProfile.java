package kashyap.chandan.go_feedback.EnterprizePannel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.go_feedback.MainActivity;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.customerPannel.ChangeNumber;
import kashyap.chandan.go_feedback.customerPannel.ChangePassword;
import kashyap.chandan.go_feedback.customerPannel.LandingScreen;
import kashyap.chandan.go_feedback.customerPannel.PersonalDetail;

public class EnterprizeProfile extends AppCompatActivity implements View.OnClickListener{
    TextView btnSignOut,tvEmail, tvPhone,tvFname,tvdate,notificationCount;
    CircleImageView profileImage;
    ImageView iv_back;
    RelativeLayout editProfile,changeMobileNo,changePassword,my_orders;
    SharedPreferenceData sharedPreferenceData;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    Dialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprize_profile);
        init();
        tvEmail.setText(sharedPreferenceData.getEmail());
        tvFname.setText(sharedPreferenceData.getFName()+" "+sharedPreferenceData.getLName());
        tvPhone.setText(sharedPreferenceData.getPhone());
        try {
            tvdate.setText("Active Since "+formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,sharedPreferenceData.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Picasso.get().load("http://www.igranddeveloper.xyz/godesio/"+sharedPreferenceData.getImage()).placeholder(R.drawable.loading).error(R.drawable.terms).into(profileImage);
        btnSignOut.setOnClickListener(this);
        editProfile.setOnClickListener(this);
        changeMobileNo.setOnClickListener(this);
        changePassword.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        my_orders.setOnClickListener(this);
    }
    private void init() {
        my_orders=findViewById(R.id.my_orders);
        iv_back=findViewById(R.id.iv_back);
        progressDialog=new Dialog(EnterprizeProfile.this);
        tvdate=findViewById(R.id.tvdate);
        tvFname=findViewById(R.id.tvFname);
        sharedPreferenceData=new SharedPreferenceData(EnterprizeProfile.this);
        tvPhone =findViewById(R.id.tvPhone);
        tvEmail=findViewById(R.id.tvEmail);
        profileImage=findViewById(R.id.profileImage);
        editProfile=findViewById(R.id.editProfile);
        changeMobileNo=findViewById(R.id.changeMobileNo);
        changePassword=findViewById(R.id.changePassword);
        btnSignOut=findViewById(R.id.btnSignOut);
    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.editProfile:
                Intent profileIntent=new Intent(EnterprizeProfile.this, PersonalDetail.class);
                startActivity(profileIntent);
                break;
            case R.id.changeMobileNo:
                Intent changeMobileNoIntent=new Intent(EnterprizeProfile.this, ChangeNumber.class);
                startActivity(changeMobileNoIntent);
                break;
            case R.id.changePassword:
                Intent changePasswordIntent=new Intent(EnterprizeProfile.this, ChangePassword.class);
               startActivity(changePasswordIntent);
                break;
            case R.id.btnSignOut:
                Intent signOutIntent=new Intent(EnterprizeProfile.this, MainActivity.class);
                sharedPreferenceData.sessionEnd();
                signOutIntent.putExtra("key","key");
                startActivity(signOutIntent);
                finish();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.my_orders:
                Intent intent=new Intent(EnterprizeProfile.this,Purchased.class);
                startActivity(intent);
                break;
        }
    }
}