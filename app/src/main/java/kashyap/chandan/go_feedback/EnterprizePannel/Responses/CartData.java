package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import com.google.gson.annotations.SerializedName;

public class CartData {

	@SerializedName("total_amount")
	private String totalAmount;

	@SerializedName("count")
	private String count;

	public String getTotalAmount(){
		return totalAmount;
	}

	public String getCount(){
		return count;
	}
}