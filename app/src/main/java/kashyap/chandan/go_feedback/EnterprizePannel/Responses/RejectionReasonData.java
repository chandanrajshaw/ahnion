package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RejectionReasonData implements Serializable {

	@SerializedName("reason1")
	private String reason1;

	@SerializedName("reason2")
	private String reason2;

	@SerializedName("reason3")
	private String reason3;

	@SerializedName("reason4")
	private String reason4;

	public String getReason1(){
		return reason1;
	}

	public String getReason2(){
		return reason2;
	}

	public String getReason3(){
		return reason3;
	}

	public String getReason4(){
		return reason4;
	}
}