package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import com.google.gson.annotations.SerializedName;

public class PriceData {

	@SerializedName("comptitive_price")
	private String comptitivePrice;

	@SerializedName("non_exclusive_price")
	private String nonExclusivePrice;

	@SerializedName("exclusive_price")
	private String exclusivePrice;

	public String getComptitivePrice(){
		return comptitivePrice;
	}

	public String getNonExclusivePrice(){
		return nonExclusivePrice;
	}

	public String getExclusivePrice(){
		return exclusivePrice;
	}
}