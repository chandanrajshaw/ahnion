package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import com.google.gson.annotations.SerializedName;

public class AddWishListResponse{

	@SerializedName("status")
	private WishListStatus status;

	public WishListStatus getStatus(){
		return status;
	}
}