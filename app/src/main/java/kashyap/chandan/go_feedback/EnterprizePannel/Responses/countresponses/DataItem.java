package kashyap.chandan.go_feedback.EnterprizePannel.Responses.countresponses;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("rating")
	private String rating;

	@SerializedName("feedback_id")
	private String feedbackId;

	@SerializedName("attitude_of_stass")
	private String attitudeOfStass;

	@SerializedName("b_id")
	private String bId;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("ambience")
	private String ambience;

	@SerializedName("service")
	private String service;

	@SerializedName("name")
	private String name;

	@SerializedName("vicinity")
	private String vicinity;

	@SerializedName("id")
	private String id;

	@SerializedName("recipt")
	private String recipt;

	@SerializedName("customer_review")
	private String customerReview;

	@SerializedName("imagees")
	private String imagees;

	@SerializedName("dateandtime")
	private String dateandtime;

	public String getRating(){
		return rating;
	}

	public String getFeedbackId(){
		return feedbackId;
	}

	public String getAttitudeOfStass(){
		return attitudeOfStass;
	}

	public String getBId(){
		return bId;
	}

	public String getUserId(){
		return userId;
	}

	public String getAmbience(){
		return ambience;
	}

	public String getService(){
		return service;
	}

	public String getName(){
		return name;
	}

	public String getVicinity(){
		return vicinity;
	}

	public String getId(){
		return id;
	}

	public String getRecipt(){
		return recipt;
	}

	public String getCustomerReview(){
		return customerReview;
	}

	public String getImagees(){
		return imagees;
	}

	public String getDateandtime(){
		return dateandtime;
	}
}