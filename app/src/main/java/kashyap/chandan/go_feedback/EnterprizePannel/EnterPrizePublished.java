package kashyap.chandan.go_feedback.EnterprizePannel;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.EnterprizePannel.Responses.EnterpriseFeedbackDataItem;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.EnterprizeBusinessWiseFeedbckListResponse;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnterPrizePublished extends AppCompatActivity {
    ImageView iv_back;
    Dialog progressDialog;
    RecyclerView publishedRecycler;
    TextView toolHeader,newFeedback;
    Intent intent;
    List<EnterpriseFeedbackDataItem> publishedData=new ArrayList<>();
    private String id;
    SharedPreferenceData sharedPreferenceData;
    private ArrayList<EnterpriseFeedbackDataItem> newfeedbackDataList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_published);
        init();
        id=intent.getStringExtra("id");
        newFeedback.setText("New Feedbacks");
        toolHeader.setText("New Feedbacks");
        publishedRecycler.setLayoutManager(new LinearLayoutManager(EnterPrizePublished.this,LinearLayoutManager.VERTICAL,false));
getPublished();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
       getPublished();
    }

    private void init() {
        intent=getIntent();
        sharedPreferenceData=new SharedPreferenceData(EnterPrizePublished.this);
        newFeedback=findViewById(R.id.newFeedback);
        toolHeader=findViewById(R.id.toolHeader);
        iv_back=findViewById(R.id.iv_back);
        publishedRecycler=findViewById(R.id.publishedRecycler);
        progressDialog=new Dialog(EnterPrizePublished.this);
    }

    private void getPublished()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        //EnterpriseFeedbackDataItem,EnterprizeBusinessWiseFeedbckListResponse
        final Call<EnterprizeBusinessWiseFeedbckListResponse> call=apiInterface.EnterprizePublish(id,sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<EnterprizeBusinessWiseFeedbckListResponse>() {
                    @Override
                    public void onResponse(Call<EnterprizeBusinessWiseFeedbckListResponse> call, Response<EnterprizeBusinessWiseFeedbckListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            publishedData=response.body().getData();
//                            for (EnterpriseFeedbackDataItem data:publishedData)
//                            {
//                                if (Integer.parseInt(data.getEnterprisePurchessStatus())==0)
//                                {
//                                    newfeedbackDataList.add(data);
//                                }
//                            }
//                            if (newfeedbackDataList.isEmpty())
//                                Toast.makeText(EnterPrizePublished.this, "All Feedbacks Purchased", Toast.LENGTH_SHORT).show();
                            publishedRecycler.setAdapter(new EnterprizePublishedFeedbackAdapter(EnterPrizePublished.this,publishedData));
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(EnterPrizePublished.this, "Failed", Toast.LENGTH_SHORT).show();
//                            Converter<ResponseBody, ApiError> converter =
//                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
//                            ApiError error;
//                            try {
//                                error = converter.convert(response.errorBody());
//                                ApiError.StatusBean status=error.getStatus();
//                                Toast.makeText(AdminPublished.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
//                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<EnterprizeBusinessWiseFeedbckListResponse> call, Throwable t) {
progressDialog.dismiss();
                        Toast.makeText(EnterPrizePublished.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}