package kashyap.chandan.go_feedback.EnterprizePannel;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.google.android.gms.maps.model.Circle;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.go_feedback.AdminPannel.AdminPaid;
import kashyap.chandan.go_feedback.AdminPannel.AdminPaidFeedbackAdapter;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPaidResponse;
import kashyap.chandan.go_feedback.AdminPannel.Fragments.AdminDashboardFragment;
import kashyap.chandan.go_feedback.AdminPannel.Fragments.AdminMoreFragment;
import kashyap.chandan.go_feedback.AdminPannel.Fragments.AdminWallerFragment;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.EnterprizeAllFeedbackData;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.EnterprizeAllResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.enterprizeFragment.EnterPrizeMoreFragment;
import kashyap.chandan.go_feedback.EnterprizePannel.enterprizeFragment.EnterprizeDashboardFragment;
import kashyap.chandan.go_feedback.EnterprizePannel.enterprizeFragment.MyOrderFragment;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class EnterprizeDashBoard extends AppCompatActivity {
TextView enterprizeName;
CircleImageView profileImage;
ImageView cart;
    ImageView bookmark;
    SharedPreferenceData sharedPreferenceData;
    Dialog progressDialog;
    BottomNavigationView bottomNavigationView;
    private FrameLayout mainFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprize_dashboard);
init();
        FragmentManager fm=getSupportFragmentManager();
        EnterprizeDashboardFragment enterprizeDashBoard=new EnterprizeDashboardFragment();
        FragmentTransaction ft=fm.beginTransaction();
        ft.add(R.id.mainFrame,enterprizeDashBoard);
        ft.commit();
cart.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(EnterprizeDashBoard.this,Cart.class);
        startActivity(intent);
    }
});
        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(EnterprizeDashBoard.this,WishList.class);
                startActivity(intent);
            }
        });

enterprizeName.setText(sharedPreferenceData.getFName());
//profile.setOnClickListener(new View.OnClickListener() {
//    @Override
//    public void onClick(View view) {
//        Intent intent=new Intent(EnterprizeDashBoard.this,EnterprizeProfile.class);
//        startActivity(intent);
//    }
//});
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment=null;

                switch (item.getItemId())
                {
                    case R.id.action_purchase:
                        fragment=new MyOrderFragment();
                        break;
                    case R.id.action_dashboard:
                        fragment=new EnterprizeDashboardFragment();
                        break;
                    case R.id.action_more:
                        fragment=new EnterPrizeMoreFragment();
                        break;
                }
                return loadFragment(fragment);
            }
        });
    }

    private void init() {
        mainFrame=findViewById(R.id.mainFrame);
        bottomNavigationView=findViewById(R.id.bottomNavigationView);
        cart=findViewById(R.id.cart);
//        profile=findViewById(R.id.profile);
        profileImage=findViewById(R.id.profileImage);
        sharedPreferenceData=new SharedPreferenceData(EnterprizeDashBoard.this);
        bookmark=findViewById(R.id.bookmark);
        enterprizeName=findViewById(R.id.enterprizeName);
        progressDialog=new Dialog(EnterprizeDashBoard.this);
    }
    private boolean loadFragment(Fragment fragment)
    {
        if (fragment!=null)
        {
            FragmentManager fragmentManager=getSupportFragmentManager();
            FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.mainFrame,fragment);
//            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            return true;
        }
        return false;

    }
    @Override
    protected void onResume() {
        super.onResume();
        Picasso.get().load(ApiClient.IMAGE_URL+sharedPreferenceData.getImage()).placeholder(R.drawable.loading).error(R.drawable.terms).into(profileImage);
    }



}