package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import com.google.gson.annotations.SerializedName;

public class MyPurchaseListDataItem {

	@SerializedName("user_id")
	private String userId;

	@SerializedName("b_id")
	private String bId;

	@SerializedName("price")
	private String price;

	@SerializedName("rating")
	private String rating;

	@SerializedName("name")
	private String name;

	@SerializedName("feedback_id")
	private String feedbackId;

	@SerializedName("p_type")
	private String pType;

	@SerializedName("p_datetime")
	private String pDatetime;

	@SerializedName("id")
	private String id;

	@SerializedName("customer_id")
	private String customerId;

	@SerializedName("customer_review")
	private String customerReview;

	public String getUserId(){
		return userId;
	}

	public String getBId(){
		return bId;
	}

	public String getPrice(){
		return price;
	}

	public String getRating(){
		return rating;
	}

	public String getName(){
		return name;
	}

	public String getFeedbackId(){
		return feedbackId;
	}

	public String getPType(){
		return pType;
	}

	public String getPDatetime(){
		return pDatetime;
	}

	public String getId(){
		return id;
	}

	public String getCustomerId(){
		return customerId;
	}

	public String getCustomerReview(){
		return customerReview;
	}
}