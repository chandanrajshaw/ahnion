package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import com.google.gson.annotations.SerializedName;

public class CartListDataItem {

	@SerializedName("rating")
	private String rating;

	@SerializedName("attitude_of_stass")
	private String attitudeOfStass;

	@SerializedName("b_id")
	private String bId;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("ambience")
	private String ambience;

	@SerializedName("price")
	private String price;

	@SerializedName("service")
	private String service;

	@SerializedName("name")
	private String name;

	@SerializedName("p_type")
	private String pType;

	@SerializedName("vicinity")
	private String vicinity;

	@SerializedName("id")
	private String id;

	@SerializedName("recipt")
	private String recipt;

	@SerializedName("customer_review")
	private String customerReview;

	@SerializedName("imagees")
	private String imagees;

	@SerializedName("dateandtime")
	private String dateandtime;

	public String getRating(){
		return rating;
	}

	public String getAttitudeOfStass(){
		return attitudeOfStass;
	}

	public String getBId(){
		return bId;
	}

	public String getUserId(){
		return userId;
	}

	public String getAmbience(){
		return ambience;
	}

	public String getPrice(){
		return price;
	}

	public String getService(){
		return service;
	}

	public String getName(){
		return name;
	}

	public String getPType(){
		return pType;
	}

	public String getVicinity(){
		return vicinity;
	}

	public String getId(){
		return id;
	}

	public String getRecipt(){
		return recipt;
	}

	public String getCustomerReview(){
		return customerReview;
	}

	public String getImagees(){
		return imagees;
	}

	public String getDateandtime(){
		return dateandtime;
	}
}