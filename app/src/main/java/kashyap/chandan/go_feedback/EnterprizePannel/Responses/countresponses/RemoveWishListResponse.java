package kashyap.chandan.go_feedback.EnterprizePannel.Responses.countresponses;

import com.google.gson.annotations.SerializedName;

public class RemoveWishListResponse{

	@SerializedName("status")
	private Status status;

	public Status getStatus(){
		return status;
	}
}