package kashyap.chandan.go_feedback.EnterprizePannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.EnterprizeAllFeedbackData;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.EnterprizeAllResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.MyPurchaseDataItem;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.MyPurchaseListDataItem;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.MyPurchaseListResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.MyPurchaseResponse;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class Purchased extends AppCompatActivity {
//TextView enterprizeName;
//CircleImageView profileImage;
SharedPreferenceData sharedPreferenceData;
    RecyclerView recyclerview;
    Dialog progressDialog;
    ImageView iv_back;
    Intent intent;
    String id;
    List<MyPurchaseListDataItem> myPurchaseDataItems=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchased);
        init();
//        Picasso.get().load("http://www.igranddeveloper.xyz/godesio/"+sharedPreferenceData.getImage()).placeholder(R.drawable.loading).error(R.drawable.terms).into(profileImage);
//        enterprizeName.setText(sharedPreferenceData.getFName());
        recyclerview.setLayoutManager(new LinearLayoutManager(Purchased.this,LinearLayoutManager.VERTICAL,false));
id=intent.getStringExtra("id");
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
 myAllPurchase();
    }

    private void init() {
        intent=getIntent();
        iv_back=findViewById(R.id.iv_back);
//        enterprizeName=findViewById(R.id.enterprizeName);
//        profileImage=findViewById(R.id.profileImage);
        sharedPreferenceData=new SharedPreferenceData(Purchased.this);
        recyclerview=findViewById(R.id.enterprizeFeedbackrecycler);
        progressDialog=new Dialog(Purchased.this);
    }
    private  void myAllPurchase()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<MyPurchaseListResponse> call=apiInterface.myPurchase(sharedPreferenceData.getId(),id);
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<MyPurchaseListResponse>() {
                    @Override
                    public void onResponse(Call<MyPurchaseListResponse> call, Response<MyPurchaseListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            myPurchaseDataItems=response.body().getData();
                            recyclerview.setAdapter(new MyPurchaseAdapter(Purchased.this,myPurchaseDataItems));
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(Purchased.this, "Error", Toast.LENGTH_SHORT).show();
//                            Converter<ResponseBody, ApiError> converter =
//                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
//                            ApiError error;
//                            try {
//                                error = converter.convert(response.errorBody());
//                                ApiError.StatusBean status=error.getStatus();
//                                Toast.makeText(Purchased.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
//                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<MyPurchaseListResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}