package kashyap.chandan.go_feedback.EnterprizePannel;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import kashyap.chandan.go_feedback.DetailData;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.AddToCartResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.AddWishListResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.EnterpriseFeedbackDataItem;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.PriceListResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.PurchaseResponse;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.ViewDetailResponse;
import kashyap.chandan.go_feedback.ViewReviewDetail;
import kashyap.chandan.go_feedback.customerPannel.FeedbackScreen;
import kashyap.chandan.go_feedback.customerPannel.LandingScreen;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;


public class EnterprizePublishedFeedbackAdapter extends RecyclerView.Adapter<EnterprizePublishedFeedbackAdapter.ViewHolder>{

    Context context;
    Dialog dialog,progressDialog;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    List<EnterpriseFeedbackDataItem> feedbackDataList;
    List<EnterpriseFeedbackDataItem> newfeedbackDataList=new ArrayList<>();
String price1,price2,price3;
String ptype;
SharedPreferenceData sharedPreferenceData;
    public EnterprizePublishedFeedbackAdapter(Context dashBoard, List<EnterpriseFeedbackDataItem> feedbackDataList) {
        this.context=dashBoard;
        this.newfeedbackDataList=feedbackDataList;
        progressDialog=new Dialog(context);
        sharedPreferenceData=new SharedPreferenceData(context);
//        for (EnterpriseFeedbackDataItem data:feedbackDataList)
//        {
//            if (Integer.parseInt(data.getEnterprisePurchessStatus())==0)
//            {
//                newfeedbackDataList.add(data);
//            }
//        }


    }


    @NonNull
    @Override
    public EnterprizePublishedFeedbackAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_dashboard, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final EnterprizePublishedFeedbackAdapter.ViewHolder holder, final int position) {
//if (Integer.parseInt(feedbackDataList.get(position).getEnterprisePurchessStatus())==0)
//{
//
//}
        holder.businessName.setText(newfeedbackDataList.get(position).getName());
        holder.comment.setText(newfeedbackDataList.get(position).getCustomerReview());
        holder.rating.setText(newfeedbackDataList.get(position).getRating());
        holder.vincity.setText(newfeedbackDataList.get(position).getVicinity());
        holder.id.setText("GDS"+newfeedbackDataList.get(position).getBId());
        if (Integer.parseInt(newfeedbackDataList.get(position).getEnterprisePurchessStatus())==0)
        {
            //EnterprizeBusinessWiseFeedbckListResponse,EnterpriseFeedbackDataItem
            holder.btnpurchase.setText("Purchase");
            if (Integer.parseInt(newfeedbackDataList.get(position).getEnterpriseWishlistStatus())==0)
            {
                //EnterprizeBusinessWiseFeedbckListResponse,EnterpriseFeedbackDataItem
                holder.wishlist.setVisibility(View.VISIBLE);
                holder.wishlist_added.setVisibility(View.GONE);
            }
            else
            {

                holder.wishlist_added.setVisibility(View.VISIBLE);
                holder.wishlist.setVisibility(View.GONE);
            }
            if (Integer.parseInt(newfeedbackDataList.get(position).getEnterpriseCartStatus())==0)
            {
                holder.added.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.added.setVisibility(View.GONE);
            }

        }
        else
        {
            holder.btnpurchase.setText("Purchased");
            holder.purchase.setVisibility(View.GONE);
            holder.added.setVisibility(View.GONE);
            holder.wishlist.setVisibility(View.GONE);
        }
//        if (Integer.parseInt(newfeedbackDataList.get(position).getEnterpriseWishlistStatus())==0)
//        {
//            //EnterprizeBusinessWiseFeedbckListResponse,EnterpriseFeedbackDataItem
//            holder.wishlist.setVisibility(View.VISIBLE);
//            holder.wishlist_added.setVisibility(View.GONE);
//        }
//        else
//        {
//
//            holder.wishlist_added.setVisibility(View.VISIBLE);
//            holder.wishlist.setVisibility(View.GONE);
//        }
        try {
            holder.date.setText(formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,newfeedbackDataList.get(position).getDatetime().substring(0,10)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);
                final Activity activity=(Activity)context;
                if (!activity.isFinishing())
                    progressDialog.show();
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                Call<PriceListResponse>call=apiInterface.getPrice();
                call.enqueue(new Callback<PriceListResponse>() {
                    @Override
                    public void onResponse(Call<PriceListResponse> call, final Response<PriceListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            dialog = new Dialog(context);
                            dialog.setContentView(R.layout.purchase);
                            TextView btnpurchase=dialog.findViewById(R.id.btnpurchase);
                            final TextView exclusive=dialog.findViewById(R.id.exclusive);
                            exclusive.setText("Exclusive-$"+response.body().getData().getExclusivePrice());
                            final TextView nonexclusive=dialog.findViewById(R.id.nonexclusive);
                            nonexclusive.setText("Non-Exclusive-$"+response.body().getData().getNonExclusivePrice());
                            final TextView competitive=dialog.findViewById(R.id.competitive);
                            competitive.setVisibility(View.GONE);
                            competitive.setText("competitive-$"+response.body().getData().getComptitivePrice());
                            TextView businessName=dialog.findViewById(R.id.businessName);
                            businessName.setText(newfeedbackDataList.get(position).getName());
                            TextView vincity=dialog.findViewById(R.id.vincity);
                            vincity.setText(newfeedbackDataList.get(position).getVicinity());
                            TextView rating=dialog.findViewById(R.id.rating);
                            rating.setText(newfeedbackDataList.get(position).getRating());
                            TextView id=dialog.findViewById( R.id.id);
                            id.setText(newfeedbackDataList.get(position).getId());
                            TextView comment=dialog.findViewById(R.id.comment);
                            comment.setText(newfeedbackDataList.get(position).getCustomerReview());

                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            if (!activity.isFinishing()) {
                                dialog.show();
                                exclusive.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        exclusive.setBackground(context.getDrawable(R.drawable.exclusivebackground));
                                        exclusive.setTextColor(context.getResources().getColor(R.color.txtcolorwhite));
                                        price2=response.body().getData().getExclusivePrice();
                                        price1="";
                                        price3="";
                                        ptype="2";
                                        nonexclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        nonexclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                        competitive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        competitive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                    }
                                });
                                nonexclusive.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        nonexclusive.setBackground(context.getDrawable(R.drawable.exclusivebackground));
                                        nonexclusive.setTextColor(context.getResources().getColor(R.color.txtcolorwhite));
                                        price1=response.body().getData().getNonExclusivePrice();
                                        price3="";
                                        price2="";
                                        ptype="1";
                                        exclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        exclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                        competitive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        competitive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                    }
                                });
                                competitive.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        competitive.setBackground(context.getDrawable(R.drawable.exclusivebackground));
                                        competitive.setTextColor(context.getResources().getColor(R.color.txtcolorwhite));
                                        price3=response.body().getData().getComptitivePrice();
                                        price2="";
                                        price1="";
                                        ptype="3";
                                        exclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        exclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                        nonexclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        nonexclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                    }
                                });
                                btnpurchase.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        progressDialog.setContentView(R.layout.loadingdialog);
                                        progressDialog.setCancelable(false);
                                        final Activity activity=(Activity)context;
                                        if (!activity.isFinishing())
                                            progressDialog.show();
                                        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                                        Call<PurchaseResponse>call1=apiInterface.requestPurchase(newfeedbackDataList.get(position).getId(),ptype,price1,price2,price3,sharedPreferenceData.getId(),"1");
                                        call1.enqueue(new Callback<PurchaseResponse>() {
                                            @Override
                                            public void onResponse(Call<PurchaseResponse> call, Response<PurchaseResponse> response) {
                                                if (response.code()==200)
                                                {
                                                    progressDialog.dismiss();
                                                    Toast.makeText(context, "Purchased", Toast.LENGTH_SHORT).show();
                                                    dialog.dismiss();
                                                    final Dialog purchasedialog=new Dialog(context);
                                                    purchasedialog.setContentView(R.layout.custommessagedialog);
                                                    purchasedialog.show();
                                                    purchasedialog.setCancelable(false);
                                                    TextView ok=purchasedialog.findViewById(R.id.ok);
                                                    TextView message=purchasedialog.findViewById(R.id.message);
                                                    message.setText("Purchase Successfull");
                                                    ok.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            purchasedialog.dismiss();
                                                            ((Activity) context).finish();
                                                        }
                                                    });


                                                }
                                                else
                                                {
                                                    progressDialog.dismiss();
                                                    Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<PurchaseResponse> call, Throwable t) {

                                            }
                                        });
                                    }
                                });
                            }
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(context, "Error Goes!! Try After Some time", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<PriceListResponse> call, Throwable t) {

                    }
                });


            }
        });
        holder.added.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);
                final Activity activity=(Activity)context;
                if (!activity.isFinishing())
                    progressDialog.show();
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                Call<PriceListResponse>call=apiInterface.getPrice();
                call.enqueue(new Callback<PriceListResponse>() {
                    @Override
                    public void onResponse(Call<PriceListResponse> call, final Response<PriceListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            dialog = new Dialog(context);
                            dialog.setContentView(R.layout.purchase);
                            TextView btnpurchase=dialog.findViewById(R.id.btnpurchase);
                            btnpurchase.setText("Add to Cart");
                            final TextView exclusive=dialog.findViewById(R.id.exclusive);
                            exclusive.setText("Exclusive-$"+response.body().getData().getExclusivePrice());
                            final TextView nonexclusive=dialog.findViewById(R.id.nonexclusive);
                            nonexclusive.setText("Non-Exclusive-$"+response.body().getData().getNonExclusivePrice());
                            final TextView competitive=dialog.findViewById(R.id.competitive);
                            competitive.setVisibility(View.GONE);
                            competitive.setText("competitive-$"+response.body().getData().getComptitivePrice());
                            TextView businessName=dialog.findViewById(R.id.businessName);
                            businessName.setText(newfeedbackDataList.get(position).getName());
                            TextView vincity=dialog.findViewById(R.id.vincity);
                            vincity.setText(newfeedbackDataList.get(position).getVicinity());
                            TextView rating=dialog.findViewById(R.id.rating);
                            rating.setText(newfeedbackDataList.get(position).getRating());
                            TextView id=dialog.findViewById( R.id.id);
                            id.setText(newfeedbackDataList.get(position).getId());
                            TextView comment=dialog.findViewById(R.id.comment);
                            comment.setText(newfeedbackDataList.get(position).getCustomerReview());

                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            if (!activity.isFinishing()) {
                                dialog.show();
                                exclusive.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        exclusive.setBackground(context.getDrawable(R.drawable.exclusivebackground));
                                        exclusive.setTextColor(context.getResources().getColor(R.color.txtcolorwhite));
                                        price2=response.body().getData().getExclusivePrice();
                                        price1="";
                                        price3="";
                                        ptype="2";
                                        nonexclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        nonexclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                        competitive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        competitive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                    }
                                });
                                nonexclusive.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        nonexclusive.setBackground(context.getDrawable(R.drawable.exclusivebackground));
                                        nonexclusive.setTextColor(context.getResources().getColor(R.color.txtcolorwhite));
                                        price1=response.body().getData().getNonExclusivePrice();
                                        price3="";
                                        price2="";
                                        ptype="1";
                                        exclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        exclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                        competitive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        competitive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                    }
                                });
                                competitive.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        competitive.setBackground(context.getDrawable(R.drawable.exclusivebackground));
                                        competitive.setTextColor(context.getResources().getColor(R.color.txtcolorwhite));
                                        price3=response.body().getData().getComptitivePrice();
                                        price2="";
                                        price1="";
                                        ptype="3";
                                        exclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        exclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                        nonexclusive.setBackground(context.getDrawable(R.drawable.nonexclusive));
                                        nonexclusive.setTextColor(context.getResources().getColor(R.color.txtcolor));
                                    }
                                });
                                btnpurchase.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        progressDialog.setContentView(R.layout.loadingdialog);
                                        progressDialog.setCancelable(false);
                                        final Activity activity=(Activity)context;
                                        if (!activity.isFinishing())
                                            progressDialog.show();
                                        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                                        Call<AddToCartResponse>call1=apiInterface.addTocart(newfeedbackDataList.get(position).getId(),sharedPreferenceData.getId(),ptype);
                                        call1.enqueue(new Callback<AddToCartResponse>() {
                                            @Override
                                            public void onResponse(Call<AddToCartResponse> call, Response<AddToCartResponse> response) {
                                                if (response.code()==200)
                                                {
                                                    progressDialog.dismiss();
                                                    Toast.makeText(context, "Added To cart", Toast.LENGTH_SHORT).show();
                                                    final Dialog purchasedialog=new Dialog(context);
                                                    purchasedialog.setContentView(R.layout.custommessagedialog);
                                                    purchasedialog.show();
                                                    purchasedialog.setCancelable(false);
                                                    TextView ok=purchasedialog.findViewById(R.id.ok);
                                                    TextView message=purchasedialog.findViewById(R.id.message);
                                                    message.setText("Added To Cart");
                                                    ok.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            newfeedbackDataList.remove(holder.getAdapterPosition());
                                                           notifyItemRemoved(holder.getAdapterPosition());
                                                           notifyDataSetChanged();
                                                            purchasedialog.dismiss();
                                                        }
                                                    });
                                                    dialog.dismiss();

                                                }
                                                else
                                                {
                                                    progressDialog.dismiss();
                                                    Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<AddToCartResponse> call, Throwable t) {

                                            }
                                        });
                                    }
                                });
                            }
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(context, "Error Goes!! Try After Some time", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<PriceListResponse> call, Throwable t) {

                    }
                });


            }
        });
        holder.wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);
                final Activity activity=(Activity)context;
                if (!activity.isFinishing())
                    progressDialog.show();
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                Call<AddWishListResponse>call=apiInterface.addToWishList(sharedPreferenceData.getId(),newfeedbackDataList.get(position).getId()) ;
                call.enqueue(new Callback<AddWishListResponse>() {
                    @Override
                    public void onResponse(Call<AddWishListResponse> call, Response<AddWishListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            Toast.makeText(context, "Added to WishList", Toast.LENGTH_SHORT).show();
                            final Dialog purchasedialog=new Dialog(context);
                            purchasedialog.setContentView(R.layout.custommessagedialog);
                            purchasedialog.show();
                            purchasedialog.setCancelable(false);
                            TextView ok=purchasedialog.findViewById(R.id.ok);
                            TextView message=purchasedialog.findViewById(R.id.message);
                            message.setText("Added To WishList");
                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    purchasedialog.dismiss();
                                    holder.wishlist_added.setVisibility(View.VISIBLE);
                                    holder.wishlist.setVisibility(View.GONE);
                                }
                            });

                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddWishListResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);
                Activity activity=(Activity)context;
                if (!activity.isFinishing()) {
                    progressDialog.show();
                }
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                Call<ViewDetailResponse> call=apiInterface.getDetail(newfeedbackDataList.get(position).getId());
                call.enqueue(new Callback<ViewDetailResponse>() {
                    @Override
                    public void onResponse(Call<ViewDetailResponse> call, Response<ViewDetailResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            DetailData detailData=response.body().getData();
                            Bundle bundle=new Bundle();
                            bundle.putSerializable("data",detailData);
                            Intent intent=new Intent(context, ViewReviewDetail.class);
                            intent.putExtra("bundle",bundle);
                            context.startActivity(intent);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<ViewDetailResponse> call, Throwable t) {

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return newfeedbackDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout detail;
        RelativeLayout purchase;
TextView id,date,businessName,rating,vincity,comment,added,btnpurchase;
ImageView wishlist,wishlist_added;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            wishlist_added=itemView.findViewById(R.id.wishlist_added);
            btnpurchase=itemView.findViewById(R.id.btnpurchase);
            detail =itemView.findViewById(R.id.linear);
            purchase=itemView.findViewById(R.id.purchase);
            wishlist=itemView.findViewById(R.id.wishlist);
            added=itemView.findViewById(R.id.added);
            comment=itemView.findViewById(R.id.comment);
            vincity=itemView.findViewById(R.id.vincity);
            id=itemView.findViewById(R.id.id);
            date=itemView.findViewById(R.id.date);
            businessName=itemView.findViewById(R.id.businessName);
            rating=itemView.findViewById(R.id.rating);
        }
    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }
}
