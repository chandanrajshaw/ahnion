package kashyap.chandan.go_feedback.EnterprizePannel.enterprizeFragment;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminPublished;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PaidCountDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PublishCountDataItem;
import kashyap.chandan.go_feedback.EnterprizePannel.EnterPrizePublished;
import kashyap.chandan.go_feedback.R;

public class EnterPrizePublishAdapter extends RecyclerView.Adapter<EnterPrizePublishAdapter.MyViewHolder>{
    Context context;
    List<PublishCountDataItem> publishCountDataItem;
    List<PublishCountDataItem> filterResult=new ArrayList<>();
    public EnterPrizePublishAdapter(Context context, List<PublishCountDataItem> publishCountDataItem) {
        this.context=context;
        this.publishCountDataItem=publishCountDataItem;
        this.filterResult=publishCountDataItem;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.published_business_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.tvBusinnessName.setText(publishCountDataItem.get(position).getName());
        holder.tvcount.setText(publishCountDataItem.get(position).getCount());
        holder.allDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, EnterPrizePublished.class);
                intent.putExtra("id",publishCountDataItem.get(position).getBId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return publishCountDataItem.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvcount,tvBusinnessName;
        RelativeLayout allDetails;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            allDetails=itemView.findViewById(R.id.allDetails);
            tvBusinnessName=itemView.findViewById(R.id.tvBusinnessName);
            tvcount=itemView.findViewById(R.id.tvcount);
        }
    }
    public void updateList(List<PublishCountDataItem> myList) {
        publishCountDataItem=new ArrayList<>();
        publishCountDataItem.addAll(myList);
        notifyDataSetChanged();

    }
}
