package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import com.google.gson.annotations.SerializedName;

public class RejectionReasonResponse{

	@SerializedName("data")
	private RejectionReasonData data;

	@SerializedName("status")
	private RejectionStatus status;

	public RejectionReasonData getData(){
		return data;
	}

	public RejectionStatus getStatus(){
		return status;
	}
}