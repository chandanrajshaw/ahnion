package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class MyPurchaseListResponse{

	@SerializedName("data")
	private List<MyPurchaseListDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<MyPurchaseListDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}