package kashyap.chandan.go_feedback.EnterprizePannel.Responses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class MyPurchaseResponse{

	@SerializedName("data")
	private List<MyPurchaseDataItem> data;

	@SerializedName("status")
	private AddCartStatus status;

	public List<MyPurchaseDataItem> getData(){
		return data;
	}

	public AddCartStatus getStatus(){
		return status;
	}
}