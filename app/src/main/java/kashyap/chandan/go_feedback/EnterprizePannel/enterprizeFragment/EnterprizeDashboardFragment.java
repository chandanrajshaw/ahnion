package kashyap.chandan.go_feedback.EnterprizePannel.enterprizeFragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.BusinessPublishCountResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PaidCountDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PublishCountDataItem;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class EnterprizeDashboardFragment extends Fragment {
    List<PublishCountDataItem> feedbackDataList=new ArrayList<>();
    RecyclerView recyclerview;
    EnterPrizePublishAdapter recyclerAdapter;
    Dialog progressDialog;
    EditText search_location_box;
    List<PublishCountDataItem> myList;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.enterprizedashboard_fragment,container,false);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        recyclerview.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

    }

    private void init(View view) {
        recyclerview=view.findViewById(R.id.enterprizeFeedbackrecycler);
        progressDialog=new Dialog(getActivity());
        search_location_box=view.findViewById(R.id.search_location_box);
    }

    @Override
    public void onResume() {
        super.onResume();
        getFeedbacks();
        search_location_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String userInput=charSequence.toString();
                myList=new ArrayList<>();
                for (PublishCountDataItem data:feedbackDataList)
                {
                    if (data.getName().toLowerCase().startsWith(userInput.toLowerCase()))
                    {
//                        recyclerAdapter.getFilter().filter(userInput);
                        myList.add(data);
                    }
                }
                recyclerAdapter.updateList(myList);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private  void getFeedbacks()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<BusinessPublishCountResponse> call=apiInterface.publishCount();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<BusinessPublishCountResponse>() {
                    @Override
                    public void onResponse(Call<BusinessPublishCountResponse> call, Response<BusinessPublishCountResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            feedbackDataList=response.body().getData();
                            recyclerAdapter=new EnterPrizePublishAdapter(getActivity(),feedbackDataList);
                            recyclerview.setAdapter(recyclerAdapter);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<BusinessPublishCountResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }

}
