package kashyap.chandan.go_feedback;

import com.google.gson.annotations.SerializedName;

public class ViewDetailResponse{

	@SerializedName("data")
	private DetailData data;

	@SerializedName("status")
	private Status status;

	public DetailData getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}