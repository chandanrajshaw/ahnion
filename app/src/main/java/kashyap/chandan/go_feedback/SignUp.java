package kashyap.chandan.go_feedback;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.go_feedback.ResponseClasses.RegistrationResponse;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;


public class SignUp extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
  static {
      System.loadLibrary("api-keys");
  }
 public native static String getSiteKey();
    public native static String getSecretKey();

    private static final String TAG ="TAG" ;
    EditText cntrycode,et_phone;

private static final String SITE_KEY=getSiteKey();
    private static final String SECRET_KEY=getSecretKey();
TextInputEditText et_first_name,et_email,et_pin,et_confirm_pin,et_Last_name;
TextView btnRegister,signin;
Dialog progressDialog,cameradialog;
CheckBox chkRobot;
FrameLayout profilePic;
boolean verified=false;
    private String picturePath;
    Bitmap converetdImage;
    File image=null;
    ImageView iv_back;
    CircleImageView profileImage;
    MultipartBody.Part profileImg=null;

    private GoogleApiClient mGoogleApiClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        init();
        cntrycode.setText("+1");
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBtnRegister();
            }
        });
        profilePic.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                chooseProfilePic();
            }
        });
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           finish();
            }
        });
        chkRobot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                {
                    if (!verified) {
                        getCaptcha();
                    }
                    else
                    {
                        chkRobot.setText("Verified!You are not Robot");
                    }
                }
                else {
                    chkRobot.setText("Verify you are not Robot?");
                    chkRobot.setChecked(false);
                }

            }
        });
    }
    private void init() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(SafetyNet.API)
                .addConnectionCallbacks(SignUp.this)
                .addOnConnectionFailedListener(SignUp.this)
                .build();
        iv_back=findViewById(R.id.iv_back);
        mGoogleApiClient.connect();
        profileImage=findViewById(R.id.profileImage);
        profilePic=findViewById(R.id.profilePic);
        chkRobot=findViewById(R.id.chkRobot);
        cntrycode=findViewById(R.id.cntrycode);
        et_phone=findViewById(R.id.et_phone);
        et_Last_name=findViewById(R.id.et_Last_name);
        et_first_name =findViewById(R.id.et_first_name);
        et_email=findViewById(R.id.et_email);
        et_pin=findViewById(R.id.et_pin);
        et_confirm_pin=findViewById(R.id.et_confirm_pin);
        btnRegister=findViewById(R.id.btnRegister);
        signin=findViewById(R.id.signin);
    }


@RequiresApi(api = Build.VERSION_CODES.M)
private void chooseProfilePic()
{
    if (ContextCompat.checkSelfPermission(SignUp.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
            ContextCompat.checkSelfPermission(SignUp.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
            ||ContextCompat.checkSelfPermission(SignUp.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {

        askPermission();
    }
    else
        {

        ImageView camera, folder;
        cameradialog = new Dialog(SignUp.this);
        cameradialog.setContentView(R.layout.dialogboxcamera);
        DisplayMetrics metrics=getResources().getDisplayMetrics();
        int width=metrics.widthPixels;
        cameradialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
        cameradialog.show();
        cameradialog.setCancelable(true);
        cameradialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = cameradialog.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        camera = cameradialog.findViewById(R.id.camera);
        folder = cameradialog.findViewById(R.id.gallery);
        folder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);
                cameradialog.dismiss();
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 101);
                cameradialog.dismiss();
            }
        });
    }
}
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (resultCode == RESULT_OK) {

            if (requestCode == 100  && resultData != null) {

//the image URI
                Uri selectedImage = resultData.getData();

                //     imagepath=selectedImage.getPath();

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();


                if (picturePath != null && !picturePath.equals("")) {
                    image = new File(picturePath);
                }

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    converetdImage = getResizedBitmap(bitmap, 500);
                    profileImage.setImageBitmap(converetdImage);
                    profileImage.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else if (requestCode == 101 ) {
                Bitmap converetdImage = (Bitmap) resultData.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                profileImage.setImageBitmap(converetdImage);
                profileImage.setVisibility(View.VISIBLE);
                image = new File(Environment.getExternalStorageDirectory(), "ProfileImage.jpg");
                FileOutputStream fo;
                try {
                    fo = new FileOutputStream(image);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }
    }
    private void setBtnRegister()
    {
        final String firstName= et_first_name.getText().toString();
        String lastName=et_Last_name.getText().toString();
        final String phone=et_phone.getText().toString();
        final String email=et_email.getText().toString();
        final String  password=et_pin.getText().toString();
        String conPassword=et_confirm_pin.getText().toString();
        if (firstName.isEmpty()&&lastName.isEmpty()&&email.isEmpty()&&phone.isEmpty()&&password.isEmpty()&&conPassword.isEmpty())
            Toast.makeText(this, "Fill All the Fields", Toast.LENGTH_SHORT).show();
        else if (firstName.isEmpty())
            Toast.makeText(this, "Enter First Name", Toast.LENGTH_SHORT).show();
        else if (lastName.isEmpty())
            Toast.makeText(this, "Enter Last Name", Toast.LENGTH_SHORT).show();
        else if (!emailValidation(email))
            Toast.makeText(this, "Enter Email/Valid Email", Toast.LENGTH_SHORT).show();
        else if (phone.isEmpty()||phone.length()!=10)
            Toast.makeText(this, "Enter Phone/Valid Phone", Toast.LENGTH_SHORT).show();
        else if (password.isEmpty()||password.length()!=8)
            Toast.makeText(this, "Enter Pin/Invalid Pin", Toast.LENGTH_SHORT).show();
        else if (conPassword.isEmpty()||conPassword.length()!=8)
            Toast.makeText(this, "Re-Enter Pin/Invalid Pin", Toast.LENGTH_SHORT).show();
        else if (!conPassword.equalsIgnoreCase(password))
            Toast.makeText(this, "Pin Miss-match", Toast.LENGTH_SHORT).show();
        else if (!(chkRobot.isChecked())||!verified)
        {
            chkRobot.requestFocus();
            chkRobot.setChecked(false);
            Toast.makeText(this, "Veryify You are Not a Robot", Toast.LENGTH_SHORT).show();
        }
        else
        {

            if (image!=null)
            {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                profileImg = MultipartBody.Part.createFormData("image", image.getName(), requestFile);
            }
            else {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                profileImg = MultipartBody.Part.createFormData("image", "", requestFile);
            }
            RequestBody fname = RequestBody.create(MediaType.parse("multipart/form-data"),firstName);
            RequestBody lname = RequestBody.create(MediaType.parse("multipart/form-data"),lastName);
            RequestBody mob = RequestBody.create(MediaType.parse("multipart/form-data"),phone);
            RequestBody semail = RequestBody.create(MediaType.parse("multipart/form-data"),email);
            RequestBody spass = RequestBody.create(MediaType.parse("multipart/form-data"),password);


            progressDialog=new Dialog(SignUp.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Call<RegistrationResponse>call=apiInterface.register(fname,lname,semail,mob,spass,profileImg);
            call.enqueue(new Callback<RegistrationResponse>() {
                @Override
                public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                    if (response.code()==200)
                    {
                        String id=String.valueOf(response.body().getUser_id());
                        progressDialog.dismiss();
                        Toast.makeText(SignUp.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(SignUp.this,OtpAuthentication.class);
                        intent.putExtra("id",id);
                        intent.putExtra("flow","signUp");
                        startActivity(intent);
                    }
                    else
                    {
                        progressDialog.dismiss();
                        Converter<ResponseBody, ApiError> converter =
                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            Toast.makeText(SignUp.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(SignUp.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    public boolean emailValidation(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void askPermission() {
        if (ContextCompat.checkSelfPermission(SignUp.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(SignUp.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                ||ContextCompat.checkSelfPermission(SignUp.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},100);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int length=0;
        switch (requestCode)
        {
            case 100:
                length= grantResults.length;
                if (grantResults.length>0)
                {
                    for (int i=0;i<length;i++)
                    {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            // Granted. Start getting the location information
                        }
                        else if(grantResults[i]==PackageManager.PERMISSION_DENIED) {
                            {
                                final AlertDialog.Builder builder=new AlertDialog.Builder(SignUp.this,R.style.AlertDialogTheme);
                                builder.setTitle("Notice");
                                builder.setMessage("You Had Denied the Necessary Permissions.Please Go to app Setting and give All the permissions");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent=new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                        intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                                        startActivity(intent);
                                    }
                                });
                                builder.show();
                                break;
                            }

                        }

                    }

                }
//                break;
        }
    }
    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
private  void getCaptcha()
{
SafetyNet.SafetyNetApi.verifyWithRecaptcha(mGoogleApiClient,SITE_KEY)
        .setResultCallback(new ResultCallback<SafetyNetApi.RecaptchaTokenResult>() {
            @Override
            public void onResult(@NonNull SafetyNetApi.RecaptchaTokenResult recaptchaTokenResult) {
                Status status=recaptchaTokenResult.getStatus();
                if ((status != null) && status.isSuccess())
                {
                    chkRobot.setChecked(true);
                    verified=true;
                    chkRobot.setText("Verified! You are Not Robot");
                }
                else
                {
                    chkRobot.setChecked(false);
                    verified=false;
                    Toast.makeText(SignUp.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
}

    @Override
    public void onConnected(@Nullable Bundle bundle) {
//        Toast.makeText(this, "onConnected()", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
//        Toast.makeText(this,
//                "onConnectionSuspended: " + i,
//                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//        Toast.makeText(this,
//                "onConnectionFailed():\n" + connectionResult.getErrorMessage(),
//                Toast.LENGTH_LONG).show();
    }
}