package kashyap.chandan.go_feedback;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.ResponseClasses.DeleteImageResponse;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {
    Context context;
    List<String> images=new ArrayList<>();
    Dialog progressDialog;
    String id;
    CustomItemClickListener customItemClickListener;
    SharedPreferenceData sharedPreferenceData;
    public ImageAdapter(Context context, List<String> images, CustomItemClickListener customItemClickListener) {
        this.context=context;
        this.images=images;
        this.customItemClickListener=customItemClickListener;
        progressDialog=new Dialog(context);
        sharedPreferenceData=new SharedPreferenceData(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.rec_image,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
    Picasso.get().load(ApiClient.IMAGE_URL+"uploads/"+images.get(position)).placeholder(R.drawable.loading).error(R.drawable.terms).into(holder.imageView);
    holder.remove.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           customItemClickListener.onItemClick(view,String.valueOf(holder.getAdapterPosition()),"");
        }
    });
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView,remove;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            remove=itemView.findViewById(R.id.remove);

            imageView=itemView.findViewById(R.id.imageView);
        }
    }
}
