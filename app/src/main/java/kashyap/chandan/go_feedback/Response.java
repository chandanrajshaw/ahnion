package kashyap.chandan.go_feedback;

import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("status")
	private Status status;

	public Status getStatus(){
		return status;
	}
}