package kashyap.chandan.go_feedback.customerPannel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.RedemptionDataItem;

public class AllRedemptionAdapter extends RecyclerView.Adapter<AllRedemptionAdapter.MyViewHolder> {
    Context context;
    List<RedemptionDataItem> data;
    String account;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    String date;
    public AllRedemptionAdapter(Context context, List<RedemptionDataItem> data) {
        this.context=context;
        this.data=data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.all_redemption_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
holder.amount.setText("-$"+data.get(position).getAmount());
System.out.println(data.get(position).getDatetime());
        try {
       holder.tvdate.setText(formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,data.get(position).getDatetime().substring(0,10)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (Integer.parseInt(data.get(position).getStatus())==0)
        {
          holder.status.setText("Pending");
        }
       else if (Integer.parseInt(data.get(position).getStatus())==1)
        {
            holder.status.setText("Accepted");
        }
        account=getLastfour(data.get(position).getCustomerAccountNo());
        holder.message.setText("Payment of $"+data.get(position).getAmount()+" was sent to your bank account ending with xxxx"+
                account+". You’ll receive a confirmation message once payment credited into your account.");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView amount,message,status,tvdate;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvdate=itemView.findViewById(R.id.tvdate);
            status=itemView.findViewById(R.id.status);
            amount=itemView.findViewById(R.id.amount);
            message=itemView.findViewById(R.id.message);
        }
    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }
    private String getLastfour(String input)
    {
        String lastFourDigits;
        if (input.length() > 4)
        {
            lastFourDigits = input.substring(input.length() - 4);
        }
        else
        {
            lastFourDigits = input;
        }
        return lastFourDigits;
    }
}
