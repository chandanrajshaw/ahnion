package kashyap.chandan.go_feedback.customerPannel.maps;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import kashyap.chandan.go_feedback.R;


/**
 * Created by anupamchugh on 01/03/17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {


    private Context context;
    private List<Result> results;
    public RecyclerViewAdapter(Context context,  List<Result> results) {

       this.context=context;
       this.results=results;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.store_list_row, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Result result = results.get(position);
        holder.txtStoreName.setText(result.getName());
        holder.txtStoreAddr.setText(result.getVicinity());
        holder.txtStoreDist.setText(result.getScope());
    
    }


    @Override
    public int getItemCount() {
        return results.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView txtStoreName;
        TextView txtStoreAddr;
        TextView txtStoreDist;
        StoreModel model;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtStoreDist = itemView.findViewById(R.id.txtStoreDist);
            this.txtStoreName = (TextView) itemView.findViewById(R.id.txtStoreName);
            this.txtStoreAddr = (TextView) itemView.findViewById(R.id.txtStoreAddr);


        }


//        public void setData(PlacesPOJO.CustomA info, MyViewHolder holder, StoreModel storeModel) {
//            this.model = storeModel;
//            holder.txtStoreDist.setText(model.distance + "\n" + model.duration);
//            holder.txtStoreName.setText(info.name);
//            holder.txtStoreAddr.setText(info.vicinity);
//
//
//        }

    }
}
