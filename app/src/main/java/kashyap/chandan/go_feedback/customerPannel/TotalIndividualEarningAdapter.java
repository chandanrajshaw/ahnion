package kashyap.chandan.go_feedback.customerPannel;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.go_feedback.CustomItemClickListener;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.IndividualEarningDataItem;

public class TotalIndividualEarningAdapter extends RecyclerView.Adapter<TotalIndividualEarningAdapter.MyViewHolder> {
    Context context;
    List<IndividualEarningDataItem> individualEarningDataItems;
    CustomItemClickListener customItemClickListener;
    public TotalIndividualEarningAdapter(Context context,List<IndividualEarningDataItem> individualEarningDataItems, CustomItemClickListener customItemClickListener) {
        this.context=context;
        this.customItemClickListener=customItemClickListener;
        this.individualEarningDataItems=individualEarningDataItems;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.total_earning_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.amount.setText(individualEarningDataItems.get(position).getBusinessWiseEarn());
        holder.businessName.setText(individualEarningDataItems.get(position).getName());
holder.allDetails.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view)
    {
        Intent intent=new Intent(context,DetailedEarningIndividual.class);
        intent.putExtra("id",individualEarningDataItems.get(position).getBId());
        context.startActivity(intent);
    }
});
    }

    @Override
    public int getItemCount() {
        return individualEarningDataItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout allDetails;
        TextView amount,businessName;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            allDetails=itemView.findViewById(R.id.allDetails);
            amount=itemView.findViewById(R.id.amount);
            businessName=itemView.findViewById(R.id.businessName);
        }
    }
}
