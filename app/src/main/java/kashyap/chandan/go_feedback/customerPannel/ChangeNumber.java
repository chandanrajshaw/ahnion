package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.UpdateProfileResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ChangeNumber extends AppCompatActivity implements View.OnClickListener {
TextView btnChangeNumber;
TextInputEditText oldPhoneNumber,newPhoneNumber;
private Dialog progressDialog;
ImageView iv_back;
SharedPreferenceData sharedPreferenceData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_number);
        init();
        btnChangeNumber.setOnClickListener(this);
        iv_back.setOnClickListener(this);
    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        sharedPreferenceData=new SharedPreferenceData(ChangeNumber.this);
        newPhoneNumber=findViewById(R.id.newPhoneNumber);
        oldPhoneNumber=findViewById(R.id.oldPhoneNumber);
        btnChangeNumber=findViewById(R.id.btnChangeNumber);
        progressDialog=new Dialog(ChangeNumber.this);
    }
    private void setBtnChangeNumber()
    {
       String oldNumber=oldPhoneNumber.getText().toString();
       final String newNumber=newPhoneNumber.getText().toString();
       if (oldNumber.isEmpty()&&newNumber.isEmpty())
           Toast.makeText(this, "Enter old & new phone number", Toast.LENGTH_SHORT).show();
       else if (oldNumber.isEmpty())
           Toast.makeText(this, "Enter Old phone number", Toast.LENGTH_SHORT).show();
       else if (oldNumber.length()!=10)
           Toast.makeText(this, "Enter Valid old Phone number", Toast.LENGTH_SHORT).show();
       else if (newNumber.isEmpty())
           Toast.makeText(this, "Enter New Phone Number", Toast.LENGTH_SHORT).show();
       else if (newNumber.length()!=10)
           Toast.makeText(this, "Enter Valid new Phone Number", Toast.LENGTH_SHORT).show();
       else if (newNumber.equals(oldNumber)|| newNumber.equalsIgnoreCase(sharedPreferenceData.getPhone()))
           Toast.makeText(this, "Try Different Phone Number", Toast.LENGTH_SHORT).show();
       else
       {
           progressDialog.setContentView(R.layout.loadingdialog);
           progressDialog.setCancelable(false);
           progressDialog.show();
           MultipartBody.Part profileImg=null;
           RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
           profileImg = MultipartBody.Part.createFormData("image", "", requestFile);
           RequestBody fname = RequestBody.create(MediaType.parse("multipart/form-data"),sharedPreferenceData.getFName());
           RequestBody lname = RequestBody.create(MediaType.parse("multipart/form-data"),sharedPreferenceData.getLName());
           RequestBody phone = RequestBody.create(MediaType.parse("multipart/form-data"),newNumber);
           RequestBody email = RequestBody.create(MediaType.parse("multipart/form-data"),sharedPreferenceData.getEmail());
           RequestBody id = RequestBody.create(MediaType.parse("multipart/form-data"),sharedPreferenceData.getId());
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
           final Call<UpdateProfileResponse>call=apiInterface.updateProfile(id,fname,lname,email,phone,profileImg);
           Runnable runnable=new Runnable() {
               @Override
               public void run() {
                   call.enqueue(new Callback<UpdateProfileResponse>() {
                       @Override
                       public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                           if (response.code()==200)
                           {
                               progressDialog.dismiss();
                               Toast.makeText(ChangeNumber.this, "Number Changed Successfully", Toast.LENGTH_SHORT).show();
                               sharedPreferenceData.putSharedPreference(sharedPreferenceData.getId(),sharedPreferenceData.getFName(),
                                       sharedPreferenceData.getLName(),newNumber,sharedPreferenceData.getRole(),sharedPreferenceData.getEmail()
                               ,sharedPreferenceData.getImage(),sharedPreferenceData.getDate());
                               finish();
                           }
                           else
                           {
                               progressDialog.dismiss();
                               Converter<ResponseBody, ApiError> converter =
                                       ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                               ApiError error;
                               try {
                                   error = converter.convert(response.errorBody());
                                   ApiError.StatusBean status=error.getStatus();
                                   Toast.makeText(ChangeNumber.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                               } catch (IOException e) { e.printStackTrace(); }
                           }
                       }

                       @Override
                       public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                        progressDialog.dismiss();
                           Toast.makeText(ChangeNumber.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                       }
                   });
               }
           };
           Thread thread=new Thread(runnable);
           thread.start();

       }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnChangeNumber:
setBtnChangeNumber();
                break;
            case R.id.iv_back:
                finish();
                break;
        }
    }
}