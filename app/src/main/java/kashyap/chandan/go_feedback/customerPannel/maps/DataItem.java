package kashyap.chandan.go_feedback.customerPannel.maps;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("amount")
	private String amount;

	@SerializedName("datetime")
	private String datetime;

	@SerializedName("name")
	private String name;

	@SerializedName("rating")
	private String rating;

	@SerializedName("feedback_id")
	private String feedbackId;

	@SerializedName("vicinity")
	private String vicinity;

	@SerializedName("customer_review")
	private String customerReview;

	public String getAmount(){
		return amount;
	}

	public String getDatetime(){
		return datetime;
	}

	public String getName(){
		return name;
	}

	public String getRating(){
		return rating;
	}

	public String getFeedbackId(){
		return feedbackId;
	}

	public String getVicinity(){
		return vicinity;
	}

	public String getCustomerReview(){
		return customerReview;
	}
}