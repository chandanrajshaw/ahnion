package kashyap.chandan.go_feedback.customerPannel;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.Manifest;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.Constants;
import kashyap.chandan.go_feedback.MainActivity;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.RealPathUtil;
import kashyap.chandan.go_feedback.ResponseClasses.SendDraftResponse;
import kashyap.chandan.go_feedback.ResponseClasses.SendFeedbackResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;

import kashyap.chandan.go_feedback.customerPannel.maps.Result;
import kashyap.chandan.go_feedback.customerPannel.searchlocation.ResultsItem;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class FeedbackScreen extends AppCompatActivity implements View.OnClickListener {
    TextView btnSendFeedback,btnSendDraft, googlerating, tvAddress, tvname;
    RecyclerView imagelist, receiptRecycler;
    RelativeLayout uploadReceipt,sharePics;
    ArrayList<Uri> feedbackArrayList = new ArrayList<>();
    ArrayList<Uri> uploadReceiptArraylist = new ArrayList<>();
    SimpleRatingBar servicerating, ambiancerating, staffrating, ratingbar;
    List<MultipartBody.Part> UploadReceiptparts = new ArrayList<>();
    List<MultipartBody.Part> FeedbackPicsParts = new ArrayList<>();
    FrameLayout feedbackFrame,receiptFrame;
EditText customer_review;
    private static final int REQUEST_CODE_READ_STORAGE = 101, RECEIPT_CODE = 102,CLICK_RECEIPT=103,CLICK_IMAGE=104;
    Intent intent;
    ImageView image,iv_back,feedbackImage,receiptImage,removereceipt,remove;
String b_id;
SharedPreferenceData sharedPreferenceData;
Dialog progressDialog;
    Result result;
    ResultsItem resultsItems;
    private File imageFeedback,imageReceipt;
    private Dialog cameradialog;
String resultItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_screen);
        init();
        Bundle bundle = intent.getBundleExtra("bundle");
        resultItem=intent.getStringExtra("resultsItem");
        if (resultItem!=null)
        {
            resultsItems = (ResultsItem) bundle.getSerializable("result");
            googlerating.setText(String.valueOf(resultsItems.getRating()));
            tvname.setText(resultsItems.getName());
            tvAddress.setText(resultsItems.getFormattedAddress());
            if (resultsItems.getPhotos()==null||resultsItems.getPhotos().isEmpty()||resultsItems.getPhotos().size()<=0)
            {
                Picasso.get().load(resultsItems.getIcon()).into(image);
            }
            else
            {
                Picasso.get().load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+ resultsItems.getPhotos().get(0).getPhotoReference()+"&key="+Constants.KEY).into(image);

            }
        }
        else
        {
            result = (Result) bundle.getSerializable("result");
            googlerating.setText(String.valueOf(result.getRating()));
            tvname.setText(result.getName());
            tvAddress.setText(result.getVicinity());
            if (result.getPhotos()==null||result.getPhotos().isEmpty()||result.getPhotos().size()<=0)
            {
                Picasso.get().load(result.getIcon()).into(image);

            }
            else
            {
                Picasso.get().load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+ result.getPhotos().get(0).getPhotoReference()+"&key="+ Constants.KEY).into(image);

            }
        }

        b_id=intent.getStringExtra("b_id");
iv_back.setOnClickListener(this);
remove.setOnClickListener(this);
removereceipt.setOnClickListener(this);


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (sharedPreferenceData.getId()==null)
        {
           finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (sharedPreferenceData.getId()==null)
        {
            Intent intent=new Intent(FeedbackScreen.this, MainActivity.class);
            intent.putExtra("user","user");
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        uploadReceipt.setOnClickListener(this);
        btnSendFeedback.setOnClickListener(this);
        btnSendDraft.setOnClickListener(this);
        sharePics.setOnClickListener(this);

    }

    private void showChooser(int CODE) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), CODE);
    }

    private void init() {
        remove=findViewById(R.id.remove);
        removereceipt=findViewById(R.id.removereceipt);
        receiptFrame=findViewById(R.id.receiptFrame);
        feedbackFrame=findViewById(R.id.feedbackFrame);
        feedbackImage=findViewById(R.id.feedbackImage);
        receiptImage=findViewById(R.id.receiptImage);
        iv_back=findViewById(R.id.iv_back);
        customer_review=findViewById(R.id.customer_review);
        receiptRecycler = findViewById(R.id.receiptRecycler);
        image = findViewById(R.id.image);
        intent = getIntent();
        btnSendDraft=findViewById(R.id.btnSendDraft);
        sharedPreferenceData=new SharedPreferenceData(FeedbackScreen.this);
        uploadReceipt = findViewById(R.id.uploadReceipt);
        tvAddress = findViewById(R.id.tvAddress);
        googlerating = findViewById(R.id.googlerating);
        btnSendFeedback = findViewById(R.id.btnSendFeedback);
        imagelist = findViewById(R.id.feedbackpics);
        sharePics = findViewById(R.id.sharePics);
        tvname = findViewById(R.id.tvname);
        servicerating = findViewById(R.id.servicerating);
        ambiancerating = findViewById(R.id.ambiancerating);
        staffrating = findViewById(R.id.staffrating);
        ratingbar = findViewById(R.id.ratingbar);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.remove:
                feedbackFrame.setVisibility(View.GONE);
                break;
            case R.id.removereceipt:
                receiptFrame.setVisibility(View.GONE);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.btnSendFeedback:
                sendFeedback();
                break;
            case R.id.sharePics:
                if (ContextCompat.checkSelfPermission(FeedbackScreen.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(FeedbackScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                  ContextCompat.checkSelfPermission(FeedbackScreen.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        askPermission();
                    }
                }
                else {
                    ImageView camera, folder;
                    cameradialog = new Dialog(FeedbackScreen.this);
                    cameradialog.setContentView(R.layout.dialogboxcamera);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    cameradialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                    cameradialog.show();
                    cameradialog.setCancelable(true);
                    cameradialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Window window = cameradialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    camera = cameradialog.findViewById(R.id.camera);
                    folder = cameradialog.findViewById(R.id.gallery);
                    folder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            feedbackImage.setVisibility(View.GONE);
                            imageFeedback=null;
                            feedbackFrame.setVisibility(View.GONE);
                            showChooser(REQUEST_CODE_READ_STORAGE);
                            cameradialog.dismiss();
                        }
                    });
                    camera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            imagelist.removeAllViewsInLayout();
//                            imagelist.setAdapter(null);
                            imagelist.setVisibility(View.GONE);
//                            feedbackArrayList.clear();
                            feedbackFrame.setVisibility(View.VISIBLE);
                            feedbackImage.setVisibility(View.VISIBLE);
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, 104);
                            cameradialog.dismiss();
                        }
                    });
//                    showChooser(REQUEST_CODE_READ_STORAGE);
                }
                break;
            case R.id.uploadReceipt:
//                uploadReceiptArraylist.clear();
//                receiptRecycler.removeAllViews();
                if (ContextCompat.checkSelfPermission(FeedbackScreen.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(FeedbackScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(FeedbackScreen.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        askPermission();
                    }
                } else
                    {

                        ImageView camera, folder;
                        cameradialog = new Dialog(FeedbackScreen.this);
                        cameradialog.setContentView(R.layout.dialogboxcamera);
                        DisplayMetrics metrics=getResources().getDisplayMetrics();
                        int width=metrics.widthPixels;
                        cameradialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                        cameradialog.show();
                        cameradialog.setCancelable(true);
                        cameradialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        Window window = cameradialog.getWindow();
                        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                        camera = cameradialog.findViewById(R.id.camera);
                        folder = cameradialog.findViewById(R.id.gallery);
                        folder.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                receiptImage.setVisibility(View.GONE);
                                imageReceipt=null;
                                receiptFrame.setVisibility(View.GONE);
                                showChooser(RECEIPT_CODE);
                                cameradialog.dismiss();
                            }
                        });
                        camera.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
//                                receiptRecycler.removeAllViewsInLayout();
//                                receiptRecycler.setAdapter(null);
                                receiptRecycler.setVisibility(View.GONE);
                                receiptImage.setVisibility(View.VISIBLE);
//                                uploadReceiptArraylist.clear();
                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, 103);
                                cameradialog.dismiss();
                            }
                        });
//                    showChooser(RECEIPT_CODE);
                }
                break;
            case R.id.btnSendDraft:
                sendDraft();
                break;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_READ_STORAGE) {
                if (resultData != null) {
                    if (resultData.getClipData() != null) {
                        imagelist.removeAllViews();
                        int count = resultData.getClipData().getItemCount();
                        int currentItem = 0;
                        while (currentItem < count) {
                            Uri imageUri = resultData.getClipData().getItemAt(currentItem).getUri();
                            currentItem = currentItem + 1;
                            Log.d("Uri Selected", imageUri.toString());
                            try {
                                feedbackArrayList.add(imageUri);
                                imagelist.setLayoutManager(new LinearLayoutManager(FeedbackScreen.this, LinearLayoutManager.HORIZONTAL, false));
                                imagelist.setAdapter(new FeedbackImageAdapter(FeedbackScreen.this, feedbackArrayList));
                                imagelist.setVisibility(View.VISIBLE);
                            } catch (Exception e) {
                                Log.e("TAG", "File select error", e);
                            }
                        }
                    } else if (resultData.getData() != null) {
                        final Uri uri = resultData.getData();
                        Log.i("TAG", "Uri = " + uri.toString());
                        try {
                            feedbackArrayList.add(uri);
                            imagelist.setLayoutManager(new LinearLayoutManager(FeedbackScreen.this, LinearLayoutManager.HORIZONTAL, false));
                            imagelist.setAdapter(new FeedbackImageAdapter(FeedbackScreen.this, feedbackArrayList));
                            imagelist.setVisibility(View.VISIBLE);

                        } catch (Exception e) {
                            Log.e("TAG", "File select error", e);
                        }
                    }
                }
            }
            if (requestCode == RECEIPT_CODE) {
                if (resultData != null) {
                    if (resultData.getClipData() != null) {
                        imagelist.removeAllViews();
                        int count = resultData.getClipData().getItemCount();
                        int currentItem = 0;
                        while (currentItem < count) {
                            Uri imageUri = resultData.getClipData().getItemAt(currentItem).getUri();
                            currentItem = currentItem + 1;
                            Log.d("Uri Selected", imageUri.toString());
                            try {
                                uploadReceiptArraylist.add(imageUri);
                                receiptRecycler.setLayoutManager(new LinearLayoutManager(FeedbackScreen.this, LinearLayoutManager.HORIZONTAL, false));
                                receiptRecycler.setAdapter(new UploadReceiptAdapter(FeedbackScreen.this, uploadReceiptArraylist));
                           receiptRecycler.setVisibility(View.VISIBLE);
                            } catch (Exception e) {
                                Log.e("TAG", "File select error", e);
                            }
                        }
                    } else if (resultData.getData() != null) {
                        final Uri uri = resultData.getData();
                        Log.i("TAG", "Uri = " + uri.toString());
                        try {
                            uploadReceiptArraylist.add(uri);
                            receiptRecycler.setLayoutManager(new LinearLayoutManager(FeedbackScreen.this, LinearLayoutManager.HORIZONTAL, false));
                            receiptRecycler.setAdapter(new UploadReceiptAdapter(FeedbackScreen.this, uploadReceiptArraylist));
                            receiptRecycler.setVisibility(View.VISIBLE);

                        } catch (Exception e) {
                            Log.e("TAG", "File select error", e);
                        }
                    }
                }
            }
            else if (requestCode == 103 ) {
                Bitmap converetdImage = (Bitmap) resultData.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                receiptImage.setImageBitmap(converetdImage);
                receiptImage.setVisibility(View.VISIBLE);
                receiptFrame.setVisibility(View.VISIBLE);
                File dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                imageReceipt = new File(dirPath, "receiptImage.jpg");
                try {
                    if(!dirPath.isDirectory()) {
                        dirPath.mkdirs();
                    }
                    imageReceipt.createNewFile();
                }catch (Exception e){}
                FileOutputStream fo;
                try {
                    fo = new FileOutputStream(imageReceipt);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            else if (requestCode == 104 ) {
                Bitmap converetdImage = (Bitmap) resultData.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                feedbackImage.setImageBitmap(converetdImage);
                feedbackImage.setVisibility(View.VISIBLE);
                feedbackFrame.setVisibility(View.VISIBLE);
                File dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                imageFeedback = new File(dirPath, "feedbackImage.jpg");
                try {
                    if(!dirPath.isDirectory()) {
                        dirPath.mkdirs();
                    }
                    imageFeedback.createNewFile();
                }catch (Exception e){}
                FileOutputStream fo;
                try {
                    fo = new FileOutputStream(imageFeedback);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void askPermission() {
        if (ContextCompat.checkSelfPermission(FeedbackScreen.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(FeedbackScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(FeedbackScreen.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA}, 100);
        }
    }

    private void sendFeedback() {
        String comments=customer_review.getText().toString();
        float ser_rating=servicerating.getRating();
        float staf_rating=staffrating.getRating();
        float amb_rating=ambiancerating.getRating();
        float allratingbar=ratingbar.getRating();
        if (allratingbar==0 &&staf_rating==0 &&amb_rating==0 &&ser_rating==0)
            Toast.makeText(this, "Give Ratings", Toast.LENGTH_SHORT).show();
        else if (ser_rating==0)
            Toast.makeText(this, "Give Customer Service Ratings", Toast.LENGTH_SHORT).show();
        else if (staf_rating==0)
            Toast.makeText(this, "Give Product & Service Rating", Toast.LENGTH_SHORT).show();
        else if (amb_rating==0)
            Toast.makeText(this, "Give Money Experience Ratings", Toast.LENGTH_SHORT).show();
        else if (allratingbar==0)
            Toast.makeText(this, "Give Overall Ratings", Toast.LENGTH_SHORT).show();

        else if (comments.isEmpty())
            Toast.makeText(this, "Give Your Experience", Toast.LENGTH_SHORT).show();
       else
        {

            String overAllRating = String.valueOf(ratingbar.getRating());
            String staffRatting = String.valueOf(staffrating.getRating());
            String ambienceRating = String.valueOf(ambiancerating.getRating());
            String servicRating = String.valueOf(servicerating.getRating());
            RequestBody u_id = RequestBody.create(MediaType.parse("multipart/form-data"),sharedPreferenceData.getId() );
            RequestBody id = RequestBody.create(MediaType.parse("multipart/form-data"), b_id);
            RequestBody allRating = RequestBody.create(MediaType.parse("multipart/form-data"), overAllRating);
            RequestBody aRating = RequestBody.create(MediaType.parse("multipart/form-data"), ambienceRating);
            RequestBody sRating = RequestBody.create(MediaType.parse("multipart/form-data"), staffRatting);
            RequestBody serRating = RequestBody.create(MediaType.parse("multipart/form-data"), servicRating);
            RequestBody cmnts = RequestBody.create(MediaType.parse("multipart/form-data"), comments);
            if (uploadReceiptArraylist.isEmpty()&&receiptImage==null)
            {
                MultipartBody.Part uploadReceiptPart = null;
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                uploadReceiptPart = MultipartBody.Part.createFormData("receipt[]", "", requestFile);
                UploadReceiptparts.add(uploadReceiptPart);
            }

            else
            {
                if ((!uploadReceiptArraylist.isEmpty())&&imageReceipt==null)
                {
                    for (Uri uri : uploadReceiptArraylist)
                        UploadReceiptparts.add(prepareFilePart("receipt[]", uri));    //here image is// }
                }
                else if (uploadReceiptArraylist.isEmpty()&&imageReceipt!=null)
                {
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageReceipt);
                    MultipartBody.Part receiptImg = MultipartBody.Part.createFormData("receipt[]", imageReceipt.getName(), requestFile);
                    UploadReceiptparts.add(receiptImg);
                }


            }
            if ((!feedbackArrayList.isEmpty())||imageFeedback!=null)
            {
                if ((!feedbackArrayList.isEmpty())&&imageFeedback==null) {
                    for (Uri feedback : feedbackArrayList)
                        FeedbackPicsParts.add(prepareFilePart("image[]", feedback));
                }
                else if ((feedbackArrayList.isEmpty()&&imageFeedback!=null))
                {
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFeedback);
                    MultipartBody.Part feedbackImg = MultipartBody.Part.createFormData("image[]", imageFeedback.getName(), requestFile);
                    FeedbackPicsParts.add(feedbackImg);
                }
            }
            progressDialog=new Dialog(FeedbackScreen.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            final Call<SendFeedbackResponse>call=apiInterface.sendFeedback(id,u_id,allRating,sRating,aRating,serRating,cmnts,UploadReceiptparts,FeedbackPicsParts);

            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    call.enqueue(new Callback<SendFeedbackResponse>() {
                        @Override
                        public void onResponse(Call<SendFeedbackResponse> call, Response<SendFeedbackResponse> response) {
                            if (response.code()==200)
                            {
                                progressDialog.dismiss();
                                Toast.makeText(FeedbackScreen.this, "Submitted", Toast.LENGTH_SHORT).show();
                                final Dialog dialog=new Dialog(FeedbackScreen.this);
                                dialog.setContentView(R.layout.custommessagedialog);
                                dialog.show();
                                dialog.setCancelable(false);
                                TextView ok=dialog.findViewById(R.id.ok);
                                TextView message=dialog.findViewById(R.id.message);
                                message.setText("Submitted");
                                ok.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent=new Intent(FeedbackScreen.this,LandingScreen.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                        dialog.dismiss();
                                    }
                                });

                            }
                            else
                            {
                                progressDialog.dismiss();
                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                    Toast.makeText(FeedbackScreen.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }}

                        }

                        @Override
                        public void onFailure(Call<SendFeedbackResponse> call, Throwable t) {
                            progressDialog.dismiss();
                        }
                    });
                }
            };
            Thread thread=new Thread(runnable);
            thread.start();
        }

    }

    private void sendDraft() {
        String comments=customer_review.getText().toString();
        float ser_rating=servicerating.getRating();
        float staf_rating=staffrating.getRating();
        float amb_rating=ambiancerating.getRating();
        float allratingbar=ratingbar.getRating();
//        if (allratingbar==0 &&staf_rating==0 &&amb_rating==0 &&ser_rating==0)
//            Toast.makeText(this, "Give Ratings", Toast.LENGTH_SHORT).show();
//        else if (allratingbar==0)
//            Toast.makeText(this, "Give Ratings", Toast.LENGTH_SHORT).show();
//        else if (staf_rating==0)
//            Toast.makeText(this, "Give Staff Rating", Toast.LENGTH_SHORT).show();
//        else if (amb_rating==0)
//            Toast.makeText(this, "Give Ambience Ratings", Toast.LENGTH_SHORT).show();
//        else if (ser_rating==0)
//            Toast.makeText(this, "Give Service Ratings", Toast.LENGTH_SHORT).show();
//        else if (comments.isEmpty())
//            Toast.makeText(this, "Give Your Experience", Toast.LENGTH_SHORT).show();
//       else
//        {
            String overAllRating = String.valueOf(ratingbar.getRating());
            String staffRatting = String.valueOf(staffrating.getRating());
            String ambienceRating = String.valueOf(ambiancerating.getRating());
            String servicRating = String.valueOf(servicerating.getRating());
            RequestBody u_id = RequestBody.create(MediaType.parse("multipart/form-data"),sharedPreferenceData.getId() );
            RequestBody id = RequestBody.create(MediaType.parse("multipart/form-data"), b_id);
            RequestBody allRating = RequestBody.create(MediaType.parse("multipart/form-data"), overAllRating);
            RequestBody aRating = RequestBody.create(MediaType.parse("multipart/form-data"), ambienceRating);
            RequestBody sRating = RequestBody.create(MediaType.parse("multipart/form-data"), staffRatting);
            RequestBody serRating = RequestBody.create(MediaType.parse("multipart/form-data"), servicRating);
            RequestBody cmnts = RequestBody.create(MediaType.parse("multipart/form-data"), comments);

            if (uploadReceiptArraylist.isEmpty()&&receiptImage==null)
            {
                MultipartBody.Part uploadReceiptPart = null;
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                uploadReceiptPart = MultipartBody.Part.createFormData("receipt[]", "", requestFile);
                UploadReceiptparts.add(uploadReceiptPart);
            }

            else
            {
                if ((!uploadReceiptArraylist.isEmpty())&&imageReceipt==null)
                {
                    for (Uri uri : uploadReceiptArraylist)
                        UploadReceiptparts.add(prepareFilePart("receipt[]", uri));    //here image is// }
                }
                else if (uploadReceiptArraylist.isEmpty()&&imageReceipt!=null)
                {
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageReceipt);
                    MultipartBody.Part receiptImg = MultipartBody.Part.createFormData("receipt[]", imageReceipt.getName(), requestFile);
                    UploadReceiptparts.add(receiptImg);
                }


            }
            if ((!feedbackArrayList.isEmpty())||imageFeedback!=null)
            {
                if ((!feedbackArrayList.isEmpty())&&imageFeedback==null) {
                    for (Uri feedback : feedbackArrayList)
                        FeedbackPicsParts.add(prepareFilePart("image[]", feedback));
                }
                else if ((feedbackArrayList.isEmpty()&&imageFeedback!=null))
                {
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFeedback);
                    MultipartBody.Part feedbackImg = MultipartBody.Part.createFormData("image[]", imageFeedback.getName(), requestFile);
                    FeedbackPicsParts.add(feedbackImg);
                }
            }
            progressDialog=new Dialog(FeedbackScreen.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            final Call<SendDraftResponse>call=apiInterface.sendDraft(id,u_id,allRating,sRating,aRating,serRating,cmnts,UploadReceiptparts,FeedbackPicsParts);
            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    call.enqueue(new Callback<SendDraftResponse>() {
                        @Override
                        public void onResponse(Call<SendDraftResponse> call, Response<SendDraftResponse> response) {
                            if (response.code()==200)
                            {
                                progressDialog.dismiss();
                                Toast.makeText(FeedbackScreen.this, "Saved As Draft", Toast.LENGTH_SHORT).show();
                                final Dialog dialog=new Dialog(FeedbackScreen.this);
                                dialog.setContentView(R.layout.custommessagedialog);
                                dialog.show();
                                dialog.setCancelable(false);
                                TextView ok=dialog.findViewById(R.id.ok);
                                TextView message=dialog.findViewById(R.id.message);
                                message.setText("Saved As Draft");
                                ok.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent=new Intent(FeedbackScreen.this,LandingScreen.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                        dialog.dismiss();
                                    }
                                });

                            }
                            else
                            {
                                progressDialog.dismiss();
                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                    Toast.makeText(FeedbackScreen.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }}

                        }

                        @Override
                        public void onFailure(Call<SendDraftResponse> call, Throwable t) {
                            progressDialog.dismiss();
                        }
                    });
                }
            };
            Thread thread=new Thread(runnable);
            thread.start();

//        }




    }
    private MultipartBody.Part prepareFilePart (String partName, Uri fileUri){
        String path = RealPathUtil.getRealPath(FeedbackScreen.this, fileUri);
        File file = new File(path);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }
}