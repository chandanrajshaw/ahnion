package kashyap.chandan.go_feedback.customerPannel;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import kashyap.chandan.go_feedback.R;

public class FeedbackImageAdapter extends RecyclerView.Adapter<FeedbackImageAdapter.MyViewHolder> {
    Context context;
    ArrayList<Uri> arrayList;
    public FeedbackImageAdapter(Context context, ArrayList<Uri> arrayList) {
        this.context=context;
        this.arrayList=arrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.rec_image,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        Picasso.get().load(arrayList.get(position)).into(holder.image);
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arrayList.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image,remove;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            remove=itemView.findViewById(R.id.remove);
            image=itemView.findViewById(R.id.imageView);
        }
    }
}
