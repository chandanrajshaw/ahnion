package kashyap.chandan.go_feedback.customerPannel;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.Constants;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.AddBusinessResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.customerPannel.searchlocation.ResultsItem;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class SearchSelectedPlaceAdapter extends RecyclerView.Adapter<SearchSelectedPlaceAdapter.MyViewHolder> implements Filterable {
    Context context;
    List<ResultsItem> results=new ArrayList<>();
    List<ResultsItem> filterResult=new ArrayList<>();
    private Dialog progressDialog;
    SharedPreferenceData sharedPreferenceData;
    private String reference;

    public SearchSelectedPlaceAdapter(Context context, List<ResultsItem> results) {
        this.context=context;
        this.results=results ;
        filterResult=results;
        sharedPreferenceData=new SharedPreferenceData(context);
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(context).inflate(R.layout.place_detail_layout,parent,false);
        return new MyViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position)
    {
        //         PlacesClient placesClient= (PlacesClient) results.get(position);
        if (filterResult.get(position).getPhotos()==null||filterResult.get(position).getPhotos().isEmpty()) {
           // filterResult.get(position).getPhotos().size()<=0

            Picasso.get().load(filterResult.get(position).getIcon()).into(holder.image);
        }
        else
        {
//            Picasso.get().load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+filterResult.get(position).getPhotos().get(0).getPhotoReference()+"&key="+context.getResources().getString(R.string.googlemap)).into(holder.image);
            Picasso.get().load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+filterResult.get(position).getPhotos().get(0).getPhotoReference()+"&key="+ Constants.KEY).into(holder.image);

        }

        holder.placeName.setText(filterResult.get(position).getName());
        holder.placeVincity.setText(filterResult.get(position).getFormattedAddress());
        holder.placeRating.setText(String.valueOf(filterResult.get(position).getRating()));
        holder.placeType.setText(filterResult.get(position).getTypes().get(1));
//        holder.placedistance.setText(filterResult.get(position).getScope());
        holder.placedistance.setVisibility(View.GONE);
        holder.detail.setOnClickListener(new View.OnClickListener()
        {
    @Override
    public void onClick(View view) {
        final ResultsItem result=filterResult.get(position);
            progressDialog=new Dialog(context);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    final APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                    if (result.getPhotos()==null||result.getPhotos().isEmpty()||result.getPhotos().size()<=0)
                    {
                        reference="";

                    }
                    else
                    {
                        reference=result.getPhotos().get(0).getPhotoReference();
                    }
                    Call<AddBusinessResponse> call1=apiInterface.registerBusiness(result.getPlaceId(),result.getName(),String.valueOf(result.getGeometry().getLocation().getLat()),String.valueOf(result.getGeometry().getLocation().getLng()),result.getReference(),"GOOGLE",result.getFormattedAddress(),String.valueOf(result.getRating()),reference);
                    call1.enqueue(new Callback<AddBusinessResponse>()
                    {
                        @Override
                        public void onResponse(Call<AddBusinessResponse> call, Response<AddBusinessResponse> response) {
                            if (response.code()==200)
                            {
                                progressDialog.dismiss();
//                                Toast.makeText(context, ""+response.message(), Toast.LENGTH_SHORT).show();
//                                if (sharedPreferenceData.getId()!=null)
//                                {
                                    Intent intent=new Intent(context, FeedbackScreen.class);
                                    Bundle bundle=new Bundle();
                                    intent.putExtra("resultsItem","resultsItem");
                                    bundle.putSerializable("result",result);
                                    intent.putExtra("bundle",bundle);
                                    intent.putExtra("b_id",String.valueOf(response.body().getBId()));
                                    context.startActivity(intent);
//                                }
//                                else
//                                {
//                                    Intent intent=new Intent(context, MainActivity.class);
////                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
//                                }

                            }
                            else
                            {
                                progressDialog.dismiss();
                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                    Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }}
                        }

                        @Override
                        public void onFailure(Call<AddBusinessResponse> call, Throwable t) {

                        }
                    });
                }
            };
            Thread thread=new Thread(runnable);
            thread.start();
        }
//    }
});
    }

    @Override
    public int getItemCount() {
        return filterResult.size();
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                if (charSequence == null || charSequence.length() == 0) {
                    filterResult.addAll(results);
                }
                else
                {
                    List<ResultsItem> filteredList=new ArrayList<>();
                    String filterPattern = charSequence.toString().toLowerCase().trim();
                    for (ResultsItem item : results) {
                        if (item.getName().toLowerCase().contains(filterPattern)) {
                            filteredList.add(item);
                        }
                        else if (item.getFormattedAddress().toLowerCase().contains(filterPattern))
                        {
                            filteredList.add(item);
                        }
filterResult=filteredList;
                    }
                }
                FilterResults filter = new FilterResults();
                filter.values = filterResult;
                return filter;

            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                filterResult.addAll((List) filterResults.values);
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout detail;
        TextView placedistance,placeRating,placeName,placeVincity, placeType;
        ImageView image;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            placedistance=itemView.findViewById(R.id.placedistance);
            placeRating=itemView.findViewById(R.id.placeRating);
            placeName=itemView.findViewById(R.id.placeName);
            placeVincity=itemView.findViewById(R.id.placeVincity);
            placeType =itemView.findViewById(R.id.placeType);
            image=itemView.findViewById(R.id.image);
            detail=itemView.findViewById(R.id.detail);
        }
    }
}
