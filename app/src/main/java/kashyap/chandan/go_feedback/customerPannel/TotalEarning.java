package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import kashyap.chandan.go_feedback.CustomItemClickListener;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.CustomerBalanceResponse;
import kashyap.chandan.go_feedback.ResponseClasses.IndividualEarningDataItem;
import kashyap.chandan.go_feedback.ResponseClasses.TotalIndividualEarningResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class TotalEarning extends AppCompatActivity {
RecyclerView totalEarningRecycler;
    private Dialog progressDialog;
    TextView tvavailableBalance,tvTotalEarning;
    SharedPreferenceData sharedPreferenceData;
    List<IndividualEarningDataItem>individualEarningDataItems;
    TotalIndividualEarningAdapter adapter;
    ImageView iv_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_earning);
        init();
        totalEarningRecycler.setLayoutManager(new LinearLayoutManager(TotalEarning.this,LinearLayoutManager.VERTICAL,false));
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        tvTotalEarning=findViewById(R.id.tvTotalEarning);
        tvavailableBalance=findViewById(R.id.tvavailableBalance);
        sharedPreferenceData=new SharedPreferenceData(TotalEarning.this);
        totalEarningRecycler=findViewById(R.id.totalEarningRecycler);
        progressDialog=new Dialog(TotalEarning.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCustomerBalance();
        getBusinessEarning();
    }
    private void getBusinessEarning()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<TotalIndividualEarningResponse> call=apiInterface.getBusinessEarning(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<TotalIndividualEarningResponse>() {
                    @Override
                    public void onResponse(Call<TotalIndividualEarningResponse> call, Response<TotalIndividualEarningResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                           individualEarningDataItems=response.body().getData();

                            totalEarningRecycler.setAdapter(new TotalIndividualEarningAdapter(TotalEarning.this,individualEarningDataItems, new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, String id, String value) {

                                }
                            }));

                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(TotalEarning.this, "No Transaction", Toast.LENGTH_LONG).show();
                                if (response.code()==401)
                                {
                                    tvavailableBalance.setText("$0");
                                }
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<TotalIndividualEarningResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(TotalEarning.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
    private void getCustomerBalance()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<CustomerBalanceResponse> call=apiInterface.getCustomerBalance(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<CustomerBalanceResponse>() {
                    @Override
                    public void onResponse(Call<CustomerBalanceResponse> call, Response<CustomerBalanceResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            tvavailableBalance.setText("$"+response.body().getData().getCurentBal());
                            tvTotalEarning.setText("$"+response.body().getData().getTotalEarn());
                        }
                        else
                        {
                            tvavailableBalance.setText("$0");
                            tvTotalEarning.setText("$0");
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(TotalEarning.this, "No Transaction", Toast.LENGTH_LONG).show();
                                if (response.code()==401)
                                {
                                    tvavailableBalance.setText("$0");
                                }
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<CustomerBalanceResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(TotalEarning.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}