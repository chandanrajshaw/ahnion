package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.AllNotificationResponse;
import kashyap.chandan.go_feedback.ResponseClasses.NotificationItem;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class Notification extends AppCompatActivity implements View.OnClickListener {
ImageView iv_back;
TextView toolHeader,date;
RecyclerView notificationRecycler;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    Dialog progressDialog;
    SharedPreferenceData sharedPreferenceData;
    List<NotificationItem> notificationItems;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        init();
        iv_back.setOnClickListener(this);
        String localDate=getDate();
          try {
           date.setText(formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,localDate));
        } catch (ParseException e) {
          e.printStackTrace();
      }
        notificationRecycler.setLayoutManager(new LinearLayoutManager(Notification.this,LinearLayoutManager.VERTICAL,false));
getNotification();
    }

    private void init() {
        notificationItems=new ArrayList<>();
        date=findViewById(R.id.date);
        sharedPreferenceData=new SharedPreferenceData(Notification.this);
        progressDialog=new Dialog(Notification.this);
        iv_back=findViewById(R.id.iv_back);
        toolHeader=findViewById(R.id.toolHeader);
        notificationRecycler=findViewById(R.id.notificationRecycler);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_back:
                finish();
                break;
        }
    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }
    private String   getDate()
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(cal.getTime());
    }
    private void getNotification()
    {
progressDialog.setContentView(R.layout.loadingdialog);
progressDialog.setCancelable(false);
progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<AllNotificationResponse>call=apiInterface.getAllNotifications(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
call.enqueue(new Callback<AllNotificationResponse>() {
    @Override
    public void onResponse(Call<AllNotificationResponse> call, Response<AllNotificationResponse> response) {
        if (response.code()==200)
        {
            progressDialog.dismiss();
            notificationItems=response.body().getNotification();
            notificationRecycler.setAdapter(new NotificationAdapter(Notification.this,notificationItems));
        }
        else
        {
            progressDialog.dismiss();
            Converter<ResponseBody, ApiError> converter =
                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
            ApiError error;
            try {
                error = converter.convert(response.errorBody());
                ApiError.StatusBean status=error.getStatus();
                Toast.makeText(Notification.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
            } catch (IOException e) { e.printStackTrace(); }
        }
    }

    @Override
    public void onFailure(Call<AllNotificationResponse> call, Throwable t) {

    }
});
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}