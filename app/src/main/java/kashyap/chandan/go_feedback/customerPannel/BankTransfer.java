package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import kashyap.chandan.go_feedback.CustomItemClickListener;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.CustomerBalanceResponse;
import kashyap.chandan.go_feedback.ResponseClasses.FinalRedeemResponse;
import kashyap.chandan.go_feedback.ResponseClasses.StateData;
import kashyap.chandan.go_feedback.ResponseClasses.StateListResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class BankTransfer extends AppCompatActivity  implements View.OnClickListener {
 ImageView iv_back;
 TextView availableBalance,btnwithdraw;
    Dialog progressDialog;
    EditText balanceRedeem;
    TextInputEditText et_first_name,et_Last_name,et_address_1,et_address_2,et_zip,et_city,et_bank_rout_no,et_bank_acc_no,et_bank_name;
    String stateId;
    String balance;
    RecyclerView stateRecycler;
    TextView et_state;
    SharedPreferenceData sharedPreferenceData;
    Dialog stateDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_transfer);
        init();
        getCustomerBalance();
        iv_back.setOnClickListener(this);
        btnwithdraw.setOnClickListener(this);
        et_state.setOnClickListener(this);
    }
    private void init() {
        btnwithdraw=findViewById(R.id.btnwithdraw);
        balanceRedeem=findViewById(R.id.balanceRedeem);
        sharedPreferenceData=new SharedPreferenceData(BankTransfer.this);
        progressDialog=new Dialog(BankTransfer.this);
        iv_back=findViewById(R.id.iv_back);
        availableBalance=findViewById(R.id.availableBalance);
        et_state=findViewById(R.id.et_state);
        et_city=findViewById(R.id.et_city);
        et_zip=findViewById(R.id.et_zip);
        et_address_1=findViewById(R.id.et_address_1);
        et_first_name=findViewById(R.id.et_first_name);
        et_Last_name=findViewById(R.id.et_Last_name);
        et_address_2=findViewById(R.id.et_address_2) ;
        et_address_1=findViewById(R.id.et_address_1) ;
        et_bank_rout_no=findViewById(R.id.et_bank_rout_no);
        et_bank_acc_no=findViewById(R.id.et_bank_acc_no);
        et_bank_name=findViewById(R.id.et_bank_name);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_back:
                finish();
                break;

            case R.id.btnwithdraw:
               Redeem();
                break;
            case R.id.et_state: ;
                setEt_state();
                break;
        }
    }
    private void getCustomerBalance()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<CustomerBalanceResponse> call=apiInterface.getCustomerBalance(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<CustomerBalanceResponse>() {
                    @Override
                    public void onResponse(Call<CustomerBalanceResponse> call, Response<CustomerBalanceResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
//                            List<CustomerBalanceDataItem>customerBalanceData=response.body().getData();
                            balance=response.body().getData().getCurentBal();
                            availableBalance.setText("$"+response.body().getData().getCurentBal());
//                            tvTotalRedeem.setText("$"+response.body().getData().getTotalRedeem());
//                            tvTotalEarning.setText("$"+response.body().getData().getTotalEarn());
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(BankTransfer.this, "No Transaction", Toast.LENGTH_LONG).show();
                                    availableBalance.setText("$0");
                                    balance="0";

                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<CustomerBalanceResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(BankTransfer.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }

   private void Redeem()
    {
        String acc=et_bank_acc_no.getText().toString();
        String redeem=balanceRedeem.getText().toString();
        String city=et_city.getText().toString();
        String zip=et_zip.getText().toString();
        String add1=et_address_1.getText().toString();
        String add2=et_address_2.getText().toString();
        String fname=et_first_name.getText().toString();
        String lname=et_Last_name.getText().toString();
        String bankName=et_bank_name.getText().toString();
        String bankroute=et_bank_rout_no.getText().toString();
        if (fname.isEmpty()&&lname.isEmpty()&&add1.isEmpty()&&add2.isEmpty()&&stateId==null&&city.isEmpty()&&zip.isEmpty()&&bankroute.isEmpty())
            Toast.makeText(this, "Fill All the Fields", Toast.LENGTH_SHORT).show();
        else if (fname.isEmpty())
            Toast.makeText(this, "Enter First Name", Toast.LENGTH_SHORT).show();
        else if (lname.isEmpty())
            Toast.makeText(this, "Enter Last Name", Toast.LENGTH_SHORT).show();
        else if (add1.isEmpty())
            Toast.makeText(this, "Enter Address Line 1", Toast.LENGTH_SHORT).show();
        else if (add2.isEmpty())
            Toast.makeText(this, "Enter Address Line 2", Toast.LENGTH_SHORT).show();
        else if (stateId==null||stateId.isEmpty())
            Toast.makeText(this, "Select State", Toast.LENGTH_SHORT).show();
        else if (city.isEmpty())
            Toast.makeText(this, "Enter City Name", Toast.LENGTH_SHORT).show();
        else if (zip.isEmpty()||zip.length()!=5)
            Toast.makeText(this, "Enter Valid Zip Code", Toast.LENGTH_SHORT).show();
        else if (bankName.isEmpty())
            Toast.makeText(this, "Enter Bank Name ", Toast.LENGTH_SHORT).show();
        else if (bankroute.isEmpty()||bankroute.length()!=10)
            Toast.makeText(this, "Enter Valid Bank Routing ", Toast.LENGTH_SHORT).show();
        else if (acc.isEmpty())
            Toast.makeText(this, "Enter Valid Bank Account No ", Toast.LENGTH_SHORT).show();
       else if (redeem.isEmpty())
            Toast.makeText(this, "Enter Amount", Toast.LENGTH_SHORT).show();
        else if ((Float.parseFloat(balance))<(Float.parseFloat(redeem)))
            Toast.makeText(this, "You don't have Sufficient Balance", Toast.LENGTH_SHORT).show();
        else
        {
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            final Call<FinalRedeemResponse>call=apiInterface.redeemed(sharedPreferenceData.getId(),fname,lname,add1,add2,stateId,city,zip,bankName,bankroute,acc,redeem);
            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                   call.enqueue(new Callback<FinalRedeemResponse>() {
                       @Override
                       public void onResponse(Call<FinalRedeemResponse> call, Response<FinalRedeemResponse> response) {
                           if (response.code()==200)
                           {
                               progressDialog.dismiss();
                               final Dialog dialog=new Dialog(BankTransfer.this);
                               dialog.setContentView(R.layout.custommessagedialog);
                               dialog.show();
                               dialog.setCancelable(false);
                               TextView ok=dialog.findViewById(R.id.ok);
                               TextView message=dialog.findViewById(R.id.message);
                               message.setText("Amount\nWill Be Credited\nAfter Admin Approval");
                               ok.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View view) {
                                       dialog.dismiss();
                                       finish();
                                   }
                               });
                               Toast.makeText(BankTransfer.this, "Your Amount will be credited After Admin Approval", Toast.LENGTH_SHORT).show();


                           }
                           else
                           {
                               progressDialog.dismiss();
                               Toast.makeText(BankTransfer.this, "Failed", Toast.LENGTH_SHORT).show();
//                               Converter<ResponseBody, ApiError> converter =
//                                       ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
//                               ApiError error;
//                               try {
//                                   error = converter.convert(response.errorBody());
//                                   ApiError.StatusBean status=error.getStatus();
//                                   Toast.makeText(BankTransfer.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
//                               } catch (IOException e) { e.printStackTrace(); }
                           }
                       }

                       @Override
                       public void onFailure(Call<FinalRedeemResponse> call, Throwable t) {
progressDialog.dismiss();
                           Toast.makeText(BankTransfer.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                       }
                   });
                }
            };
            Thread thread=new Thread(runnable);
            thread.start();

        }
    }
    private void setEt_state()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<StateListResponse>call=apiInterface.stateList();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<StateListResponse>() {
                    @Override
                    public void onResponse(Call<StateListResponse> call, Response<StateListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            List<StateData> stateData=response.body().getData();
                            stateDialog = new Dialog(BankTransfer.this);
                            stateDialog.setContentView(R.layout.drop_down_dialog);
                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                            int width = metrics.widthPixels;
                            stateDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            stateRecycler = stateDialog.findViewById(R.id.recycleroption);
                            stateRecycler.setLayoutManager(new LinearLayoutManager(BankTransfer.this, LinearLayoutManager.VERTICAL, false));
                            stateRecycler.setAdapter(new StateListAdapter(BankTransfer.this, stateData, stateDialog, new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, String id, String value) {
                                    et_state.setText(value);
                                    stateId = id;
                                }
                            }
                            ));
                            stateDialog.show();
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class, new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status = error.getStatus();
                                Toast.makeText(BankTransfer.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<StateListResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}