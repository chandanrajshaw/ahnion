package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.CustomItemClickListener;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.AddBankResponse;
import kashyap.chandan.go_feedback.ResponseClasses.StateData;
import kashyap.chandan.go_feedback.ResponseClasses.StateListResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class BankDetail extends AppCompatActivity implements View.OnClickListener {
TextInputEditText et_bank_name,et_bank_address,et_bank_city,et_bank_routing,et_ifsc,et_account_number,et_bank_zip;
TextView btnsubmitDetail,et_state;
Intent intent;
ImageView iv_back;
Dialog progressDialog,stateDialog;
RecyclerView stateRecycler;
    private String stateId;
SharedPreferenceData sharedPreferenceData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_detail);
        init();
        et_state.setOnClickListener(this);
        btnsubmitDetail.setOnClickListener(this);
        iv_back.setOnClickListener(this);
    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        sharedPreferenceData=new SharedPreferenceData(BankDetail.this);
        progressDialog=new Dialog(BankDetail.this);
        intent=getIntent();
        et_bank_zip=findViewById(R.id.et_bank_zip);
        et_state=findViewById(R.id.et_state);
        et_bank_name=findViewById(R.id.et_bank_name);
        et_bank_address=findViewById(R.id.et_bank_address);
        et_bank_city=findViewById(R.id.et_bank_city);
        et_bank_routing=findViewById(R.id.et_bank_routing);
        et_ifsc=findViewById(R.id.et_ifsc);
        et_account_number=findViewById(R.id.et_account_number);
        btnsubmitDetail=findViewById(R.id.btnsubmitDetail);
    }
private  void addBankDetail()
{
    String bankZip=et_bank_zip.getText().toString();
    String bname=et_bank_name.getText().toString();
    String add=et_bank_address.getText().toString();
    String bcity=et_bank_city.getText().toString();
    String route=et_bank_routing.getText().toString();
    String  ifsc=et_ifsc.getText().toString();
    String account=et_account_number.getText().toString();
    if (bname.isEmpty()&&add.isEmpty()&&stateId==null&&bcity.isEmpty()&&route.isEmpty()&&ifsc.isEmpty()&&account.isEmpty())
        Toast.makeText(this, "Enter all Fields", Toast.LENGTH_SHORT).show();
    else if (add.isEmpty())
        Toast.makeText(this, "Enter Bank Address", Toast.LENGTH_SHORT).show();
    else if (stateId==null||stateId.isEmpty())
        Toast.makeText(this, "Select Bank State", Toast.LENGTH_SHORT).show();
    else if (bcity.isEmpty())
        Toast.makeText(this, "Enter Bank City", Toast.LENGTH_SHORT).show();
    else if (bankZip.isEmpty()||bankZip.length()!=6)
        Toast.makeText(this, "Enter Bank Zip Code", Toast.LENGTH_SHORT).show();
    else if (ifsc.isEmpty())
        Toast.makeText(this, "Enter IFSC Code of Bank", Toast.LENGTH_SHORT).show();
    else if (route.isEmpty())
        Toast.makeText(this, "Bank Routing", Toast.LENGTH_SHORT).show();
    else if (account.isEmpty())
        Toast.makeText(this, "Enter Bank Account Carefully ", Toast.LENGTH_SHORT).show();
    else
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
         final Call<AddBankResponse> call=apiInterface.addBank(sharedPreferenceData.getId(),
                intent.getStringExtra("fname"),
                intent.getStringExtra("lname"), intent.getStringExtra("add1"), intent.getStringExtra("add2"), intent.getStringExtra("state"),
                intent.getStringExtra("city"),
        intent.getStringExtra("zip"),
        intent.getStringExtra("phone"),
        bname,add,stateId,bcity,bankZip,route,account,ifsc);
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<AddBankResponse>() {
                    @Override
                    public void onResponse(Call<AddBankResponse> call, Response<AddBankResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            Toast.makeText(BankDetail.this, "Bank Detail Added Successfully", Toast.LENGTH_SHORT).show();
                            finish();
                            Intent intent=new Intent(BankDetail.this,BankTransfer.class);
                            startActivity(intent);
                        }
                         else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(BankDetail.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<AddBankResponse> call, Throwable t) {
progressDialog.dismiss();
                        Toast.makeText(BankDetail.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btnsubmitDetail:
                addBankDetail();
                break;
            case R.id.et_state:
                setEt_state();
                break;
        }
    }
    private void setEt_state()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<StateListResponse> call=apiInterface.stateList();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<StateListResponse>() {
                    @Override
                    public void onResponse(Call<StateListResponse> call, Response<StateListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            List<StateData>stateData=response.body().getData();
                            stateDialog = new Dialog(BankDetail.this);
                            stateDialog.setContentView(R.layout.drop_down_dialog);
                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                            int width = metrics.widthPixels;
                            stateDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            stateRecycler = stateDialog.findViewById(R.id.recycleroption);
                            stateRecycler.setLayoutManager(new LinearLayoutManager(BankDetail.this, LinearLayoutManager.VERTICAL, false));
                            stateRecycler.setAdapter(new StateListAdapter(BankDetail.this, stateData, stateDialog, new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, String id, String value) {
                                    et_state.setText(value);
                                    stateId = id;
                                }
                            }
                            ));
                            stateDialog.show();
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class, new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status = error.getStatus();
                                Toast.makeText(BankDetail.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<StateListResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}