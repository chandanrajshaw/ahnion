package kashyap.chandan.go_feedback.customerPannel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import kashyap.chandan.go_feedback.MainActivity;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.customerPannel.fragments.DashBoardFragment;
import kashyap.chandan.go_feedback.customerPannel.fragments.MoreFragment;
import kashyap.chandan.go_feedback.customerPannel.fragments.NearByFragment;

public class LandingScreen extends AppCompatActivity  {
    private static final int ALL_PERMISSIONS_RESULT =1;
    RecyclerView nearByPlaceRecycler;
    SupportMapFragment mapFragment;
    FrameLayout mainFrame;
    private LocationManager locationManager;
    TextView viewAll;
    SharedPreferenceData sharedPreferenceData;
    BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
// Hide both the navigation bar and the status bar.
// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
// a general rule, you should design your app to hide the status bar whenever you
// hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.activity_landing_screen);
        init();
        FragmentManager fm=getSupportFragmentManager();
        NearByFragment nearByFragment=new NearByFragment();
        FragmentTransaction ft=fm.beginTransaction();
        ft.add(R.id.mainFrame,nearByFragment);
        ft.commit();
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment=null;
                switch (item.getItemId())
                {
                    case R.id.action_nearBy:
                        fragment=new NearByFragment();
                        break;
                    case R.id.dashBoard:
                        if (sharedPreferenceData.getId()==null)
                        {
                            Intent loginIntent=new Intent(LandingScreen.this, MainActivity.class);
                            startActivity(loginIntent);
                        }
                        else {
                            fragment=new DashBoardFragment();
                        }
                        break;
                    case R.id.more:
                        if (sharedPreferenceData.getId()==null)
                        {
                            Intent loginIntent=new Intent(LandingScreen.this, MainActivity.class);
                            startActivity(loginIntent);
                        }
                        else {
                            fragment=new MoreFragment();
                        }

                        break;
                }
                return loadFragment(fragment);
            }
        });
    }
    private boolean loadFragment(Fragment fragment)
    {
        if (fragment!=null)
        {
            FragmentManager fragmentManager=getSupportFragmentManager();
            FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.mainFrame,fragment);
//            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            return true;
        }
        return false;

    }


    private void init() {
        nearByPlaceRecycler=findViewById(R.id.nearByPlaceRecycler);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        viewAll=findViewById(R.id.viewAll);
        sharedPreferenceData=new SharedPreferenceData(this);
        mainFrame=findViewById(R.id.mainFrame);
        bottomNavigationView=findViewById(R.id.bottomNavigationView);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case ALL_PERMISSIONS_RESULT:
                int length= grantResults.length;
                if (grantResults.length>0)
                {
                    for (int i=0;i<length;i++)
                    {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            // Granted. Start getting the location information
                        }
                        else if(grantResults[i]==PackageManager.PERMISSION_DENIED) {
                            {
                                final AlertDialog.Builder builder=new AlertDialog.Builder(getApplicationContext(),R.style.AlertDialogTheme);
                                builder.setTitle("Notice");
                                builder.setMessage("You Had Denied the Necessary Permissions.Please Go to app Setting and give All the permissions");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent=new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                        intent.setData(Uri.parse("package:" +getApplicationContext().getPackageName()));
                                        startActivity(intent);
                                    }
                                });
                                builder.show();
                                break;
                            }

                        }

                    }

                }
                else {
                }
        }
    }
//    private LocationListener locationListener = new LocationListener() {
//
//        @Override
//        public void onLocationChanged(Location location) {
//
//            String keyword = editText.getText().toString();
//            String key = getText(R.string.google_maps_key).toString();
//            String currentLocation = location.getLatitude() + "," + location.getLongitude();
//            System.out.println(location.getLatitude()+","+location.getLatitude());
//            int radius = 5000;
//            ApiInterface googleMapAPI = APIClient.getClient().create(ApiInterface.class);
//            googleMapAPI.getNearBy(currentLocation, radius, keyword, keyword, key).enqueue(new Callback<PlacesResults>() {
//                @Override
//                public void onResponse(Call<PlacesResults> call, Response<PlacesResults> response) {
//                    if (response.isSuccessful()) {
//
//                        List<Result> results = response.body().getResults();
//                        RecyclerViewAdapter placesListAdapter = new RecyclerViewAdapter(getContext(), results);
//                        recyclerView.setAdapter(placesListAdapter);
//                        progressDialog.dismiss();
//                    } else {
//                        progressDialog.dismiss();
//                        Toast.makeText(getContext(), "Failed", Toast.LENGTH_LONG).show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<PlacesResults> call, Throwable t) {
//                    progressDialog.dismiss();
//                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
//                }
//            });
//        }
//
//        @Override
//        public void onStatusChanged(String s, int i, Bundle bundle) {
//            progressDialog.dismiss();
//        }
//
//        @Override
//        public void onProviderEnabled(String s) {
//
//        }
//
//        @Override
//        public void onProviderDisabled(String s) {
//
//        }
//    };

}