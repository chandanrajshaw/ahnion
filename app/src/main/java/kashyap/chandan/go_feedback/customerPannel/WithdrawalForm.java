package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.CustomItemClickListener;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.StateData;
import kashyap.chandan.go_feedback.ResponseClasses.StateListResponse;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class WithdrawalForm extends AppCompatActivity implements View.OnClickListener {
ImageView iv_back;
TextInputEditText et_first_name,et_Last_name,et_address_1,et_address_2,et_zip,et_city,et_phone;
TextView btnsubmitDetail;
RecyclerView stateRecycler;
TextView et_state;
    private Dialog progressDialog,stateDialog;
    String stateId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawal_form);
        init();
        iv_back.setOnClickListener(this);
        btnsubmitDetail.setOnClickListener(this);
        et_state.setOnClickListener(this);
    }

    private void btnSendDetail()
    {
       String city=et_city.getText().toString();
       String zip=et_zip.getText().toString();
       String add1=et_address_1.getText().toString();
       String add2=et_address_2.getText().toString();
       String fname=et_first_name.getText().toString();
       String lname=et_Last_name.getText().toString();
       String phone=et_phone.getText().toString();
       if (fname.isEmpty()&&lname.isEmpty()&&add1.isEmpty()&&add2.isEmpty()&&stateId==null&&city.isEmpty()&&zip.isEmpty()&&phone.isEmpty())
           Toast.makeText(this, "Fill All the Fields", Toast.LENGTH_SHORT).show();
       else if (fname.isEmpty())
           Toast.makeText(this, "Enter First Name", Toast.LENGTH_SHORT).show();
       else if (lname.isEmpty())
           Toast.makeText(this, "Enter Last Name", Toast.LENGTH_SHORT).show();
       else if (add1.isEmpty())
           Toast.makeText(this, "Enter Address Line 1", Toast.LENGTH_SHORT).show();
       else if (add2.isEmpty())
           Toast.makeText(this, "Enter Address Line 2", Toast.LENGTH_SHORT).show();
       else if (stateId==null||stateId.isEmpty())
           Toast.makeText(this, "Select State", Toast.LENGTH_SHORT).show();
       else if (city.isEmpty())
           Toast.makeText(this, "Enter City Name", Toast.LENGTH_SHORT).show();
       else if (zip.isEmpty()||zip.length()!=6)
           Toast.makeText(this, "Enter Valid Zip Code", Toast.LENGTH_SHORT).show();
       else if (phone.isEmpty()||phone.length()!=10)
           Toast.makeText(this, "Enter Valid Phone ", Toast.LENGTH_SHORT).show();
       else
       {
           Intent intent=new Intent(WithdrawalForm.this,BankDetail.class);
           intent.putExtra("fname",fname);
           intent.putExtra("lname",lname);
           intent.putExtra("add1",add1);
           intent.putExtra("add2",add2);
           intent.putExtra("state",stateId);
           intent.putExtra("city",city);
           intent.putExtra("zip",zip);
           intent.putExtra("phone",phone);
           startActivity(intent);
           finish();
       }
    }
    private void init() {
        et_phone=findViewById(R.id.et_phone);
        progressDialog=new Dialog(WithdrawalForm.this);
        btnsubmitDetail=findViewById(R.id.btnsubmitDetail);
        et_state=findViewById(R.id.et_state);
        et_city=findViewById(R.id.et_city);
        et_zip=findViewById(R.id.et_zip);
        et_address_1=findViewById(R.id.et_address_1);
        iv_back=findViewById(R.id.iv_back);
        et_first_name=findViewById(R.id.et_first_name);
        et_Last_name=findViewById(R.id.et_Last_name);
        et_address_2=findViewById(R.id.et_address_2) ;
        et_address_1=findViewById(R.id.et_address_1) ;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btnsubmitDetail: ;
            btnSendDetail();
                break;

            case R.id.et_state: ;
            setEt_state();
                break;
        }
    }
    private void setEt_state()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<StateListResponse>call=apiInterface.stateList();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<StateListResponse>() {
                    @Override
                    public void onResponse(Call<StateListResponse> call, Response<StateListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            List<StateData>stateData=response.body().getData();
                            stateDialog = new Dialog(WithdrawalForm.this);
                            stateDialog.setContentView(R.layout.drop_down_dialog);
                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                            int width = metrics.widthPixels;
                            stateDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            stateRecycler = stateDialog.findViewById(R.id.recycleroption);
                            stateRecycler.setLayoutManager(new LinearLayoutManager(WithdrawalForm.this, LinearLayoutManager.VERTICAL, false));
                            stateRecycler.setAdapter(new StateListAdapter(WithdrawalForm.this, stateData, stateDialog, new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, String id, String value) {
                                    et_state.setText(value);
                                    stateId = id;
                                }
                            }
                            ));
                            stateDialog.show();
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class, new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status = error.getStatus();
                                Toast.makeText(WithdrawalForm.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<StateListResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}