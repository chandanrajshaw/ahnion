package kashyap.chandan.go_feedback.customerPannel.fragments;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import kashyap.chandan.go_feedback.ResponseClasses.NotificationItem;

public class Response{

	@SerializedName("notification")
	private List<NotificationItem> notification;

	@SerializedName("status")
	private Status status;

	public List<NotificationItem> getNotification(){
		return notification;
	}

	public Status getStatus(){
		return status;
	}
}