package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.View;

import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;


import kashyap.chandan.go_feedback.Constants;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.customerPannel.maps.APIClient;
import kashyap.chandan.go_feedback.customerPannel.maps.ApiInterface;
import kashyap.chandan.go_feedback.customerPannel.maps.Result;
import kashyap.chandan.go_feedback.customerPannel.searchlocation.ResultsItem;
import kashyap.chandan.go_feedback.customerPannel.searchlocation.SearchPlaceResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectedBusiness extends AppCompatActivity {

    RecyclerView allRecyclerView;
    Intent intent;
    ImageView iv_back;
    List<Result> results=new ArrayList<>();
    List<ResultsItem> searchResult;
    EditText search_location_box;
    SelectedPlaceAdapter adapter;
    String keyword;
    SearchSelectedPlaceAdapter searchSelectedPlaceAdapter;
    Dialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_business);
        init();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        intent=getIntent();
        final String key=intent.getStringExtra("key");
        if (key!=null)
        {
            Bundle bundle=intent.getBundleExtra("bundle");
            searchResult= (List<ResultsItem>) bundle.getSerializable("searchresult");
            searchSelectedPlaceAdapter=new SearchSelectedPlaceAdapter(SelectedBusiness.this,searchResult);
            allRecyclerView.setAdapter(searchSelectedPlaceAdapter);
        }
        else if (key==null)
        {
            Bundle bundle=intent.getBundleExtra("bundle");
            results= (List<Result>) bundle.getSerializable("allResult");
            adapter=new SelectedPlaceAdapter(SelectedBusiness.this,results);
            allRecyclerView.setAdapter(adapter);
        }
        search_location_box.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i== EditorInfo.IME_ACTION_SEARCH)
                {
                    progressDialog.setCancelable(false);
                    progressDialog.setContentView(R.layout.loadingdialog);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    keyword=search_location_box.getText().toString();
                    final ApiInterface googleMapAPI = APIClient.getClient().create(ApiInterface.class);
                    Runnable runnable=new Runnable() {
                        @Override
                        public void run() {
                            googleMapAPI.getSearchPlaces(keyword, Constants.KEY).enqueue(new Callback<SearchPlaceResponse>() {
                                @Override
                                public void onResponse(Call<SearchPlaceResponse> call, Response<SearchPlaceResponse> response) {
                                    if (response.code()==200)
                                    {
                                        progressDialog.dismiss();
                                        searchResult=response.body().getResults();
                                        allRecyclerView.setAdapter(null);
                                        searchSelectedPlaceAdapter=new SearchSelectedPlaceAdapter(SelectedBusiness.this,searchResult);
                                            allRecyclerView.setAdapter(searchSelectedPlaceAdapter);


                                    }
                                    else
                                    {
                                        progressDialog.dismiss();
                                        Toast.makeText(SelectedBusiness.this, "No Places Found With this Name", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<SearchPlaceResponse> call, Throwable t) {
                                    progressDialog.dismiss();
                                    Toast.makeText(SelectedBusiness.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    };
                    Thread thread=new Thread(runnable);
                    thread.start();
                    return true;
                }
                return false;
            }
        });
//        search_location_box.addTextChangedListener(new TextWatcher()
//        {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if (key!=null)
//                {
//                    searchSelectedPlaceAdapter.getFilter().filter(charSequence);
//                }
//               else
//                {
//                    adapter.getFilter().filter(charSequence);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
    }
    private void init() {
        allRecyclerView=findViewById(R.id.allRecyclerView);
        allRecyclerView.setLayoutManager(new LinearLayoutManager(SelectedBusiness.this,LinearLayoutManager.VERTICAL,false));
        search_location_box=findViewById(R.id.search_location_box);
        iv_back=findViewById(R.id.iv_back);
        progressDialog=new Dialog(SelectedBusiness.this);
    }
}