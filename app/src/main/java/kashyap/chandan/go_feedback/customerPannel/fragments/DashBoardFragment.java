package kashyap.chandan.go_feedback.customerPannel.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.List;

import kashyap.chandan.go_feedback.ClickListnerAdapterInterface;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessDraftData;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessDraftResponse;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPendingData;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPendingResponse;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPublishedData;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPublishedResponse;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPurchaseData;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPurchaseResponse;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessRejectedData;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessRejectedResponse;
import kashyap.chandan.go_feedback.ResponseClasses.CustomerBalanceResponse;
import kashyap.chandan.go_feedback.ResponseClasses.DraftCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.DraftDataItem;
import kashyap.chandan.go_feedback.ResponseClasses.MyReviewCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.PendingCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.PendingDataItem;
import kashyap.chandan.go_feedback.ResponseClasses.PublishCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.PublishDataItem;
import kashyap.chandan.go_feedback.ResponseClasses.PurchaseCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.PurchaseDataItem;
import kashyap.chandan.go_feedback.ResponseClasses.RejectedCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.RejectedDataItem;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.customerPannel.AllRedemption;
import kashyap.chandan.go_feedback.customerPannel.CustomerDrafts;
import kashyap.chandan.go_feedback.customerPannel.CustomerPublished;
import kashyap.chandan.go_feedback.customerPannel.CustomerPurchased;
import kashyap.chandan.go_feedback.customerPannel.CustomerRejected;
import kashyap.chandan.go_feedback.customerPannel.CustomerSubmit;
import kashyap.chandan.go_feedback.customerPannel.DraftsBussinessAdapters;
import kashyap.chandan.go_feedback.customerPannel.PublishedBussinessAdapters;
import kashyap.chandan.go_feedback.customerPannel.PurchasedBussinessAdapters;
import kashyap.chandan.go_feedback.customerPannel.RejectedBussinessAdapters;
import kashyap.chandan.go_feedback.customerPannel.PendingBussinessAdapters;
import kashyap.chandan.go_feedback.customerPannel.TotalEarning;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class DashBoardFragment extends Fragment implements View.OnClickListener {
    RecyclerView dashboardRecycler;
    FrameLayout totalIndividualEarning,allRedeem;
CardView drafts,submitted, published,purchased,rejected;
TextView tvdrafts,tvsubmitted, tvpublished,tvpurchased,tvrejected,tvCurrentBalance,tvTotalRedeem,tvTotalEarning;
Dialog progressDialog;
SharedPreferenceData sharedPreferenceData;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.dashboard_fragment,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        dashboardRecycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        Count();
       draftCount();

       getCustomerBalance();
        tvpurchased.setOnClickListener(this);
        tvdrafts.setOnClickListener(this);
        tvsubmitted.setOnClickListener(this);
        tvpublished.setOnClickListener(this);
        tvrejected.setOnClickListener(this);
        totalIndividualEarning.setOnClickListener(this);
        allRedeem.setOnClickListener(this);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
//        onResume();
    }

    @Override
    public void onResume() {
        super.onResume();
        Count();
        drafts.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.activeCardColor));
        tvdrafts.setTextColor(ContextCompat.getColor(getContext(),R.color.activeTextColor));

        rejected.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
        tvrejected.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

        published.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
        tvpublished.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

        submitted.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
        tvsubmitted.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

        purchased.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
        tvpurchased.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));
        dashboardRecycler.removeAllViews();
        dashboardRecycler.setAdapter(null);
        draftCount();
        getCustomerBalance();
    }

    private void init(View view) {
        tvCurrentBalance =view.findViewById(R.id.tvCurrentBalance);
                tvTotalRedeem=view.findViewById(R.id.tvTotalRedeem);
        tvTotalEarning=view.findViewById(R.id.tvTotalEarning);
        sharedPreferenceData=new SharedPreferenceData(getContext());
        allRedeem=view.findViewById(R.id.allRedeem);
        totalIndividualEarning=view.findViewById(R.id.totalIndividualEarning);
        dashboardRecycler=view.findViewById(R.id.dashboardRecycler);
        drafts=view.findViewById(R.id.drafts);
        submitted=view.findViewById(R.id.submitted);
        published =view.findViewById(R.id.approved);
        purchased=view.findViewById(R.id.purchased);
        rejected=view.findViewById(R.id.rejected);
        tvdrafts=view.findViewById(R.id.tvdrafts);
        tvsubmitted=view.findViewById(R.id.tvsubmitted);
        tvpublished =view.findViewById(R.id.tvpublished);
        tvpurchased=view.findViewById(R.id.tvpurchased);
        tvrejected=view.findViewById(R.id.tvrejected);
progressDialog=new Dialog(getContext());
    }

    @Override
    public void onClick(View view) {
//        cardName.setCardBackgroundColor(ContextCompat.getColor(this, R.color.colorGray))
//        mTextView.setTextColor(ContextCompat.getColor(context, R.color.<name_of_color>));
        switch (view.getId())
        {
            case R.id.tvdrafts:
                drafts.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.activeCardColor));
                tvdrafts.setTextColor(ContextCompat.getColor(getContext(),R.color.activeTextColor));

                rejected.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvrejected.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                published.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvpublished.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                submitted.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvsubmitted.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                purchased.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvpurchased.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));
                dashboardRecycler.removeAllViews();
              draftCount();
                break;

            case R.id.tvsubmitted:
                drafts.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvdrafts.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                rejected.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvrejected.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                published.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvpublished.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                submitted.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.activeCardColor));
                tvsubmitted.setTextColor(ContextCompat.getColor(getContext(),R.color.activeTextColor));

                purchased.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvpurchased.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));
                dashboardRecycler.removeAllViews();
               pendingCount();
                break;

            case R.id.tvpublished:
                drafts.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvdrafts.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                rejected.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvrejected.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                published.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.activeCardColor));
                tvpublished.setTextColor(ContextCompat.getColor(getContext(),R.color.activeTextColor));

                submitted.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvsubmitted.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                purchased.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvpurchased.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));
                dashboardRecycler.removeAllViews();
                publishedCount();
                break;

            case R.id.tvpurchased:
                drafts.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvdrafts.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                rejected.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvrejected.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                published.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvpublished.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                submitted.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvsubmitted.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                purchased.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.activeCardColor));
                tvpurchased.setTextColor(ContextCompat.getColor(getContext(),R.color.activeTextColor));
                dashboardRecycler.removeAllViews();
               purchasedCount();
                break;

            case R.id.tvrejected:
                drafts.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvdrafts.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                rejected.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.activeCardColor));
                tvrejected.setTextColor(ContextCompat.getColor(getContext(),R.color.activeTextColor));

                published.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvpublished.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                submitted.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvsubmitted.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));

                purchased.setCardBackgroundColor(ContextCompat.getColor(getContext(),R.color.inactiveCardColor));
                tvpurchased.setTextColor(ContextCompat.getColor(getContext(),R.color.inactiveTextColor));
                dashboardRecycler.removeAllViews();
                rejectedCount();
                break;

                case R.id.totalIndividualEarning:
                    Intent totalEarningIntent=new Intent(getContext(), TotalEarning.class);
                    getContext().startActivity(totalEarningIntent);
                    break;

            case R.id.allRedeem:
                Intent allRedeemIntent=new Intent(getContext(), AllRedemption.class);
                getContext().startActivity(allRedeemIntent);
                break;

        }
    }
    private void purchasedCount()
    {
        dashboardRecycler.setAdapter(null);
        dashboardRecycler.removeAllViewsInLayout();
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<PurchaseCountResponse>call=apiInterface.getPurchaseCount(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<PurchaseCountResponse>() {
                    @Override
                    public void onResponse(Call<PurchaseCountResponse> call, Response<PurchaseCountResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            final List<PurchaseDataItem> purchaseData=response.body().getData();

                            dashboardRecycler.setAdapter(new PurchasedBussinessAdapters(getContext(),purchaseData, new ClickListnerAdapterInterface() {
                                @Override
                                public void onClicked(View view, Object object) {
                                    progressDialog=new Dialog(getContext());
                                    progressDialog.setContentView(R.layout.loadingdialog);
                                    progressDialog.setCancelable(false);
                                    progressDialog.show();
                                    APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                                    final PurchaseDataItem purchaseDataItem= (PurchaseDataItem) object;
                                    //Toast.makeText(getContext(), ""+purchaseDataItem.getCount(), Toast.LENGTH_SHORT).show();
                              Call<BusinessPurchaseResponse>call1=apiInterface.businessPurchased(sharedPreferenceData.getId(),purchaseDataItem.getBId());
                              call1.enqueue(new Callback<BusinessPurchaseResponse>() {
                                  @Override
                                  public void onResponse(Call<BusinessPurchaseResponse> call, Response<BusinessPurchaseResponse> response) {
                                      if (response.code()==200) {
                                          progressDialog.dismiss();
                                          List<BusinessPurchaseData>purchaseDataList=response.body().getData();
                                          if (purchaseDataList.size()==0||purchaseDataList==null)
                                          {
                                              Toast.makeText(getContext(), "No Data Available for"+purchaseDataItem.getName(), Toast.LENGTH_SHORT).show();
                                          }
                                          else
                                          {
                                              Intent intent=new Intent(getContext(), CustomerPurchased.class);
                                              Bundle bundle=new Bundle();
                                              bundle.putSerializable("purchase", (Serializable) purchaseDataList);
                                              intent.putExtra("b_name",purchaseDataItem.getName());
                                              intent.putExtra("bundle",bundle);
                                              startActivity(intent);
                                          }



                                      }
                                      else
                                      {
                                          progressDialog.dismiss();
                                          Converter<ResponseBody, ApiError> converter =
                                                  ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                          ApiError error;
                                          try {
                                              error = converter.convert(response.errorBody());
                                              ApiError.StatusBean status=error.getStatus();
                                              Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                                          } catch (IOException e) { e.printStackTrace(); }
                                      }
                                  }

                                  @Override
                                  public void onFailure(Call<BusinessPurchaseResponse> call, Throwable t) {
progressDialog.dismiss();
                                      Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                  }
                              });
                                }
                            }));
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<PurchaseCountResponse> call, Throwable t) {
                        Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
    private void rejectedCount()
    {
        dashboardRecycler.setAdapter(null);
        dashboardRecycler.removeAllViewsInLayout();
        dashboardRecycler.removeAllViews();
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<RejectedCountResponse>call=apiInterface.getRejectedCount(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<RejectedCountResponse>() {
                    @Override
                    public void onResponse(Call<RejectedCountResponse> call, Response<RejectedCountResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            final List<RejectedDataItem> rejectedData=response.body().getData();

                            dashboardRecycler.setAdapter(new RejectedBussinessAdapters(getContext(),rejectedData, new ClickListnerAdapterInterface() {
                                @Override
                                public void onClicked(View view, Object object) {
                                    final RejectedDataItem rejectedDataItem= (RejectedDataItem) object;
                                    Intent intent=new Intent(getContext(), CustomerRejected.class);
                                    intent.putExtra("b_name",rejectedDataItem.getName());
                                    intent.putExtra("b_id",rejectedDataItem.getBId());
                                    startActivity(intent);



                                }
                            }));
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<RejectedCountResponse> call, Throwable t) {
progressDialog.dismiss();
                        Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
    private void getCustomerBalance()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<CustomerBalanceResponse>call=apiInterface.getCustomerBalance(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<CustomerBalanceResponse>() {
                    @Override
                    public void onResponse(Call<CustomerBalanceResponse> call, Response<CustomerBalanceResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
//                            List<CustomerBalanceDataItem>customerBalanceData=response.body().getData();
                          tvCurrentBalance.setText("$"+response.body().getData().getCurentBal());
                          tvTotalRedeem.setText("$"+response.body().getData().getTotalRedeem());
                          tvTotalEarning.setText("$"+response.body().getData().getTotalEarn());
                        }
                        else
                        {
 tvCurrentBalance.setText("$0");
                          tvTotalRedeem.setText("$0");
                          tvTotalEarning.setText("$0");
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(getContext(), "No Transaction", Toast.LENGTH_SHORT).show();
                                if (response.code()==401)
                                {
                                    tvCurrentBalance.setText("$0");
                                    tvTotalRedeem.setText("$0");
                                    tvTotalEarning.setText("$0");
                                }
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<CustomerBalanceResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
    private void pendingCount()
    { dashboardRecycler.setAdapter(null);
        dashboardRecycler.removeAllViewsInLayout();
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<PendingCountResponse>call=apiInterface.getPendingCount(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<PendingCountResponse>() {
                    @Override
                    public void onResponse(Call<PendingCountResponse> call, Response<PendingCountResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            final List<PendingDataItem> pendingData=response.body().getData();

                            dashboardRecycler.setAdapter(new PendingBussinessAdapters(getContext(),pendingData, new ClickListnerAdapterInterface() {
                                @Override
                                public void onClicked(View view, Object object) {
                                    progressDialog=new Dialog(getContext());
                                    progressDialog.setContentView(R.layout.loadingdialog);
                                    progressDialog.setCancelable(false);
                                    progressDialog.show();
                                    APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                                    final PendingDataItem pendingDataItem= (PendingDataItem) object;
//                                    Toast.makeText(getContext(), ""+pendingDataItem.getCount(), Toast.LENGTH_SHORT).show();
                                    Call<BusinessPendingResponse>call1=apiInterface.businessPending(sharedPreferenceData.getId(),pendingDataItem.getBId());
                                    call1.enqueue(new Callback<BusinessPendingResponse>() {
                                        @Override
                                        public void onResponse(Call<BusinessPendingResponse> call, Response<BusinessPendingResponse> response) {
                                            if (response.code()==200) {
                                                progressDialog.dismiss();
                                                List<BusinessPendingData>pendingDataList=response.body().getData();
                                                if (pendingDataList.size()==0||pendingDataList==null)
                                                {
                                                    Toast.makeText(getContext(), "No Data Available for"+pendingDataItem.getName(), Toast.LENGTH_SHORT).show();
                                                }
                                                else
                                                {
                                                    Intent intent=new Intent(getContext(), CustomerSubmit.class);
                                                    Bundle bundle=new Bundle();
                                                    bundle.putSerializable("pending", (Serializable) pendingDataList);
                                                    intent.putExtra("b_name",pendingDataItem.getName());
                                                    intent.putExtra("bundle",bundle);
                                                    startActivity(intent);
                                                }



                                            }
                                            else
                                            {
                                                progressDialog.dismiss();
                                                Converter<ResponseBody, ApiError> converter =
                                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                                ApiError error;
                                                try {
                                                    error = converter.convert(response.errorBody());
                                                    ApiError.StatusBean status=error.getStatus();
                                                    Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                                                } catch (IOException e) { e.printStackTrace(); }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<BusinessPendingResponse> call, Throwable t) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }));
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<PendingCountResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
    private void Count()
    {
        dashboardRecycler.setAdapter(null);
        dashboardRecycler.removeAllViewsInLayout();
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<MyReviewCountResponse>call=apiInterface.myReviewCount(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<MyReviewCountResponse>() {
                    @Override
                    public void onResponse(Call<MyReviewCountResponse> call, Response<MyReviewCountResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            tvdrafts.setText("Draft("+response.body().getDraftCount().getCount()+")");
                            tvrejected.setText("Rejected("+response.body().getRejectedCount().getCount()+")");
                            tvpublished.setText("Published("+response.body().getApprovedCount().getCount()+")");
                            tvpurchased.setText("Purchased("+response.body().getPurchesedCount().getCount()+")");
                            tvsubmitted.setText("Submitted("+response.body().getPendingCount().getCount()+")");
                        }

                        else
                        {
                            progressDialog.dismiss();
                            tvdrafts.setText("Draft(0)");
                            tvrejected.setText("Rejected(0)");
                            tvpublished.setText("Published(0)");
                            tvpurchased.setText("Purchased(0)");
                            tvsubmitted.setText("Submitted(0)");
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<MyReviewCountResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
    private void draftCount()
    {
        dashboardRecycler.setAdapter(null);
        dashboardRecycler.removeAllViewsInLayout();
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<DraftCountResponse>call=apiInterface.getDraftCount(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
              call.enqueue(new Callback<DraftCountResponse>() {
                  @Override
                  public void onResponse(Call<DraftCountResponse> call, Response<DraftCountResponse> response) {
                      if (response.code()==200)
                      {
                          progressDialog.dismiss();
                          final List<DraftDataItem> draftData=response.body().getData();
                          dashboardRecycler.setAdapter(null);
                          dashboardRecycler.setAdapter(new DraftsBussinessAdapters(getContext(),draftData, new ClickListnerAdapterInterface() {
                              @Override
                              public void onClicked(View view, Object object) {
                                  final DraftDataItem draftDataItem= (DraftDataItem) object;
                                  Intent intent=new Intent(getContext(),CustomerDrafts.class);
                                  intent.putExtra("b_id",draftDataItem.getBId());
                                  intent.putExtra("name",draftDataItem.getName());
                                  startActivity(intent);


                              }
                          }));
                      }
                      else
                      {
                          progressDialog.dismiss();
                          Converter<ResponseBody, ApiError> converter =
                                  ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                          ApiError error;
                          try {
                              error = converter.convert(response.errorBody());
                              ApiError.StatusBean status=error.getStatus();
                              Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                          } catch (IOException e) { e.printStackTrace(); }
                      }
                  }

                  @Override
                  public void onFailure(Call<DraftCountResponse> call, Throwable t) {
                      progressDialog.dismiss();
                      Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                  }
              });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
    private void publishedCount()
    { dashboardRecycler.setAdapter(null);
        dashboardRecycler.removeAllViewsInLayout();
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<PublishCountResponse>call=apiInterface.getpublishCount(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<PublishCountResponse>() {
                    @Override
                    public void onResponse(Call<PublishCountResponse> call, Response<PublishCountResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            final List<PublishDataItem> publishData=response.body().getData();
                            dashboardRecycler.setAdapter(new PublishedBussinessAdapters(getContext(),publishData, new ClickListnerAdapterInterface() {
                                @Override
                                public void onClicked(View view, Object object) {
                                    progressDialog=new Dialog(getContext());
                                    progressDialog.setContentView(R.layout.loadingdialog);
                                    progressDialog.setCancelable(false);
                                    progressDialog.show();
                                    APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                                    final PublishDataItem publishDataItem= (PublishDataItem) object;
//                                    Toast.makeText(getContext(), ""+publishDataItem.getCount(), Toast.LENGTH_SHORT).show();
                                       Call<BusinessPublishedResponse>call1=apiInterface.businessPublished(sharedPreferenceData.getId(),publishDataItem.getBId());
                                    call1.enqueue(new Callback<BusinessPublishedResponse>() {
                                        @Override
                                        public void onResponse(Call<BusinessPublishedResponse> call, Response<BusinessPublishedResponse> response) {
                                            if (response.code()==200) {
                                                progressDialog.dismiss();
                                                List<BusinessPublishedData>publishedDataList=response.body().getData();
                                                if (publishedDataList.size()==0||publishedDataList==null)
                                                {
                                                    Toast.makeText(getContext(), "No Data Available for"+publishDataItem.getName(), Toast.LENGTH_SHORT).show();
                                                }
                                                else
                                                {
                                                    Intent intent=new Intent(getContext(), CustomerPublished.class);
                                                    Bundle bundle=new Bundle();
                                                    bundle.putSerializable("published", (Serializable) publishedDataList);
                                                    intent.putExtra("b_name",publishDataItem.getName());
                                                    intent.putExtra("bundle",bundle);
                                                    startActivity(intent);
                                                }



                                            }
                                            else
                                            {
                                                progressDialog.dismiss();
                                                Converter<ResponseBody, ApiError> converter =
                                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                                ApiError error;
                                                try {
                                                    error = converter.convert(response.errorBody());
                                                    ApiError.StatusBean status=error.getStatus();
                                                    Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                                                } catch (IOException e) { e.printStackTrace(); }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<BusinessPublishedResponse> call, Throwable t) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                            }));
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<PublishCountResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}
