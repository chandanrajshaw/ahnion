package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.go_feedback.MainActivity;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.ContactUsResponse;
import kashyap.chandan.go_feedback.ResponseClasses.ResetPasswordResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ContactUs extends AppCompatActivity implements View.OnClickListener {
TextView btnSend;
EditText et_comments;
TextInputEditText et_email,et_Name;
ImageView iv_back;
SharedPreferenceData sharedPreferenceData;
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        init();
    }
    @Override
    protected void onResume() {
        super.onResume();
        iv_back.setOnClickListener(this);
        et_Name.setText(sharedPreferenceData.getFName()+" "+sharedPreferenceData.getLName());
        et_email.setText(sharedPreferenceData.getEmail());
        btnSend.setOnClickListener(this);
    }

    private void init() {
        sharedPreferenceData=new SharedPreferenceData(ContactUs.this);
        iv_back=findViewById(R.id.iv_back);
        et_comments=findViewById(R.id.et_comments);
        et_Name=findViewById(R.id.et_Name);
        et_email=findViewById(R.id.et_email);
        btnSend=findViewById(R.id.btnSend);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnSend:
                sendContact();
                break;
            case R.id.iv_back:
                finish();
                break;
        }
    }
    private void sendContact() {
        final String name=et_Name.getText().toString();
        final String email=et_email.getText().toString();
        final String comment=et_comments.getText().toString();
        if (name.isEmpty()&& email.isEmpty()&&comment.isEmpty())
            Toast.makeText(this, "Enter All Fields", Toast.LENGTH_SHORT).show();
        else if (name.isEmpty())
            Toast.makeText(this, "Please Enter Name", Toast.LENGTH_SHORT).show();
        else if (email.isEmpty())
            Toast.makeText(this, "Please Enter Email", Toast.LENGTH_SHORT).show();
        else if (comment.isEmpty())
            Toast.makeText(this, "Enter Comment", Toast.LENGTH_SHORT).show();
        else
        {
            progressDialog=new Dialog(ContactUs.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            final APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    Call<ContactUsResponse> call=apiInterface.putContactUs(name,email,comment);
                    call.enqueue(new Callback<ContactUsResponse>() {
                        @Override
                        public void onResponse(Call<ContactUsResponse> call, Response<ContactUsResponse> response) {
                            if (response.code()==200)
                            {
                                progressDialog.dismiss();
                                final Dialog dialog=new Dialog(ContactUs.this);
                                dialog.setContentView(R.layout.custommessagedialog);
                                dialog.show();
                                dialog.setCancelable(false);
                                TextView ok=dialog.findViewById(R.id.ok);
                                TextView message=dialog.findViewById(R.id.message);
                                message.setText("Enquiry has been submitted successfully");
                                ok.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        finish();
                                        dialog.dismiss();
                                    }
                                });
                                Toast.makeText(ContactUs.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                progressDialog.dismiss();
                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                    Toast.makeText(ContactUs.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }
                            }
                        }

                        @Override
                        public void onFailure(Call<ContactUsResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(ContactUs.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            };
            Thread thread=new Thread(runnable);
            thread.start();
        }
    }
}