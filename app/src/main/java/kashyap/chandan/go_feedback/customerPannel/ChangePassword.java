package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.go_feedback.MainActivity;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.ChangePasswordResponse;
import kashyap.chandan.go_feedback.ResponseClasses.ResetPasswordResponse;
import kashyap.chandan.go_feedback.SetPassword;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener {
TextInputEditText etNewPassword,etConNewPassword;
TextView btnChangePassword;
    private Dialog progressDialog;
    ImageView iv_back;
SharedPreferenceData sharedPreferenceData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        init();
        btnChangePassword.setOnClickListener(this);
        iv_back.setOnClickListener(this);
    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        sharedPreferenceData=new SharedPreferenceData(ChangePassword.this);
        etConNewPassword=findViewById(R.id.etConNewPassword);
        etNewPassword=findViewById(R.id.etNewPassword);
        btnChangePassword=findViewById(R.id.btnChangePassword);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnChangePassword:
                changePassword();
                break;

            case R.id.iv_back:
                finish();
                break;
        }
    }

    private void changePassword() {
        final String newPassword=etNewPassword.getText().toString();
        String conNewPassword=etConNewPassword.getText().toString();
        if (newPassword.isEmpty()&& conNewPassword.isEmpty())
            Toast.makeText(this, "Enter New Password", Toast.LENGTH_SHORT).show();
        else if (newPassword.isEmpty()||newPassword.length()!=8)
            Toast.makeText(this, "Please Enter new 8-digit Pin", Toast.LENGTH_SHORT).show();
        else if (conNewPassword.isEmpty()||conNewPassword.length()!=8)
            Toast.makeText(this, "Please Re-Enter new 8-digit Pin", Toast.LENGTH_SHORT).show();
        else if (!newPassword.equals(conNewPassword))
            Toast.makeText(this, "Pin Mis-Match", Toast.LENGTH_SHORT).show();
        else
        {
            progressDialog=new Dialog(ChangePassword.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            final APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    Call<ResetPasswordResponse>call=apiInterface.resetPassword(sharedPreferenceData.getId(),newPassword);
                    call.enqueue(new Callback<ResetPasswordResponse>() {
                        @Override
                        public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                            if (response.code()==200)
                            {
                                progressDialog.dismiss();
                                Toast.makeText(ChangePassword.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(ChangePassword.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                            else
                            {
                                progressDialog.dismiss();
                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                    Toast.makeText(ChangePassword.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }
                            }
                        }

                        @Override
                        public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
progressDialog.dismiss();
                            Toast.makeText(ChangePassword.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            };
            Thread thread=new Thread(runnable);
            thread.start();
        }
    }
}