package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessRejectedData;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessRejectedResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;


public class CustomerRejected extends AppCompatActivity implements View.OnClickListener {
RecyclerView rejectedRecycler;
    Intent intent;
    ImageView iv_back;
    TextView toolHeader;
    String b_id;
    List<BusinessRejectedData> rejectedDataList;
    Dialog progressDialog;
    SharedPreferenceData sharedPreferenceData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_rejected);
        init();
        toolHeader.setText(intent.getStringExtra("b_name"));
        b_id=intent.getStringExtra("b_id");
        iv_back.setOnClickListener(this);
        getRejectedDataList();
    }

    private void init() {
        sharedPreferenceData=new SharedPreferenceData(CustomerRejected.this);
        iv_back=findViewById(R.id.iv_back);
        rejectedRecycler=findViewById(R.id.rejectedRecycler);
        toolHeader=findViewById(R.id.toolHeader);
        rejectedDataList=new ArrayList<>();
        intent=getIntent();
        progressDialog=new Dialog(CustomerRejected.this);
        rejectedRecycler.setLayoutManager(new LinearLayoutManager(CustomerRejected.this,LinearLayoutManager.VERTICAL,false));

    }

    @Override
    protected void onResume() {
        super.onResume();
        getRejectedDataList();
    }

    private void getRejectedDataList()
{
    rejectedRecycler.setAdapter(null);
    progressDialog.setContentView(R.layout.loadingdialog);
    progressDialog.setCancelable(false);
    progressDialog.show();
    APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
    Call<BusinessRejectedResponse> call1=apiInterface.businessRejected(sharedPreferenceData.getId(),b_id);
    call1.enqueue(new Callback<BusinessRejectedResponse>() {
        @Override
        public void onResponse(Call<BusinessRejectedResponse> call, Response<BusinessRejectedResponse> response) {
            if (response.code()==200)
            {
                progressDialog.dismiss();
               rejectedDataList=response.body().getData();
                rejectedRecycler.setAdapter(new CustomerRejectedAdapter(CustomerRejected.this,rejectedDataList));
            }
            else
            {
                progressDialog.dismiss();
                Converter<ResponseBody, ApiError> converter =
                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                ApiError error;
                try {
                    error = converter.convert(response.errorBody());
                    ApiError.StatusBean status=error.getStatus();
                    Toast.makeText(CustomerRejected.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                } catch (IOException e) { e.printStackTrace(); }
            }
        }

        @Override
        public void onFailure(Call<BusinessRejectedResponse> call, Throwable t) {
            progressDialog.dismiss();
            Toast.makeText(CustomerRejected.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
        }
    });
}
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}