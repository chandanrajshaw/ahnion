package kashyap.chandan.go_feedback.customerPannel;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import kashyap.chandan.go_feedback.Constants;
import kashyap.chandan.go_feedback.CustomItemClickListener;
import kashyap.chandan.go_feedback.DetailData;
import kashyap.chandan.go_feedback.ImageAdapter;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.RealPathUtil;
import kashyap.chandan.go_feedback.ReceiptImageAdapter;
import kashyap.chandan.go_feedback.ResponseClasses.DeleteImageResponse;
import kashyap.chandan.go_feedback.ResponseClasses.EditFeedbackResponse;
import kashyap.chandan.go_feedback.ResponseClasses.ReceiptDeleteResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.ViewDetailResponse;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class EditRejectedFeedback extends AppCompatActivity implements View.OnClickListener {
    TextView btnSendFeedback,btnSendDraft, googlerating, tvAddress, tvname,rejectedReason,date;
    RecyclerView imagelist, receiptRecycler;
    String reason;
    RelativeLayout uploadReceipt,sharePics;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    ArrayList<Uri> feedbackArrayList = new ArrayList<>();
    ArrayList<Uri> uploadReceiptArraylist = new ArrayList<>();
    SimpleRatingBar servicerating, ambiancerating, staffrating, ratingbar;
    List<MultipartBody.Part> UploadReceiptparts = new ArrayList<>();
    List<MultipartBody.Part> FeedbackPicsParts = new ArrayList<>();
    private File imageFeedback,imageReceipt;
    List<String> images=new ArrayList<>();
    DetailData detailData;
    List<String> receipts=new ArrayList<>();
    String Id;
    LinearLayout rejectionlayout;

    FrameLayout feedbackFrame,receiptFrame;
EditText customer_review;
View views;
    private int REQUEST_CODE_READ_STORAGE = 101, RECEIPT_CODE = 102;
    Intent intent;
    ImageView image,iv_back,removereceipt,remove;
String b_id;
SharedPreferenceData sharedPreferenceData;
Dialog progressDialog;
    DetailData result;
    private ImageView feedbackImage,receiptImage;
    private Dialog cameradialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_screen);
        init();

        Id=intent.getStringExtra("id");
        views.setVisibility(View.GONE);
        btnSendDraft.setVisibility(View.GONE);
        uploadReceipt.setOnClickListener(this);
        btnSendFeedback.setOnClickListener(this);
        btnSendDraft.setOnClickListener(this);
        sharePics.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        btnSendFeedback.setText("Re-Submit");
        remove.setOnClickListener(this);
        removereceipt.setOnClickListener(this);
      getFeedbackData();



    }
private void getFeedbackData()
{
    progressDialog.setContentView(R.layout.loadingdialog);
    progressDialog.setCancelable(false);
    progressDialog.show();
    APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
    Call<ViewDetailResponse> call=apiInterface.getDetail(Id);
    call.enqueue(new Callback<ViewDetailResponse>() {
        @Override
        public void onResponse(Call<ViewDetailResponse> call, Response<ViewDetailResponse> response) {
            if (response.code()==200)
            {
                progressDialog.dismiss();
                detailData=response.body().getData();
                if (detailData.getRecipt().isEmpty())
                { receiptRecycler.setVisibility(View.GONE);}
                else
                {
                    receipts= Arrays.asList(detailData.getRecipt().split(","));
                    receiptRecycler.setAdapter(new ReceiptImageAdapter(EditRejectedFeedback.this,receipts, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, String id, String value) {
                            progressDialog.setContentView(R.layout.loadingdialog);
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                            Call<ReceiptDeleteResponse>call=apiInterface.receiptDelete(detailData.getId(),id);
                            call.enqueue(new Callback<ReceiptDeleteResponse>() {
                                @Override
                                public void onResponse(Call<ReceiptDeleteResponse> call, Response<ReceiptDeleteResponse> response) {
                                    if (response.code()==200)
                                    {
                                        progressDialog.dismiss();
                                       getFeedbackData();

                                    }
                                    else
                                    {
                                        progressDialog.dismiss();
                                        Converter<ResponseBody, ApiError> converter =
                                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                        ApiError error;
                                        try {
                                            error = converter.convert(response.errorBody());
                                            ApiError.StatusBean status=error.getStatus();
                                            Toast.makeText(EditRejectedFeedback.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                        } catch (IOException e) { e.printStackTrace(); }
                                    }
                                }

                                @Override
                                public void onFailure(Call<ReceiptDeleteResponse> call, Throwable t) {

                                }
                            });
                        }
                    }));
                    receiptRecycler.setVisibility(View.VISIBLE);
                }

                if (detailData.getImagees().isEmpty())
                {
                    imagelist.setVisibility(View.GONE);
                }
                else
                {
                    images= Arrays.asList(detailData.getImagees().split(","));
                    imagelist.setAdapter(new ImageAdapter(EditRejectedFeedback.this, images, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, String id, String value) {
                            progressDialog.setContentView(R.layout.loadingdialog);
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                            Call<DeleteImageResponse>call=apiInterface.deleteImage(Id,id);
                            call.enqueue(new Callback<DeleteImageResponse>() {
                                @Override
                                public void onResponse(Call<DeleteImageResponse> call, Response<DeleteImageResponse> response) {
                                    if (response.code()==200)
                                    {
                                        progressDialog.dismiss();
                                       getFeedbackData();
                                    }
                                    else
                                    {
                                        progressDialog.dismiss();
                                        Converter<ResponseBody, ApiError> converter =
                                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                        ApiError error;
                                        try {
                                            error = converter.convert(response.errorBody());
                                            ApiError.StatusBean status=error.getStatus();
                                            Toast.makeText(EditRejectedFeedback.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                        } catch (IOException e) { e.printStackTrace(); }
                                    }
                                }

                                @Override
                                public void onFailure(Call<DeleteImageResponse> call, Throwable t) {

                                }
                            });
                        }
                    }));
                    imagelist.setVisibility(View.VISIBLE);
                }

                googlerating.setText(String.valueOf(detailData.getRating()));
                tvname.setText(detailData.getName());
                tvAddress.setText(detailData.getVicinity());
                staffrating.setRating(Float.parseFloat(detailData.getAttitudeOfStass()));
                ambiancerating.setRating(Float.parseFloat(detailData.getAmbience()));
                ratingbar.setRating(Float.parseFloat(detailData.getRating()));
                servicerating.setRating(Float.parseFloat(detailData.getService()));
                String ref=String.valueOf(detailData.getReferanceImage());
                rejectedReason.setText("RejectionReason:-"+detailData.getRejectionResion()+","+detailData.getReasonopt());
                rejectedReason.setVisibility(View.VISIBLE);
                System.out.println(ref);
                if (ref==null||ref.isEmpty()) {
//            image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nopreviewavailable));
                }
                else
                {
//                    Picasso.get().load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+ref+"&key="+getResources().getString(R.string.googlemap)).into(image);
                    Picasso.get().load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+ref+"&key="+ Constants.KEY).into(image);

                }

                customer_review.setText(detailData.getCustomerReview());



                if ( detailData.getReasonopt()==null)
                {
                    reason= ((String)detailData.getRejectionResion());
                }
                else if (((String) detailData.getReasonopt())!=null)
                {reason= ((String) detailData.getReasonopt()).concat(",").concat((String)detailData.getRejectionResion());}

                if (reason.isEmpty()) {
                    rejectedReason.setVisibility(View.GONE);
                    rejectionlayout.setVisibility(View.GONE);
                }
                else
                {
                    rejectedReason.setText(reason);
                    rejectedReason.setVisibility(View.VISIBLE);
                    rejectionlayout.setVisibility(View.VISIBLE);
                }

                try {
                  date.setText(formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,detailData.getDatetime().substring(0,10)));
                } catch (ParseException e) { e.printStackTrace(); }

            }
            else
            {
                progressDialog.dismiss();
                Converter<ResponseBody, ApiError> converter =
                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                ApiError error;
                try {
                    error = converter.convert(response.errorBody());
                    ApiError.StatusBean status=error.getStatus();
                    Toast.makeText(EditRejectedFeedback.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                } catch (IOException e) { e.printStackTrace(); }
            }
        }

        @Override
        public void onFailure(Call<ViewDetailResponse> call, Throwable t) {

        }
    });
}
    @Override
    protected void onRestart() {
        super.onRestart();
//        if (sharedPreferenceData.getId()==null)
//        {
//            Intent intent=new Intent(EditFeedbackScreen.this, MainActivity.class);
//            startActivity(intent);
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void showChooser(int CODE) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), CODE);
    }

    private void init() {
        date=findViewById(R.id.date);
        rejectionlayout=findViewById(R.id.rejectionlayout);
        rejectedReason=findViewById(R.id.rejectedReason);

        remove=findViewById(R.id.remove);
        removereceipt=findViewById(R.id.removereceipt);
        receiptFrame=findViewById(R.id.receiptFrame);
        feedbackImage=findViewById(R.id.feedbackImage);
        receiptImage=findViewById(R.id.receiptImage);
        views=findViewById(R.id.views);
        feedbackFrame=findViewById(R.id.feedbackFrame);
        iv_back=findViewById(R.id.iv_back);
        customer_review=findViewById(R.id.customer_review);
        receiptRecycler = findViewById(R.id.receiptRecycler);
        image = findViewById(R.id.image);
        intent = getIntent();
        btnSendDraft=findViewById(R.id.btnSendDraft);
        sharedPreferenceData=new SharedPreferenceData(EditRejectedFeedback.this);
        uploadReceipt = findViewById(R.id.uploadReceipt);
        tvAddress = findViewById(R.id.tvAddress);
        googlerating = findViewById(R.id.googlerating);
        btnSendFeedback = findViewById(R.id.btnSendFeedback);
        imagelist = findViewById(R.id.feedbackpics);
        sharePics = findViewById(R.id.sharePics);
        tvname = findViewById(R.id.tvname);
        servicerating = findViewById(R.id.servicerating);
        ambiancerating = findViewById(R.id.ambiancerating);
        staffrating = findViewById(R.id.staffrating);
        ratingbar = findViewById(R.id.ratingbar);
        progressDialog=new Dialog(EditRejectedFeedback.this);
        receiptRecycler.setLayoutManager(new LinearLayoutManager(EditRejectedFeedback.this, LinearLayoutManager.HORIZONTAL, false));
        imagelist.setLayoutManager(new LinearLayoutManager(EditRejectedFeedback.this, LinearLayoutManager.HORIZONTAL, false));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.remove:
                imageReceipt=null;
                feedbackFrame.setVisibility(View.GONE);
                break;
            case R.id.removereceipt:
                imageFeedback=null;
                receiptFrame.setVisibility(View.GONE);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.btnSendFeedback:
                editFeedbackSubmit();
                break;
            case R.id.sharePics:
//                feedbackArrayList.clear();
//                imagelist.removeAllViews();
//                imagelist.setAdapter(null);
                if (ContextCompat.checkSelfPermission(EditRejectedFeedback.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(EditRejectedFeedback.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(EditRejectedFeedback.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        askPermission();
                    }
                } else {
                    ImageView camera, folder;
                    cameradialog = new Dialog(EditRejectedFeedback.this);
                    cameradialog.setContentView(R.layout.dialogboxcamera);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    cameradialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                    cameradialog.show();
                    cameradialog.setCancelable(true);
                    cameradialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Window window = cameradialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    camera = cameradialog.findViewById(R.id.camera);
                    folder = cameradialog.findViewById(R.id.gallery);
                    folder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            feedbackImage.setVisibility(View.GONE);
                            imageFeedback=null;
                            feedbackFrame.setVisibility(View.GONE);
                            showChooser(REQUEST_CODE_READ_STORAGE);
                            cameradialog.dismiss();
                        }
                    });
                    camera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            imagelist.removeAllViewsInLayout();
//                            imagelist.setAdapter(null);
//                            imagelist.setVisibility(View.GONE);
                            feedbackFrame.setVisibility(View.VISIBLE);
                            feedbackArrayList.clear();
                            feedbackImage.setVisibility(View.VISIBLE);
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, 104);
                            cameradialog.dismiss();
                        }
                    });
                }
                break;
            case R.id.uploadReceipt:
//                uploadReceiptArraylist.clear();
//                receiptRecycler.removeAllViews();
//                receiptRecycler.setAdapter(null);
                if (ContextCompat.checkSelfPermission(EditRejectedFeedback.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(EditRejectedFeedback.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(EditRejectedFeedback.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        askPermission();
                    }
                } else {
                    ImageView camera, folder;
                    cameradialog = new Dialog(EditRejectedFeedback.this);
                    cameradialog.setContentView(R.layout.dialogboxcamera);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    cameradialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                    cameradialog.show();
                    cameradialog.setCancelable(true);
                    cameradialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Window window = cameradialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    camera = cameradialog.findViewById(R.id.camera);
                    folder = cameradialog.findViewById(R.id.gallery);
                    folder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            imageReceipt=null;
                            receiptFrame.setVisibility(View.GONE);
                            receiptFrame.setVisibility(View.GONE);
                            showChooser(RECEIPT_CODE);
                            cameradialog.dismiss();
                        }
                    });
                    camera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            receiptRecycler.removeAllViewsInLayout();
//                            receiptRecycler.setAdapter(null);
//                            receiptRecycler.setVisibility(View.GONE);
                            receiptFrame.setVisibility(View.VISIBLE);
                            receiptImage.setVisibility(View.VISIBLE);
//                            uploadReceiptArraylist.clear();
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, 103);
                            cameradialog.dismiss();
                        }
                    });
                }
                break;
            case R.id.btnSendDraft:
//                sendDraft();
                editFeedbackDraft();
                break;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_READ_STORAGE) {
                if (resultData != null) {
                    if (resultData.getClipData() != null) {
                        imagelist.removeAllViews();
                        imagelist.setAdapter(null);
                        int count = resultData.getClipData().getItemCount();
                        int currentItem = 0;
                        while (currentItem < count) {
                            Uri imageUri = resultData.getClipData().getItemAt(currentItem).getUri();
                            currentItem = currentItem + 1;
                            Log.d("Uri Selected", imageUri.toString());
                            try {
                                feedbackArrayList.add(imageUri);
                                imagelist.setAdapter(new FeedbackImageAdapter(EditRejectedFeedback.this, feedbackArrayList));
                                imagelist.setVisibility(View.VISIBLE);
                            } catch (Exception e) {
                                Log.e("TAG", "File select error", e);
                            }
                        }
                    }
                    else if (resultData.getData() != null) {
                        final Uri uri = resultData.getData();
                        Log.i("TAG", "Uri = " + uri.toString());
                        try {
                            feedbackArrayList.add(uri);
                            imagelist.setAdapter(new FeedbackImageAdapter(EditRejectedFeedback.this, feedbackArrayList));
                            imagelist.setVisibility(View.VISIBLE);

                        } catch (Exception e) {
                            Log.e("TAG", "File select error", e);
                        }
                    }
                }
            }
            if (requestCode==RECEIPT_CODE) {
                if (resultData!=null) {
                    receiptRecycler.removeAllViews();
                    receiptRecycler.setAdapter(null);
                    if (resultData.getClipData()!=null) {
                        int count = resultData.getClipData().getItemCount();
                        int currentItem = 0;
                        while (currentItem < count) {
                            Uri imageUri = resultData.getClipData().getItemAt(currentItem).getUri();
                            currentItem = currentItem + 1;
                            Log.d("Uri Selected", imageUri.toString());
                            try {
                                uploadReceiptArraylist.add(imageUri);
                                receiptRecycler.setAdapter(new UploadReceiptAdapter(EditRejectedFeedback.this, uploadReceiptArraylist));
                           receiptRecycler.setVisibility(View.VISIBLE);
                            } catch (Exception e) {
                                Log.e("TAG", "File select error", e);
                            }
                        }
                    } else if (resultData.getData() != null) {
                        final Uri uri = resultData.getData();
                        Log.i("TAG", "Uri = " + uri.toString());
                        try {
                            uploadReceiptArraylist.add(uri);
                            receiptRecycler.setAdapter(new UploadReceiptAdapter(EditRejectedFeedback.this, uploadReceiptArraylist));
                            receiptRecycler.setVisibility(View.VISIBLE);

                        } catch (Exception e) {
                            Log.e("TAG", "File select error", e);
                        }
                    }
                }
            }
            else if (requestCode == 103 ) {
                Bitmap converetdImage = (Bitmap) resultData.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                receiptImage.setImageBitmap(converetdImage);
                receiptImage.setVisibility(View.VISIBLE);
                receiptFrame.setVisibility(View.VISIBLE);
                File dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                imageReceipt = new File(dirPath, "receiptImage.jpg");
                try {
                    if(!dirPath.isDirectory()) {
                        dirPath.mkdirs();
                    }
                    imageReceipt.createNewFile();
                }catch (Exception e){}
                FileOutputStream fo;
                try {
                    fo = new FileOutputStream(imageReceipt);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            else if (requestCode == 104 ) {
                Bitmap converetdImage = (Bitmap) resultData.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                feedbackImage.setImageBitmap(converetdImage);
                feedbackImage.setVisibility(View.VISIBLE);
                feedbackFrame.setVisibility(View.VISIBLE);
                File dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                imageFeedback = new File(Environment.getExternalStorageDirectory(), "feedbackImage.jpg");
                try {
                    if(!dirPath.isDirectory()) {
                        dirPath.mkdirs();
                    }
                    imageFeedback.createNewFile();
                }catch (Exception e){}
                FileOutputStream fo;
                try {
                    fo = new FileOutputStream(imageFeedback);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void askPermission() {
        if (ContextCompat.checkSelfPermission(EditRejectedFeedback.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(EditRejectedFeedback.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        && ContextCompat.checkSelfPermission(EditRejectedFeedback.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA}, 100);
        }
    }

    private void editFeedbackSubmit() {
        String comments=customer_review.getText().toString();
        float ser_rating=servicerating.getRating();
        float staf_rating=staffrating.getRating();
        float amb_rating=ambiancerating.getRating();
        float allratingbar=ratingbar.getRating();
        if (allratingbar==0 &&staf_rating==0 &&amb_rating==0 &&ser_rating==0)
            Toast.makeText(this, "Give Ratings", Toast.LENGTH_SHORT).show();
        else if (ser_rating==0)
            Toast.makeText(this, "Give Customer Service Ratings", Toast.LENGTH_SHORT).show();
        else if (staf_rating==0)
            Toast.makeText(this, "Give Product & Service Rating", Toast.LENGTH_SHORT).show();
        else if (amb_rating==0)
            Toast.makeText(this, "Give Money Experience Ratings", Toast.LENGTH_SHORT).show();
        else if (allratingbar==0)
            Toast.makeText(this, "Give Overall Ratings", Toast.LENGTH_SHORT).show();
        else if (comments.isEmpty())
            Toast.makeText(this, "Give Your Experience", Toast.LENGTH_SHORT).show();
else
        {

            String overAllRating = String.valueOf(ratingbar.getRating());
            String staffRatting = String.valueOf(staffrating.getRating());
            String ambienceRating = String.valueOf(ambiancerating.getRating());
            String servicRating = String.valueOf(servicerating.getRating());
            RequestBody status = RequestBody.create(MediaType.parse("multipart/form-data"),"2" );
            RequestBody feedback_id = RequestBody.create(MediaType.parse("multipart/form-data"),detailData.getId() );
            RequestBody u_id = RequestBody.create(MediaType.parse("multipart/form-data"),sharedPreferenceData.getId() );
            RequestBody id = RequestBody.create(MediaType.parse("multipart/form-data"), detailData.getBId());
            RequestBody allRating = RequestBody.create(MediaType.parse("multipart/form-data"), overAllRating);
            RequestBody aRating = RequestBody.create(MediaType.parse("multipart/form-data"), ambienceRating);
            RequestBody sRating = RequestBody.create(MediaType.parse("multipart/form-data"), staffRatting);
            RequestBody serRating = RequestBody.create(MediaType.parse("multipart/form-data"), servicRating);
            RequestBody cmnts = RequestBody.create(MediaType.parse("multipart/form-data"), comments);
            if (uploadReceiptArraylist.isEmpty()&&receiptImage==null)
            {
                MultipartBody.Part uploadReceiptPart = null;
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                uploadReceiptPart = MultipartBody.Part.createFormData("receipt[]", "", requestFile);
                UploadReceiptparts.add(uploadReceiptPart);
            }

            else
            {
                if ((!uploadReceiptArraylist.isEmpty())&&imageReceipt==null)
                {
                    for (Uri uri : uploadReceiptArraylist)
                        UploadReceiptparts.add(prepareFilePart("receipt[]", uri));    //here image is// }
                }
                else if (uploadReceiptArraylist.isEmpty()&&imageReceipt!=null)
                {
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageReceipt);
                    MultipartBody.Part receiptImg = MultipartBody.Part.createFormData("receipt[]", imageReceipt.getName(), requestFile);
                    UploadReceiptparts.add(receiptImg);
                }


            }
            if ((!feedbackArrayList.isEmpty())||imageFeedback!=null)
            {
                if ((!feedbackArrayList.isEmpty())&&imageFeedback==null) {
                    for (Uri feedback : feedbackArrayList)
                        FeedbackPicsParts.add(prepareFilePart("image[]", feedback));
                }
                else if ((feedbackArrayList.isEmpty()&&imageFeedback!=null))
                {
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFeedback);
                    MultipartBody.Part feedbackImg = MultipartBody.Part.createFormData("image[]", imageFeedback.getName(), requestFile);
                    FeedbackPicsParts.add(feedbackImg);
                }
            }

            progressDialog=new Dialog(EditRejectedFeedback.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            final Call<EditFeedbackResponse>call=apiInterface.editFeedback(status,feedback_id,id,u_id,allRating,sRating,aRating,serRating,cmnts,UploadReceiptparts,FeedbackPicsParts);

            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    call.enqueue(new Callback<EditFeedbackResponse>() {
                        @Override
                        public void onResponse(Call<EditFeedbackResponse> call, Response<EditFeedbackResponse> response) {
                            if (response.code()==200)
                            {
                                progressDialog.dismiss();
                                final Dialog dialog=new Dialog(EditRejectedFeedback.this);
                                dialog.setContentView(R.layout.custommessagedialog);
                                dialog.show();
                                dialog.setCancelable(false);
                                TextView ok=dialog.findViewById(R.id.ok);
                                TextView message=dialog.findViewById(R.id.message);
                                message.setText("Re-Submitted");
                                ok.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        finish();
                                        dialog.dismiss();
                                    }
                                });
                                Toast.makeText(EditRejectedFeedback.this, "Re-Submitted", Toast.LENGTH_SHORT).show();

                            }
                            else
                            {
                                progressDialog.dismiss();
                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                    Toast.makeText(EditRejectedFeedback.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }}

                        }

                        @Override
                        public void onFailure(Call<EditFeedbackResponse> call, Throwable t) {
                            progressDialog.dismiss();
                        }
                    });
                }
            };
            Thread thread=new Thread(runnable);
            thread.start();
        }





    }

    private void editFeedbackDraft() {
        String comments=customer_review.getText().toString();
        float ser_rating=servicerating.getRating();
        float staf_rating=staffrating.getRating();
        float amb_rating=ambiancerating.getRating();
        float allratingbar=ratingbar.getRating();
        if (allratingbar==0 &&staf_rating==0 &&amb_rating==0 &&ser_rating==0)
            Toast.makeText(this, "Give Ratings", Toast.LENGTH_SHORT).show();
        else if (allratingbar==0)
            Toast.makeText(this, "Give Ratings", Toast.LENGTH_SHORT).show();
        else if (staf_rating==0)
            Toast.makeText(this, "Give Staff Rating", Toast.LENGTH_SHORT).show();
        else if (amb_rating==0)
            Toast.makeText(this, "Give Ambience Ratings", Toast.LENGTH_SHORT).show();
        else if (ser_rating==0)
            Toast.makeText(this, "Give Service Ratings", Toast.LENGTH_SHORT).show();
        else if (comments.isEmpty())
            Toast.makeText(this, "Give Your Experience", Toast.LENGTH_SHORT).show();
      else
        {
            String overAllRating = String.valueOf(ratingbar.getRating());
            String staffRatting = String.valueOf(staffrating.getRating());
            String ambienceRating = String.valueOf(ambiancerating.getRating());
            String servicRating = String.valueOf(servicerating.getRating());
            RequestBody status = RequestBody.create(MediaType.parse("multipart/form-data"),"3" );
            RequestBody feedback_id = RequestBody.create(MediaType.parse("multipart/form-data"),result.getId() );
            RequestBody u_id = RequestBody.create(MediaType.parse("multipart/form-data"),sharedPreferenceData.getId() );
            RequestBody id = RequestBody.create(MediaType.parse("multipart/form-data"), result.getBId());
            RequestBody allRating = RequestBody.create(MediaType.parse("multipart/form-data"), overAllRating);
            RequestBody aRating = RequestBody.create(MediaType.parse("multipart/form-data"), ambienceRating);
            RequestBody sRating = RequestBody.create(MediaType.parse("multipart/form-data"), staffRatting);
            RequestBody serRating = RequestBody.create(MediaType.parse("multipart/form-data"), servicRating);
            RequestBody cmnts = RequestBody.create(MediaType.parse("multipart/form-data"), comments);
            if (uploadReceiptArraylist.isEmpty()&&receiptImage==null)
            {
                MultipartBody.Part uploadReceiptPart = null;
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                uploadReceiptPart = MultipartBody.Part.createFormData("receipt[]", "", requestFile);
                UploadReceiptparts.add(uploadReceiptPart);
            }

            else
            {
                if ((!uploadReceiptArraylist.isEmpty())&&imageReceipt==null)
                {
                    for (Uri uri : uploadReceiptArraylist)
                        UploadReceiptparts.add(prepareFilePart("receipt[]", uri));    //here image is// }
                }
                else if (uploadReceiptArraylist.isEmpty()&&imageReceipt!=null)
                {
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageReceipt);
                    MultipartBody.Part receiptImg = MultipartBody.Part.createFormData("receipt[]", imageReceipt.getName(), requestFile);
                    UploadReceiptparts.add(receiptImg);
                }


            }
            if ((!feedbackArrayList.isEmpty())||imageFeedback!=null)
            {
                if ((!feedbackArrayList.isEmpty())&&imageFeedback==null) {
                    for (Uri feedback : feedbackArrayList)
                        FeedbackPicsParts.add(prepareFilePart("image[]", feedback));
                }
                else if ((feedbackArrayList.isEmpty()&&imageFeedback!=null))
                {
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFeedback);
                    MultipartBody.Part feedbackImg = MultipartBody.Part.createFormData("image[]", imageFeedback.getName(), requestFile);
                    FeedbackPicsParts.add(feedbackImg);
                }
            }

            progressDialog=new Dialog(EditRejectedFeedback.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            final Call<EditFeedbackResponse>call=apiInterface.editFeedback(status,feedback_id,id,u_id,allRating,sRating,aRating,serRating,cmnts,UploadReceiptparts,FeedbackPicsParts);

            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    call.enqueue(new Callback<EditFeedbackResponse>() {
                        @Override
                        public void onResponse(Call<EditFeedbackResponse> call, Response<EditFeedbackResponse> response) {
                            if (response.code()==200)
                            {
                                progressDialog.dismiss();
                                final Dialog dialog=new Dialog(EditRejectedFeedback.this);
                                dialog.setContentView(R.layout.custommessagedialog);
                                dialog.show();
                                dialog.setCancelable(false);
                                TextView ok=dialog.findViewById(R.id.ok);
                                TextView message=dialog.findViewById(R.id.message);
                                message.setText("Saved As Draft");
                                ok.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        finish();
                                        dialog.dismiss();
                                    }
                                });
                                Toast.makeText(EditRejectedFeedback.this, "Saved As Draft", Toast.LENGTH_SHORT).show();

                            }
                            else
                            {
                                progressDialog.dismiss();
                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                    Toast.makeText(EditRejectedFeedback.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }}

                        }

                        @Override
                        public void onFailure(Call<EditFeedbackResponse> call, Throwable t) {
                            progressDialog.dismiss();
                        }
                    });
                }
            };
            Thread thread=new Thread(runnable);
            thread.start();


        }



    }

    private MultipartBody.Part prepareFilePart (String partName, Uri fileUri){
        String path = RealPathUtil.getRealPath(EditRejectedFeedback.this, fileUri);
        File file = new File(path);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }
}