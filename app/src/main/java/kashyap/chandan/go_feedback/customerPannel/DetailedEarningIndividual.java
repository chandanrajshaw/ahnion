package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.List;

import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPurchaseData;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPurchaseResponse;
import kashyap.chandan.go_feedback.ResponseClasses.PurchaseDataItem;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class DetailedEarningIndividual extends AppCompatActivity {
RecyclerView detailEarningRecycler;
Intent intent;
TextView total_earning,header_title;
SharedPreferenceData sharedPreferenceData;
String b_id;
Double price=0.0;
ImageView iv_back;
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_earning_individual);
        init();
        b_id=intent.getStringExtra("id");
        detailEarningRecycler.setLayoutManager(new LinearLayoutManager(DetailedEarningIndividual.this,LinearLayoutManager.VERTICAL,false));
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        header_title=findViewById(R.id.header_title);
        total_earning=findViewById(R.id.total_earning);
        sharedPreferenceData=new SharedPreferenceData(DetailedEarningIndividual.this);
        detailEarningRecycler=findViewById(R.id.detailEarningRecycler);
        intent=getIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPurchased();
    }

    private void getPurchased()
    {
        progressDialog=new Dialog(DetailedEarningIndividual.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<BusinessPurchaseResponse> call1=apiInterface.businessPurchased(sharedPreferenceData.getId(),b_id);
        call1.enqueue(new Callback<BusinessPurchaseResponse>() {
            @Override
            public void onResponse(Call<BusinessPurchaseResponse> call, Response<BusinessPurchaseResponse> response) {
                if (response.code()==200) {
                    progressDialog.dismiss();
                    List<BusinessPurchaseData> purchaseDataList=response.body().getData();
                    System.out.println(purchaseDataList.size());
                    if (purchaseDataList.size()==0||purchaseDataList==null)
                    {
                        Toast.makeText(DetailedEarningIndividual.this, "No Data Available", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        price=0.0;
                      for (BusinessPurchaseData data:purchaseDataList)
                      {
                         price=price+Double.parseDouble(data.getAmount());
                          header_title.setText(data.getName());

                      }
                        detailEarningRecycler.setAdapter(new DetailedEarningIndividualAdapter(DetailedEarningIndividual.this,purchaseDataList));
                        total_earning.setText("$"+String.valueOf(price));
                    }



                }
                else
                {
                    progressDialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(DetailedEarningIndividual.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<BusinessPurchaseResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(DetailedEarningIndividual.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}