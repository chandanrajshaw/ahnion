package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPurchaseData;

public class CustomerPurchased extends AppCompatActivity implements View.OnClickListener {
RecyclerView purchasedRecycler;
Intent intent;
ImageView iv_back;
TextView toolHeader;
    List<BusinessPurchaseData>purchaseDataList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_purchased);
        init();
        toolHeader.setText(intent.getStringExtra("b_name"));
        Bundle bundle=intent.getBundleExtra("bundle");
        purchaseDataList= (List<BusinessPurchaseData>) bundle.getSerializable("purchase");
        purchasedRecycler.setLayoutManager(new LinearLayoutManager(CustomerPurchased.this,LinearLayoutManager.VERTICAL,false));
        purchasedRecycler.setAdapter(new CustomerPurchasedAdapter(CustomerPurchased.this,purchaseDataList));
        iv_back.setOnClickListener(this);
    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        toolHeader=findViewById(R.id.toolHeader);
        purchaseDataList=new ArrayList<>();
        intent=getIntent();
        purchasedRecycler=findViewById(R.id.purchasedRecycler);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}