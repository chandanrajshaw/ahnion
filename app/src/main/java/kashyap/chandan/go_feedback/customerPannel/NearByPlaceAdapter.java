package kashyap.chandan.go_feedback.customerPannel;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.Constants;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.AddBusinessResponse;
import kashyap.chandan.go_feedback.customerPannel.maps.Result;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class NearByPlaceAdapter extends RecyclerView.Adapter<NearByPlaceAdapter.MyViewHolder> {
    Context context;
    List<Result> results=new ArrayList<>();
    private Dialog progressDialog;
    String reference;
    public NearByPlaceAdapter(Context context, List<Result> results) {
        this.context=context;
        this.results=results;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(context).inflate(R.layout.place_detail_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
//         PlacesClient placesClient= (PlacesClient) results.get(position);
        if (results.get(position).getPhotos().size()>0)
        {
//            Picasso.get().load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+results.get(position).getPhotos().get(0).getPhotoReference()+"&key="+context.getResources().getString(R.string.googlemap)).into(holder.image);
            Picasso.get().load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+results.get(position).getPhotos().get(0).getPhotoReference()+"&key="+ Constants.KEY).into(holder.image);

        }
        else
        { Picasso.get().load(results.get(position).getIcon()).into(holder.image); }
         holder.placeName.setText(results.get(position).getName());
         holder.placeVincity.setText(results.get(position).getVicinity());
         holder.placeRating.setText(String.valueOf(results.get(position).getRating()));
//         boolean t=results.get(position).getOpeningHours().getOpenNow();
//        if (results.get(position).getOpeningHours().getOpenNow())
//        holder.placePhoneNumber.setText(context.getResources().getString(R.string.Open));
//         else
        holder.placeType.setText(results.get(position).getTypes().get(1));
        holder.placedistance.setText(results.get(position).getScope());

//        holder.
        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Result result=results.get(position);
//        if (sharedPreferenceData.getId()==null)
//        {
//            Intent intent=new Intent(context,MainActivity.class);
////            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(intent);
//        }
//        else
//        {
                progressDialog=new Dialog(context);
                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);
                Activity activity=(Activity)context;
                if (!activity.isFinishing()) {
                    progressDialog.show();
                }
                Runnable runnable=new Runnable() {
                    @Override
                    public void run() {
                        final APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
Integer size=result.getPhotos().size();
if (size==null||result.getPhotos().isEmpty()||result.getPhotos().size()<=0)
{
    reference="";
}
else
{
    reference=result.getPhotos().get(0).getPhotoReference();
}
                        Call<AddBusinessResponse> call1=apiInterface.registerBusiness(result.getPlaceId(),result.getName(),String.valueOf(result.getGeometry().getLocation().getLat()),String.valueOf(result.getGeometry().getLocation().getLng()),result.getReference(),result.getScope(),result.getVicinity(),String.valueOf(result.getRating()),reference);
                        call1.enqueue(new Callback<AddBusinessResponse>() {
                            @Override
                            public void onResponse(Call<AddBusinessResponse> call, Response<AddBusinessResponse> response) {
                                if (response.code()==200)
                                {
                                    progressDialog.dismiss();
//                                if (sharedPreferenceData.getId()!=null)
//                                {
                                    Intent intent=new Intent(context, FeedbackScreen.class);
                                    Bundle bundle=new Bundle();
                                    bundle.putSerializable("result",result);
                                    intent.putExtra("bundle",bundle);
                                    intent.putExtra("b_id",String.valueOf(response.body().getBId()));
                                    context.startActivity(intent);
//                                }
//                                else
//                                {
//                                    Intent intent=new Intent(context, MainActivity.class);
////                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
//                                }

                                }
                                else
                                {
                                    progressDialog.dismiss();
                                    Converter<ResponseBody, ApiError> converter =
                                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                    ApiError error;
                                    try {
                                        error = converter.convert(response.errorBody());
                                        ApiError.StatusBean status=error.getStatus();
                                        Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                                    } catch (IOException e) { e.printStackTrace(); }}
                            }

                            @Override
                            public void onFailure(Call<AddBusinessResponse> call, Throwable t) {

                            }
                        });
                    }
                };
                Thread thread=new Thread(runnable);
                thread.start();

            }

//    }
        });
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView placedistance,placeRating,placeName,placeVincity, placeType;
        ImageView image;
        LinearLayout detail;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            detail=itemView.findViewById(R.id.detail);
            placedistance=itemView.findViewById(R.id.placedistance);
            placeRating=itemView.findViewById(R.id.placeRating);
            placeName=itemView.findViewById(R.id.placeName);
            placeVincity=itemView.findViewById(R.id.placeVincity);
            placeType =itemView.findViewById(R.id.placeType);
            image=itemView.findViewById(R.id.image);
        }
    }
}
