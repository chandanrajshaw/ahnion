package kashyap.chandan.go_feedback.customerPannel.searchlocation;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResultsItem implements Serializable {

	@SerializedName("formatted_address")
	private String formattedAddress;

	@SerializedName("types")
	private List<String> types;

	@SerializedName("business_status")
	private String businessStatus;

	@SerializedName("icon")
	private String icon;

	@SerializedName("rating")
	private double rating;

	@SerializedName("photos")
	private List<PhotosItem> photos;

	@SerializedName("reference")
	private String reference;

	@SerializedName("user_ratings_total")
	private int userRatingsTotal;

	@SerializedName("name")
	private String name;

	@SerializedName("opening_hours")
	private OpeningHours openingHours;

	@SerializedName("geometry")
	private Geometry geometry;

	@SerializedName("plus_code")
	private PlusCode plusCode;

	@SerializedName("place_id")
	private String placeId;

	@SerializedName("permanently_closed")
	private boolean permanentlyClosed;

	@SerializedName("price_level")
	private int priceLevel;

	public String getFormattedAddress(){
		return formattedAddress;
	}

	public List<String> getTypes(){
		return types;
	}

	public String getBusinessStatus(){
		return businessStatus;
	}

	public String getIcon(){
		return icon;
	}

	public double getRating(){
		return rating;
	}

	public List<PhotosItem> getPhotos(){
		return photos;
	}

	public String getReference(){
		return reference;
	}

	public int getUserRatingsTotal(){
		return userRatingsTotal;
	}

	public String getName(){
		return name;
	}

	public OpeningHours getOpeningHours(){
		return openingHours;
	}

	public Geometry getGeometry(){
		return geometry;
	}

	public PlusCode getPlusCode(){
		return plusCode;
	}

	public String getPlaceId(){
		return placeId;
	}

	public boolean isPermanentlyClosed(){
		return permanentlyClosed;
	}

	public int getPriceLevel(){
		return priceLevel;
	}
}