package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.AllRedemptionResponse;
import kashyap.chandan.go_feedback.ResponseClasses.CustomerBalanceResponse;
import kashyap.chandan.go_feedback.ResponseClasses.RedemptionDataItem;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class AllRedemption extends AppCompatActivity {
RecyclerView allRedemptionRecycler;
    Dialog progressDialog;
    SharedPreferenceData sharedPreferenceData;
    TextView tvredeem,tvavailableBalance;
    ImageView iv_back;
    Double price=0.0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_redemption);
        init();
        allRedemptionRecycler.setLayoutManager(new LinearLayoutManager(AllRedemption.this,LinearLayoutManager.VERTICAL,false));
    getCustomerBalance();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCustomerBalance();
        redeemList();
    }
    private void getCustomerBalance()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<CustomerBalanceResponse>call=apiInterface.getCustomerBalance(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<CustomerBalanceResponse>() {
                    @Override
                    public void onResponse(Call<CustomerBalanceResponse> call, Response<CustomerBalanceResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            tvavailableBalance.setText("$"+response.body().getData().getCurentBal());
                            tvredeem.setText(response.body().getData().getTotalRedeem());
                        }
                        else
                        {
                            tvavailableBalance.setText("$0");
                            tvredeem.setText("$0");
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(AllRedemption.this, "No Transaction", Toast.LENGTH_LONG).show();
                                if (response.code()==401)
                                {
                                    tvavailableBalance.setText("$0");
                                    tvredeem.setText("$0");
                                }
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<CustomerBalanceResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(AllRedemption.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
    private void init() {
        tvavailableBalance=findViewById(R.id.tvavailableBalance);
        tvredeem =findViewById(R.id.tvredeem);
        allRedemptionRecycler=findViewById(R.id.allRedemptionRecycler);
        progressDialog=new Dialog(AllRedemption.this);
        sharedPreferenceData=new SharedPreferenceData(AllRedemption.this);
        iv_back=findViewById(R.id.iv_back);
    }
    private void redeemList()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<AllRedemptionResponse> call=apiInterface.getRedeemList(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<AllRedemptionResponse>() {
                    @Override
                    public void onResponse(Call<AllRedemptionResponse> call, Response<AllRedemptionResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            List<RedemptionDataItem> data=response.body().getData();
                            allRedemptionRecycler.setAdapter(new AllRedemptionAdapter(AllRedemption.this,response.body().getData()) );
                        for (int i=0;i<data.size();i++)
                        {
                            System.out.println(data.get(i).getAmount());
                             price=price+Double.parseDouble(data.get(i).getAmount());
                        }
//                        tvredeem.setText("$"+String.valueOf(price));
                        }
                        else
                        {
//                            tvredeem.setText("$0.0");
                            progressDialog.dismiss();
                            Toast.makeText(AllRedemption.this, "No Redeems", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AllRedemptionResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(AllRedemption.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}