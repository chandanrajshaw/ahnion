package kashyap.chandan.go_feedback.customerPannel.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import kashyap.chandan.go_feedback.AdapterClickListner;
import kashyap.chandan.go_feedback.AdapterClickListnerPlace;
import kashyap.chandan.go_feedback.Constants;
import kashyap.chandan.go_feedback.MainActivity;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.AddBusinessResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.customerPannel.FeedbackScreen;
import kashyap.chandan.go_feedback.customerPannel.NearByPlaceAdapter;
import kashyap.chandan.go_feedback.customerPannel.SelectedBusiness;
import kashyap.chandan.go_feedback.customerPannel.maps.APIClient;
import kashyap.chandan.go_feedback.customerPannel.maps.ApiInterface;
import kashyap.chandan.go_feedback.customerPannel.maps.PlacesPOJO;
import kashyap.chandan.go_feedback.customerPannel.maps.PlacesResults;
import kashyap.chandan.go_feedback.customerPannel.maps.Result;
import kashyap.chandan.go_feedback.customerPannel.maps.ResultDistanceMatrix;
import kashyap.chandan.go_feedback.customerPannel.maps.StoreModel;
import kashyap.chandan.go_feedback.customerPannel.searchlocation.ResultsItem;
import kashyap.chandan.go_feedback.customerPannel.searchlocation.SearchPlaceResponse;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
//https://www.zoftino.com/current-location-and-nearby-places-android-example
//https://www.journaldev.com/13911/google-places-api
public class NearByFragment extends Fragment implements OnMapReadyCallback {
//    static
//    {
//        System.loadLibrary("api-keys");
//    }
//    public native static String getGoogleAPI();
//    private static final String KEY=getGoogleAPI();
    GoogleMap googleMap;
    SharedPreferenceData sharedPreferenceData;
    private static final int ALL_PERMISSIONS_RESULT =1;
    RecyclerView nearByPlaceRecycler,placeSuggestionRecycler;
    SupportMapFragment mapFragment;
    TextView viewAll;
    LinearLayoutManager horizontalLayoutManager;
    FusedLocationProviderClient mFusedLocationClient;
    EditText search_location_box;
    LocationManager locationManager;
    String latLngString;
    ApiInterface apiService;
    List<Result> results;
    List<PlacesPOJO.CustomA> placesResult;
    List<StoreModel>storeModels;
    List<String> markerid=new ArrayList<>();
    List<String> markerTitle=new ArrayList<>();

    LatLng latLng;
    Thread thread1;
    private Dialog progressDialog;
    private double lat,lng;
    String keyword;
    List<ResultsItem> searchResult=new ArrayList<>();
    PlaceSuggestionAdapter placeSuggestionAdapter;
    RecyclerView.SmoothScroller smoothScroller;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

    }

    @Nullable

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.near_by_fragment,container,false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);

        mapFragment = (SupportMapFragment)getChildFragmentManager()
                .findFragmentById(R.id.map);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, ALL_PERMISSIONS_RESULT);
            }
            else
            {

                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(Objects.requireNonNull(getActivity()));
                getLastLocation();
                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(true);
                progressDialog.show();
                mapFragment.getMapAsync(this);
                locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                Objects.requireNonNull(locationManager).requestLocationUpdates(LocationManager.GPS_PROVIDER, 2 * 1000, 1, locationListener);
            }
        }
        else
        {
          mFusedLocationClient = LocationServices.getFusedLocationProviderClient(Objects.requireNonNull(getActivity()));
            getLastLocation();
            progressDialog=new Dialog(getContext());
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(true);
            progressDialog.show();
            mapFragment.getMapAsync(this);
            locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
            Objects.requireNonNull(locationManager).requestLocationUpdates(LocationManager.GPS_PROVIDER, 2 * 1000, 1, locationListener);
        }

        nearByPlaceRecycler.setAdapter(new NearByPlaceAdapter(getContext(), results));
        search_location_box.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @SuppressLint("MissingPermission")
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i== EditorInfo.IME_ACTION_SEARCH)
                {
                    progressDialog.setCancelable(false);
                    progressDialog.setContentView(R.layout.loadingdialog);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    
                        keyword=search_location_box.getText().toString();
                        final ApiInterface googleMapAPI = APIClient.getClient().create(ApiInterface.class);
Runnable runnable=new Runnable() {
    @Override
    public void run() {
        googleMapAPI.getSearchPlaces(keyword, Constants.KEY).enqueue(new Callback<SearchPlaceResponse>() {
            @Override
            public void onResponse(Call<SearchPlaceResponse> call, Response<SearchPlaceResponse> response) {
                if (response.code()==200)
                {
                    progressDialog.dismiss();
                    searchResult=response.body().getResults();
//                                                placeSuggestionRecycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
//                                                placeSuggestionRecycler.setVisibility(View.VISIBLE);
                    Intent intent=new Intent(getContext(),SelectedBusiness.class);
                    Bundle bundle=new Bundle();
                    intent.putExtra("key","key");
                    bundle.putSerializable("searchresult", (Serializable) searchResult);
                    intent.putExtra("bundle",bundle);
                    startActivity(intent);
                }
                else
                {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "No Places Found With this Name", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SearchPlaceResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
};
Thread thread=new Thread(runnable);
thread.start();
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, ALL_PERMISSIONS_RESULT);
            }
            else
            {
                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(Objects.requireNonNull(getActivity()));
                getLastLocation();
                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(true);
                progressDialog.show();
                mapFragment.getMapAsync(this);
                locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                Objects.requireNonNull(locationManager).requestLocationUpdates(LocationManager.GPS_PROVIDER, 2 * 1000, 1, locationListener);
            }
        }
        else
        {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(Objects.requireNonNull(getActivity()));
            getLastLocation();
            progressDialog=new Dialog(getContext());
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(true);
            progressDialog.show();
            mapFragment.getMapAsync(this);
            locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
            Objects.requireNonNull(locationManager).requestLocationUpdates(LocationManager.GPS_PROVIDER, 2 * 1000, 1, locationListener);
        }
        nearByPlaceRecycler.setAdapter(new NearByPlaceAdapter(getContext(), results));
        search_location_box.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @SuppressLint("MissingPermission")
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i== EditorInfo.IME_ACTION_SEARCH)
                {
                    progressDialog.setCancelable(false);
                    progressDialog.setContentView(R.layout.loadingdialog);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    keyword=search_location_box.getText().toString();

                    final ApiInterface googleMapAPI = APIClient.getClient().create(ApiInterface.class);
Runnable runnable=new Runnable() {
    @Override
    public void run() {
        googleMapAPI.getSearchPlaces(keyword,Constants.KEY).enqueue(new Callback<SearchPlaceResponse>() {
            @Override
            public void onResponse(Call<SearchPlaceResponse> call, Response<SearchPlaceResponse> response) {
                if (response.isSuccessful())
                {
                    progressDialog.dismiss();
                    searchResult=response.body().getResults();
                    Intent intent=new Intent(getContext(),SelectedBusiness.class);
                    Bundle bundle=new Bundle();
                    intent.putExtra("key","key");
                    bundle.putSerializable("searchresult", (Serializable) searchResult);
                    intent.putExtra("bundle",bundle);
                    startActivity(intent);
                }
                else
                {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "No Places Found With this Name", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SearchPlaceResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
};
Thread thread=new Thread(runnable);
thread.start();
                    return true;
                }
                return false;
            }
        });
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent=new Intent(getContext(), SelectedBusiness.class);
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("allResult", (Serializable) results);
                    intent.putExtra("bundle",bundle);
                    startActivity(intent);
//                }

            }
        });
    }

    private void init(View view) {
        nearByPlaceRecycler=view.findViewById(R.id.nearByPlaceRecycler);
        search_location_box =view.findViewById(R.id.search_location_box);
        viewAll=view.findViewById(R.id.viewAll);
         results=new ArrayList<>();
         sharedPreferenceData=new SharedPreferenceData(getContext());
        progressDialog=new Dialog(Objects.requireNonNull(getContext()));
        placesResult=new ArrayList<>();
        placeSuggestionRecycler=view.findViewById(R.id.placeSuggestionRecycler);
        apiService = APIClient.getClient().create(ApiInterface.class);
        horizontalLayoutManager=new SmoothScrollLinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        nearByPlaceRecycler.setLayoutManager(horizontalLayoutManager);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ALL_PERMISSIONS_RESULT) {
            int length = grantResults.length;
            if (grantResults.length > 0) {
                for (int grantResult : grantResults) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        // Granted. Start getting the location information
                    } else if (grantResult == PackageManager.PERMISSION_DENIED) {
                        {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()), R.style.AlertDialogTheme);
                            builder.setTitle("Notice");
                            builder.setMessage("You Had Denied the Necessary Permissions.Please Go to app Setting and give All the permissions");
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                                    intent.setData(Uri.parse("package:" + Objects.requireNonNull(getContext()).getPackageName()));
                                    startActivity(intent);
                                }
                            });
                            builder.show();
                            break;
                        }

                    }

                }

            }
        }
    }
    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            final int radius = 8046;
            final ApiInterface googleMapAPI = APIClient.getClient().create(ApiInterface.class);
            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    googleMapAPI.getNearBy(latLngString, radius,"Store","store",Constants.KEY).enqueue(new Callback<PlacesResults>() {
                        @Override
                        public void onResponse(Call<PlacesResults> call, Response<PlacesResults> response) {
                            if (response.isSuccessful()) {
                                progressDialog.dismiss();
                                results = response.body().getResults();
                                NearByPlaceAdapter placesListAdapter = new NearByPlaceAdapter(getContext(), results);
                                nearByPlaceRecycler.setAdapter(placesListAdapter);
                                googleMap.clear();
                                for (Result r:results)
                                {
                                    MarkerOptions markerOptions=new MarkerOptions();
                                    LatLng latLngs=new LatLng(r.getGeometry().getLocation().getLat(),r.getGeometry().getLocation().getLng());
                                    markerid.add( googleMap.addMarker(new MarkerOptions().position(latLng)).getId()) ;
                                    markerTitle.add(r.getName());
                                    markerOptions.position(latLngs);
                                    markerOptions.title(r.getName());
                                    googleMap.addMarker(markerOptions);
                                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(7));
                                }
                                CameraPosition cameraPosition = new CameraPosition.Builder().zoom(13.0f)
                                        .target(new LatLng(lat,lng)).build() ;
                                // Sets the center of the map to location user;                   // Creates a CameraPosition from the builder
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                            }
                            else {
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), "Failed", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<PlacesResults> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            };
             thread1=new Thread(runnable);
            thread1.start();

        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(final GoogleMap googleMap)
    {
        if (!checkPermissions())
        {requestPermissions();}
        else {
            this.googleMap=googleMap;
            this.googleMap.setMyLocationEnabled(true);
            this.googleMap.getUiSettings().setCompassEnabled(true);
            this.googleMap.getUiSettings().setZoomControlsEnabled(true);
            CameraPosition cameraPosition = new CameraPosition.Builder().zoom(13.0f)
                    .target(new LatLng(lat,lng)).build() ;     // Sets the center of the map to location user;                   // Creates a CameraPosition from the builder
            this.googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    if (markerid.contains(marker.getId()))
                    {
                        int index = markerid.indexOf(marker.getId());
                        String title=markerTitle.get(index);
                        nearByPlaceRecycler.getLayoutManager().smoothScrollToPosition(nearByPlaceRecycler,new RecyclerView.State(),index);
                        nearByPlaceRecycler.scrollToPosition(index);
                        marker.setTitle(title);
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(),13.0f));
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }
    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    lat=location.getLatitude();
                                    lng=location.getLongitude();
                                    latLngString = location.getLatitude() + "," + location.getLongitude();
                                    latLng = new LatLng(location.getLatitude(), location.getLongitude());
//                                    LocationAddress locationAddress = new LocationAddress();
//                                    locationAddress.getAddressFromLocation(location.getLatitude(), location.getLongitude(),
//                                            getContext(), new GeocoderHandler());
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(getContext(), "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                ALL_PERMISSIONS_RESULT
        );
    }
    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }
    private final LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            lat=mLastLocation.getLatitude();
            lng=mLastLocation.getLongitude();
            latLngString = mLastLocation.getLatitude() + "," + mLastLocation.getLongitude();
            latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        }
    };
    private boolean checkPermissions() {
        return ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }


}
