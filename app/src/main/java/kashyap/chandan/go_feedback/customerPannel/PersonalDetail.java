package kashyap.chandan.go_feedback.customerPannel;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.UpdateProfileData;
import kashyap.chandan.go_feedback.ResponseClasses.UpdateProfileResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.customerPannel.maps.APIClient;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class PersonalDetail extends AppCompatActivity implements View.OnClickListener {
CircleImageView profileImage;
TextInputEditText et_first_name,et_Last_name,et_email;
TextView btnsubmitDetail;
    private String picturePath;
    Bitmap converetdImage;
    File image=null;
    MultipartBody.Part profileImg=null;
    private Dialog cameradialog,progressDialog;
    SharedPreferenceData sharedPreferenceData;
    ImageView iv_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_detail);
        init();
        profileImage.setOnClickListener(this);
        btnsubmitDetail.setOnClickListener(this);
        et_first_name.setText(sharedPreferenceData.getFName());
        et_Last_name.setText(sharedPreferenceData.getLName());
        et_email.setText(sharedPreferenceData.getEmail());
        Picasso.get().load(ApiClient.IMAGE_URL+sharedPreferenceData.getImage()).placeholder(R.drawable.loading).error(R.drawable.terms).into(profileImage);
iv_back.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        finish();
    }
});
    }

    private  void setBtnsubmitDetail()
    {
      String  fname=et_first_name.getText().toString();
      String lname=et_Last_name.getText().toString();
      String    email=et_email.getText().toString();
      if (fname.isEmpty()&&lname.isEmpty()&&email.isEmpty())
          Toast.makeText(this, "Fill All the Fields", Toast.LENGTH_SHORT).show();
      else if (fname.isEmpty())
          Toast.makeText(this, "Enter First Name", Toast.LENGTH_SHORT).show();
      else if (lname.isEmpty())
          Toast.makeText(this, "Enter Last Name", Toast.LENGTH_SHORT).show();
      else if (email.isEmpty())
          Toast.makeText(this, "Enter Email", Toast.LENGTH_SHORT).show();
      else if (!emailValidation(email))
          Toast.makeText(this, "Enter Valid Email", Toast.LENGTH_SHORT).show();
      else
      {
          final String date=sharedPreferenceData.getDate();
          RequestBody id = RequestBody.create(MediaType.parse("multipart/form-data"),sharedPreferenceData.getId());
          RequestBody firstName = RequestBody.create(MediaType.parse("multipart/form-data"),fname);
          RequestBody lastName = RequestBody.create(MediaType.parse("multipart/form-data"),lname);
          RequestBody semail = RequestBody.create(MediaType.parse("multipart/form-data"),email);
          RequestBody sPhone = RequestBody.create(MediaType.parse("multipart/form-data"),sharedPreferenceData.getPhone());
          if (image!=null)
          {
              RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
              profileImg = MultipartBody.Part.createFormData("image", image.getName(), requestFile);
          }
          else {
              RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
              profileImg = MultipartBody.Part.createFormData("image", "", requestFile);
          }
          progressDialog=new Dialog(PersonalDetail.this);
          progressDialog.setContentView(R.layout.loadingdialog);
          progressDialog.setCancelable(false);
          progressDialog.show();
          APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
          final Call<UpdateProfileResponse>call=apiInterface.updateProfile(id,firstName,lastName,semail,sPhone,profileImg);
          Runnable runnable=new Runnable() {
              @Override
              public void run() {
                  call.enqueue(new Callback<UpdateProfileResponse>() {
                      @Override
                      public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                          if (response.code()==200)
                          {
                              progressDialog.dismiss();
                              Toast.makeText(PersonalDetail.this, "Personal Detail Updated", Toast.LENGTH_SHORT).show();
                              UpdateProfileData data=response.body().getData();
                              sharedPreferenceData.putSharedPreference(data.getId(),data.getFirstName(),data.getLastName()
                              ,data.getPhone(),data.getRole(),data.getEmail(),data.getImage(),date);
                              finish();
                          }
                          else
                          {
                              progressDialog.dismiss();
                              Converter<ResponseBody, ApiError> converter =
                                      ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                              ApiError error;
                              try {
                                  error = converter.convert(response.errorBody());
                                  ApiError.StatusBean status=error.getStatus();
                                  Toast.makeText(PersonalDetail.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                              } catch (IOException e) { e.printStackTrace(); }
                          }
                      }

                      @Override
                      public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                        progressDialog.dismiss();
                          Toast.makeText(PersonalDetail.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                      }
                  });
              }
          };
          Thread thread=new Thread(runnable);
          thread.start();
      }
    }
    private void init() {
        profileImage=findViewById(R.id.profileImage);
        et_first_name=findViewById(R.id.et_first_name);
        et_Last_name=findViewById(R.id.et_Last_name);
        iv_back=findViewById(R.id.iv_back);
        et_email=findViewById(R.id.et_email);
        btnsubmitDetail=findViewById(R.id.btnsubmitDetail);
        sharedPreferenceData=new SharedPreferenceData(PersonalDetail.this);
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void chooseProfilePic()
    {
        if (ContextCompat.checkSelfPermission(PersonalDetail.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
                ContextCompat.checkSelfPermission(PersonalDetail.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                ||ContextCompat.checkSelfPermission(PersonalDetail.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {

            askPermission();
        }
        else {

            ImageView camera, folder;
            cameradialog = new Dialog(PersonalDetail.this);
            cameradialog.setContentView(R.layout.dialogboxcamera);
            DisplayMetrics metrics=getResources().getDisplayMetrics();
            int width=metrics.widthPixels;
            cameradialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
            cameradialog.show();
            cameradialog.setCancelable(true);
            cameradialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Window window = cameradialog.getWindow();
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
            camera = cameradialog.findViewById(R.id.camera);
            folder = cameradialog.findViewById(R.id.gallery);
            folder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, 100);
                    cameradialog.dismiss();
                }
            });
            camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, 101);
                    cameradialog.dismiss();
                }
            });
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (resultCode == RESULT_OK) {

            if (requestCode == 100  && resultData != null) {

//the image URI
                Uri selectedImage = resultData.getData();

                //     imagepath=selectedImage.getPath();

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();


                if (picturePath != null && !picturePath.equals("")) {
                    image = new File(picturePath);
                }

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    converetdImage = getResizedBitmap(bitmap, 500);
                    profileImage.setImageBitmap(converetdImage);
                    profileImage.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else if (requestCode == 101 ) {
                Bitmap converetdImage = (Bitmap) resultData.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                profileImage.setImageBitmap(converetdImage);
                profileImage.setVisibility(View.VISIBLE);
                File dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                image = new File(dirPath, "ProfileImage.jpg");
                try {
                    if(!dirPath.isDirectory()) {
                        dirPath.mkdirs();
                    }
                    image.createNewFile();
                }catch (Exception e){}

                FileOutputStream fo;
                try {

                    fo = new FileOutputStream(image);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void askPermission() {
        if (ContextCompat.checkSelfPermission(PersonalDetail.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(PersonalDetail.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                ||ContextCompat.checkSelfPermission(PersonalDetail.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},100);
        }
    }
    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    public boolean emailValidation(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnsubmitDetail:
                setBtnsubmitDetail();
                break;
            case R.id.profileImage:
                chooseProfilePic();
                break;
        }
    }
}