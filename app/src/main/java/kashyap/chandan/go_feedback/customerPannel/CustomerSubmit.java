package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPendingData;
import kashyap.chandan.go_feedback.ResponseClasses.PendingDataItem;

public class CustomerSubmit extends AppCompatActivity implements View.OnClickListener {
RecyclerView submitRecycler;
Intent intent;
ImageView iv_back;
List<BusinessPendingData>pendingList;
TextView toolHeader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_submit);
        init();
        Bundle bundle=intent.getBundleExtra("bundle");
        pendingList= (List<BusinessPendingData>) bundle.getSerializable("pending");
        toolHeader.setText(intent.getStringExtra("b_name"));
        submitRecycler.setLayoutManager(new LinearLayoutManager(CustomerSubmit.this,LinearLayoutManager.VERTICAL,false));
        submitRecycler.setAdapter(new CustomerSubmitAdapter(CustomerSubmit.this,pendingList));
        iv_back.setOnClickListener(this);
    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        toolHeader=findViewById(R.id.toolHeader);
        pendingList=new ArrayList<>();
        intent=getIntent();
        submitRecycler=findViewById(R.id.submitRecycler);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}