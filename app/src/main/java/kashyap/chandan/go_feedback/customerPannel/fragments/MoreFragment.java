package kashyap.chandan.go_feedback.customerPannel.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.go_feedback.IntroSreen;
import kashyap.chandan.go_feedback.MainActivity;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.NotificationCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.RedeemStatusResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.customerPannel.BankDetail;
import kashyap.chandan.go_feedback.customerPannel.BankTransfer;
import kashyap.chandan.go_feedback.customerPannel.ChangeNumber;
import kashyap.chandan.go_feedback.customerPannel.ChangePassword;
import kashyap.chandan.go_feedback.customerPannel.ContactUs;
import kashyap.chandan.go_feedback.customerPannel.LandingScreen;
import kashyap.chandan.go_feedback.customerPannel.Notification;
import kashyap.chandan.go_feedback.customerPannel.PersonalDetail;
import kashyap.chandan.go_feedback.customerPannel.WithdrawalForm;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class MoreFragment extends Fragment implements View.OnClickListener {
    TextView btnSignOut,tvEmail, tvPhone,tvFname,tvdate,notificationCount;
    CircleImageView profileImage;
    RelativeLayout requestRedeem,editProfile,changeMobileNo,changePassword,contactUs,introScreen;
    SharedPreferenceData sharedPreferenceData;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    Dialog progressDialog;
    FrameLayout notifications;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.more_fragment,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);

        tvEmail.setText(sharedPreferenceData.getEmail());
        tvFname.setText(sharedPreferenceData.getFName()+" "+sharedPreferenceData.getLName());
        tvPhone.setText(sharedPreferenceData.getPhone());
        try {
            tvdate.setText("Active Since "+formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,sharedPreferenceData.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Picasso.get().load(ApiClient.IMAGE_URL+sharedPreferenceData.getImage()).placeholder(R.drawable.loading).error(R.drawable.terms).into(profileImage);

        btnSignOut.setOnClickListener(this);
        requestRedeem.setOnClickListener(this);
        editProfile.setOnClickListener(this);
        changeMobileNo.setOnClickListener(this);
        changePassword.setOnClickListener(this);
        contactUs.setOnClickListener(this);
        notifications.setOnClickListener(this);
        introScreen.setOnClickListener(this);
    }

    private void init(View view) {progressDialog=new Dialog(getContext());
        introScreen=view.findViewById(R.id.introScreen);
        notifications=view.findViewById(R.id.notifications);
        requestRedeem=view.findViewById(R.id.requestRedeem);
        tvdate=view.findViewById(R.id.tvdate);
        notificationCount=view.findViewById(R.id.notificationCount);
        tvFname=view.findViewById(R.id.tvFname);
        sharedPreferenceData=new SharedPreferenceData(getContext());
        tvPhone =view.findViewById(R.id.tvPhone);
        tvEmail=view.findViewById(R.id.tvEmail);
        profileImage=view.findViewById(R.id.profileImage);
        requestRedeem=view.findViewById(R.id.requestRedeem);
        editProfile=view.findViewById(R.id.editProfile);
        changeMobileNo=view.findViewById(R.id.changeMobileNo);
        changePassword=view.findViewById(R.id.changePassword);
        contactUs=view.findViewById(R.id.contactUs);
        btnSignOut=view.findViewById(R.id.btnSignOut);

    }

    @Override
    public void onResume() {
        notificationCount();
        super.onResume();
        tvEmail.setText(sharedPreferenceData.getEmail());
        tvFname.setText(sharedPreferenceData.getFName()+" "+sharedPreferenceData.getLName());
        tvPhone.setText(sharedPreferenceData.getPhone());
        try {
            tvdate.setText("Active Since "+formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,sharedPreferenceData.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Picasso.get().load(ApiClient.IMAGE_URL+sharedPreferenceData.getImage()).placeholder(R.drawable.loading).error(R.drawable.terms).into(profileImage);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.notifications:
               Intent intent=new Intent(getContext(), Notification.class);
               startActivity(intent);
                break;

            case R.id.requestRedeem:
//              redeemStatus();
                Intent redeemIntent=new Intent(getContext(), BankTransfer.class);
                startActivity(redeemIntent);
                break;
            case R.id.editProfile:
                Intent profileIntent=new Intent(getContext(), PersonalDetail.class);
                getContext().startActivity(profileIntent);
                break;
            case R.id.changeMobileNo:
                Intent changeMobileNoIntent=new Intent(getContext(), ChangeNumber.class);
                getContext().startActivity(changeMobileNoIntent);
                break;
            case R.id.changePassword:
                Intent changePasswordIntent=new Intent(getContext(), ChangePassword.class);
                getContext().startActivity(changePasswordIntent);
                break;
            case R.id.contactUs:
                Intent contactUsIntent=new Intent(getContext(), ContactUs.class);
                getContext().startActivity(contactUsIntent);
                break;
            case R.id.btnSignOut:
                Intent signOutIntent=new Intent(getContext(), LandingScreen.class);
                sharedPreferenceData.sessionEnd();
                signOutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(signOutIntent);
                break;
            case R.id.introScreen:
                Intent intro=new Intent(getContext(), IntroSreen.class);
                startActivity(intro);
                break;
        }
    }

    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                                  String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }
    private void redeemStatus()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<RedeemStatusResponse>call=apiInterface.getRedeemStatus(sharedPreferenceData.getId());
Runnable runnable=new Runnable() {
    @Override
    public void run() {
        call.enqueue(new Callback<RedeemStatusResponse>() {
            @Override
            public void onResponse(Call<RedeemStatusResponse> call, Response<RedeemStatusResponse> response) {
                if (response.code()==200)
                {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), ""+response.body().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(getContext(), BankTransfer.class);
                    startActivity(intent);
                }
                else
                {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "Add Bank Details", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(getContext(), WithdrawalForm.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<RedeemStatusResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
};
Thread thread=new Thread(runnable);
thread.start();
    }
    private void notificationCount()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<NotificationCountResponse>call=apiInterface.notificationCount(sharedPreferenceData.getId());
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<NotificationCountResponse>() {
                    @Override
                    public void onResponse(Call<NotificationCountResponse> call, Response<NotificationCountResponse> response) {
                        if (response.code()==200)
                        {
                         progressDialog.dismiss();
                            notificationCount.setText(String.valueOf(response.body().getNotificationCount()));
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<NotificationCountResponse> call, Throwable t) {
progressDialog.dismiss();
                        Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}
