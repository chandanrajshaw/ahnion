package kashyap.chandan.go_feedback.customerPannel.searchlocation;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Northeast implements Serializable {

	@SerializedName("lng")
	private double lng;

	@SerializedName("lat")
	private double lat;

	public double getLng(){
		return lng;
	}

	public double getLat(){
		return lat;
	}
}