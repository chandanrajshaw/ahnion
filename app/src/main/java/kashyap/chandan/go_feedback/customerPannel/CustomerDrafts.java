package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessDraftData;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessDraftResponse;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPendingData;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class CustomerDrafts extends AppCompatActivity {
RecyclerView DraftsAdapter;
    Intent intent;
    ImageView iv_back;
    List<BusinessDraftData> draftList;
    TextView toolHeader;
    String b_id,name;
    private Dialog progressDialog;
    SharedPreferenceData sharedPreferenceData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_drafts);
        init();
         b_id=intent.getStringExtra("b_id");
         name=intent.getStringExtra("name");
         toolHeader.setText(name);
        DraftsAdapter.setLayoutManager(new LinearLayoutManager(CustomerDrafts.this,LinearLayoutManager.VERTICAL,false));
       iv_back.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               finish();
           }
       });
    }

    @Override
    protected void onResume() {
        super.onResume();
        draft();
    }

    private void init() {
        sharedPreferenceData=new SharedPreferenceData(CustomerDrafts.this);
        DraftsAdapter=findViewById(R.id.DraftsRecycler);
        iv_back=findViewById(R.id.iv_back);
        toolHeader=findViewById(R.id.toolHeader);
        draftList=new ArrayList<>();
        intent=getIntent();
    }
    private void draft()
    {
        progressDialog=new Dialog(CustomerDrafts.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        // Toast.makeText(getContext(), ""+pendingDataItem.getCount(), Toast.LENGTH_SHORT).show();
        Call<BusinessDraftResponse> call1=apiInterface.businessDrafts(sharedPreferenceData.getId(),b_id);
        call1.enqueue(new Callback<BusinessDraftResponse>() {
            @Override
            public void onResponse(Call<BusinessDraftResponse> call, Response<BusinessDraftResponse> response) {
                if (response.code()==200) {
                    progressDialog.dismiss();
                    List<BusinessDraftData>draftDataList=response.body().getData();
                    if (draftDataList.size()==0||draftDataList==null)
                    {
                        Toast.makeText(CustomerDrafts.this, "No Data Available", Toast.LENGTH_SHORT).show();
                        DraftsAdapter.setAdapter(null);
                    }
                    else
                    {
                        DraftsAdapter.setAdapter(new CustomerDraftAdapter(CustomerDrafts.this,draftDataList));
                    }
                }
                else
                {
                    progressDialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(CustomerDrafts.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                        DraftsAdapter.setAdapter(null);
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<BusinessDraftResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(CustomerDrafts.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}