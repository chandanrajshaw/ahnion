package kashyap.chandan.go_feedback.customerPannel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.NotificationItem;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    Context context;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    List<NotificationItem> notificationItems=new ArrayList<>();
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    public NotificationAdapter(Context context, List<NotificationItem> notificationItems) {
        this.context=context;
        this.notificationItems=notificationItems;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.notification_layouts,parent,false));
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.businessName.setText((CharSequence) notificationItems.get(position).getName());
        holder.businessName.setVisibility(View.VISIBLE);
        holder.vincity.setText(notificationItems.get(position).getMessage());
        try {
            holder.date.setText(formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,notificationItems.get(position).getDatetime().substring(0,10)));
        } catch (ParseException e)
        { e.printStackTrace();}
        String status=notificationItems.get(position).getStatus();
        if (status.equalsIgnoreCase("1"))
        {
            holder.ivstatus.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_iv_tick_mark));
        }
        else
            {
            holder.ivstatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_noti_cross));
            }

    }

    @Override
    public int getItemCount() {
        return notificationItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView  businessName,vincity,date;
        ImageView ivstatus;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivstatus=itemView.findViewById(R.id.ivstatus);
            businessName=itemView.findViewById(R.id.businessName);
            vincity=itemView.findViewById(R.id.vincity);
            date=itemView.findViewById(R.id.date);
        }
    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }

}
