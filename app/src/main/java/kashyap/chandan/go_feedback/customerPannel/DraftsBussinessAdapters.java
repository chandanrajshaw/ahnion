package kashyap.chandan.go_feedback.customerPannel;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.ClickListnerAdapterInterface;
import kashyap.chandan.go_feedback.MainActivity;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.DraftDataItem;
import kashyap.chandan.go_feedback.ResponseClasses.PublishDataItem;

public class DraftsBussinessAdapters extends RecyclerView.Adapter<DraftsBussinessAdapters.MyViewHolder> {
    Context context;
    ClickListnerAdapterInterface clickListnerAdapterInterface;
    List<DraftDataItem> draftData=new ArrayList<>();
    private int mSelectedItem = -1;
    Dialog progressDialog;
    public DraftsBussinessAdapters(Context context, List<DraftDataItem> draftData, ClickListnerAdapterInterface clickListnerAdapterInterface) {
        this.context=context;
        this.draftData=draftData;
        this.clickListnerAdapterInterface=clickListnerAdapterInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.published_business_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.tvBusinnessName.setText(draftData.get(position).getName());
        holder.tvcount.setText(draftData.get(position).getCount());
//        holder.allDetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Toast.makeText(context, " Integration Work is In Progres", Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return draftData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvBusinnessName,tvcount;
        RelativeLayout allDetails;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            allDetails=itemView.findViewById(R.id.allDetails);
            tvcount=itemView.findViewById(R.id.tvcount);
            tvBusinnessName=itemView.findViewById(R.id.tvBusinnessName);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    clickListnerAdapterInterface.onClicked(v,draftData.get(mSelectedItem));
                }

            };
            allDetails.setOnClickListener(clickListener);
            itemView.setOnClickListener(clickListener);
        }
    }
}
