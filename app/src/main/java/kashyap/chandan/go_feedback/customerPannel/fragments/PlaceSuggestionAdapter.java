package kashyap.chandan.go_feedback.customerPannel.fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.go_feedback.AdapterClickListner;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.customerPannel.maps.Result;
import kashyap.chandan.go_feedback.customerPannel.searchlocation.ResultsItem;

public class PlaceSuggestionAdapter extends RecyclerView.Adapter<PlaceSuggestionAdapter.MyViewHolder> {
    Context context;
    List<ResultsItem> results;
    AdapterClickListner adapterClickListner;
    private int mSelectedItem = -1;
    public PlaceSuggestionAdapter(Context context, List<ResultsItem> results, AdapterClickListner adapterClickListner) {
        this.context=context;
        this.results=results;
        this.adapterClickListner=adapterClickListner;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.search_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
holder.placeName.setText(results.get(position).getName());
holder.placeVincity.setText(results.get(position).getFormattedAddress());
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView placeName,placeVincity;
        LinearLayout detail;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            placeVincity=itemView.findViewById(R.id.placeVincity);
            placeName=itemView.findViewById(R.id.placeName);
            detail=itemView.findViewById(R.id.detail);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    adapterClickListner.onClicked(v,results.get(mSelectedItem));
                }

            };
            detail.setOnClickListener(clickListener);

        }
    }
}
