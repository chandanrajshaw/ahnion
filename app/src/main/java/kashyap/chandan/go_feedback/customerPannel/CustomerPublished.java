package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessDraftData;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPublishedData;
import kashyap.chandan.go_feedback.customerPannel.fragments.CustomerApprovedAdapter;

public class CustomerPublished extends AppCompatActivity implements View.OnClickListener {
RecyclerView approvedRecycler;
    Intent intent;
    ImageView iv_back;
    List<BusinessPublishedData> publishList;
    TextView toolHeader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_published);
        init();
        Bundle bundle=intent.getBundleExtra("bundle");
        publishList= (List<BusinessPublishedData>) bundle.getSerializable("published");
        toolHeader.setText(intent.getStringExtra("b_name"));
        approvedRecycler.setLayoutManager(new LinearLayoutManager(CustomerPublished.this,LinearLayoutManager.VERTICAL,false));
        approvedRecycler.setAdapter(new CustomerApprovedAdapter(CustomerPublished.this,publishList));
        iv_back.setOnClickListener(this);
    }

    private void init() {
        approvedRecycler=findViewById(R.id.approvedRecycler);
        iv_back=findViewById(R.id.iv_back);
        toolHeader=findViewById(R.id.toolHeader);
        publishList=new ArrayList<>();
        intent=getIntent();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}