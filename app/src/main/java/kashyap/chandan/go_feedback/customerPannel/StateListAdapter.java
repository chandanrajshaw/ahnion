package kashyap.chandan.go_feedback.customerPannel;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.go_feedback.CustomItemClickListener;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.StateData;

public class StateListAdapter extends RecyclerView.Adapter<StateListAdapter.MyViewHolder> {
    Context context;
    List<StateData> stateData;
    Dialog stateDialog;
    CustomItemClickListener customItemClickListener;
    private int mSelectedItem = -1;
    public StateListAdapter(Context context, List<StateData> stateData, Dialog stateDialog, CustomItemClickListener customItemClickListener) {
        this.context=context;
        this.stateData=stateData;
        this.stateDialog=stateDialog;
        this.customItemClickListener=customItemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.option,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == mSelectedItem);
        holder.tvstate.setText(stateData.get(position).getStateName());
        TextView ok= stateDialog.findViewById(R.id.ok);
        TextView dialogheader=stateDialog.findViewById(R.id.dialogheader);
        dialogheader.setText("State");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedItem==-1)
                { Toast.makeText(context, "Select State", Toast.LENGTH_SHORT).show(); }
                else
                { stateDialog.dismiss(); }
            }
        });
    }

    @Override
    public int getItemCount() {
        return stateData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvstate;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvstate=itemView.findViewById(R.id.item);
            radioselect=itemView.findViewById(R.id.selected);
            View.OnClickListener clickListener=new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    customItemClickListener.onItemClick(view,stateData.get(mSelectedItem).getId(),stateData.get(mSelectedItem).getStateName());
                }
            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}
