package kashyap.chandan.go_feedback.customerPannel;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.go_feedback.ClickListnerAdapterInterface;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.PurchaseDataItem;

public class PurchasedBussinessAdapters extends RecyclerView.Adapter<PurchasedBussinessAdapters.MyViewHolder> {
    Context context; private int mSelectedItem = -1;
    List<PurchaseDataItem> purchaseData;
    ClickListnerAdapterInterface clickListnerAdapterInterface;
    public PurchasedBussinessAdapters(Context context, List<PurchaseDataItem> purchaseData, ClickListnerAdapterInterface clickListnerAdapterInterface) {
        this.context=context;
        this.clickListnerAdapterInterface=clickListnerAdapterInterface;
        this.purchaseData=purchaseData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.published_business_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvBusinnessName.setText(purchaseData.get(position).getName());
        holder.tvcount.setText(purchaseData.get(position).getCount());
//        holder.allDetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(context, " Integration Work is In Progres", Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return purchaseData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvBusinnessName,tvcount;
        RelativeLayout allDetails;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            allDetails=itemView.findViewById(R.id.allDetails);
            tvcount=itemView.findViewById(R.id.tvcount);
            tvBusinnessName=itemView.findViewById(R.id.tvBusinnessName);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    clickListnerAdapterInterface.onClicked(v,purchaseData.get(mSelectedItem));
                }

            };
            allDetails.setOnClickListener(clickListener);
        }
    }
}
