package kashyap.chandan.go_feedback.customerPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kashyap.chandan.go_feedback.AdminPannel.AdminPending;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PendingListDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.ReasonResponse;
import kashyap.chandan.go_feedback.Constants;
import kashyap.chandan.go_feedback.ImageAdapter;
import kashyap.chandan.go_feedback.ImageAdaptercross;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ReceiptImageAdapter;
import kashyap.chandan.go_feedback.ResponseClasses.TakeActionResponse;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ReviewDetail extends AppCompatActivity implements View.OnClickListener {
    List<String> images=new ArrayList<>();
    List<String> receipts=new ArrayList<>();
    TextView customer_review;
    TextView btnapprove,btnreject, googlerating, tvAddress, tvname;
    RecyclerView imagelist, receiptRecycler;
    Intent intent;
    ImageView image,iv_back;
    CheckBox reason1,reason2,reason3,reason4;
    List<String>reason=new ArrayList<>();
    RatingBar servicerating, ambiancerating, staffrating, ratingbar;
    private SharedPreferenceData sharedPreferenceData;
    Dialog progressDialog;
    PendingListDataItem dataItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_detail);
init();
iv_back.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        finish();
    }
});

    }

    @Override
    protected void onResume() {
        super.onResume();
        Bundle bundle=intent.getBundleExtra("bundle");
         dataItem= (PendingListDataItem) bundle.getSerializable("pending");
        if (dataItem.getRecipt().isEmpty())
        { receiptRecycler.setVisibility(View.GONE);}
        else
        {
            receipts= Arrays.asList(dataItem.getRecipt().split(","));
            receiptRecycler.setAdapter(new ImageAdaptercross(ReviewDetail.this,receipts));
            receiptRecycler.setVisibility(View.VISIBLE);
        }

        if (dataItem.getImagees().isEmpty())
        {
            imagelist.setVisibility(View.GONE);
        }
        else
        {
            images= Arrays.asList(dataItem.getImagees().split(","));
            imagelist.setAdapter(new ImageAdaptercross(ReviewDetail.this, images));
            imagelist.setVisibility(View.VISIBLE);
        }
        btnapprove.setOnClickListener(this);
        btnreject.setOnClickListener(this);
        googlerating.setText(String.valueOf(dataItem.getRating()));
        tvname.setText(dataItem.getName());
        tvAddress.setText(dataItem.getVicinity());
        staffrating.setRating(Float.parseFloat(dataItem.getAttitudeOfStass()));
        ambiancerating.setRating(Float.parseFloat(dataItem.getAmbience()));
        ratingbar.setRating(Float.parseFloat(dataItem.getRating()));
        servicerating.setRating(Float.parseFloat(dataItem.getService()));
        String ref=String.valueOf(dataItem.getReferanceImage());
        System.out.println(ref);
        if (ref==null||ref.isEmpty())
            image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.nopreviewavailable));
        else
        {
            Picasso.get().load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+ref+"&key="+ Constants.KEY).into(image);

        }

        customer_review.setText(dataItem.getCustomerReview());
    }

    private void init() {
progressDialog=new Dialog(ReviewDetail.this);
        iv_back=findViewById(R.id.iv_back);
        customer_review=findViewById(R.id.customer_review);
        receiptRecycler = findViewById(R.id.receiptRecycler);
        image = findViewById(R.id.image);
        intent = getIntent();
        btnapprove=findViewById(R.id.btnapprove);
        sharedPreferenceData=new SharedPreferenceData(ReviewDetail.this);
        tvAddress = findViewById(R.id.tvAddress);
        googlerating = findViewById(R.id.googlerating);
        btnreject = findViewById(R.id.btnreject);
        imagelist = findViewById(R.id.feedbackpics);
        tvname = findViewById(R.id.tvname);
        servicerating = findViewById(R.id.servicerating);
        ambiancerating = findViewById(R.id.ambiancerating);
        staffrating = findViewById(R.id.staffrating);
        ratingbar = findViewById(R.id.ratingbar);
        receiptRecycler.setLayoutManager(new LinearLayoutManager(ReviewDetail.this, LinearLayoutManager.HORIZONTAL, false));
        imagelist.setLayoutManager(new LinearLayoutManager(ReviewDetail.this, LinearLayoutManager.HORIZONTAL, false));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnapprove:
                approve();
                break;
            case R.id.btnreject:
reject();
                break;
        }
    }
    private void reject()
    {
        final Dialog rejectDialog=new Dialog(ReviewDetail.this);
        rejectDialog.setContentView(R.layout.rejection_dialog);
        rejectDialog.setCancelable(false);
        DisplayMetrics metrics=getApplicationContext().getResources().getDisplayMetrics();
        int width=metrics.widthPixels;
        rejectDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
        rejectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = rejectDialog.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        ImageView close=rejectDialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rejectDialog.dismiss();
            }
        });
        reason1=rejectDialog.findViewById(R.id.reason1);
        reason2=rejectDialog.findViewById(R.id.reason2);
        reason3=rejectDialog.findViewById(R.id.reason3);
        reason4=rejectDialog.findViewById(R.id.reason4);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);;
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<ReasonResponse>call=apiInterface.Reason();
        call.enqueue(new Callback<ReasonResponse>() {
            @Override
            public void onResponse(Call<ReasonResponse> call, Response<ReasonResponse> response) {
                if (response.code()==200)
                {
                    progressDialog.dismiss();
                    reason1.setText(response.body().getData().getReason1());
                    reason2.setText(response.body().getData().getReason2());
                    reason3.setText(response.body().getData().getReason3());
                    reason4.setText(response.body().getData().getReason4());
                    rejectDialog.show();
                }
                else
                {
                    progressDialog.dismiss();
                    Toast.makeText(ReviewDetail.this, "No Rejection Reason", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ReasonResponse> call, Throwable t) {

            }
        });
        final List<CheckBox> checkBoxes=new ArrayList<>();
        checkBoxes.add(reason1);
        checkBoxes.add(reason2);
        checkBoxes.add(reason3);
        checkBoxes.add(reason4);
        final TextView businessName=rejectDialog.findViewById(R.id.businessName);
        final TextView vincity=rejectDialog.findViewById(R.id.vincity);
        TextView rating=rejectDialog.findViewById(R.id.rating);
        TextView btnreject=rejectDialog.findViewById(R.id.btnreject);
        TextView id=rejectDialog.findViewById(R.id.id);
        final TextView comment=rejectDialog.findViewById(R.id.comment);
        final EditText customer_review=rejectDialog.findViewById(R.id.customer_review);
        businessName.setText(dataItem.getName());
        vincity.setText(dataItem.getVicinity());
        comment.setText(dataItem.getCustomerReview());
        rating.setText(dataItem.getRating());
        id.setText(dataItem.getId());

        btnreject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (CheckBox cb:checkBoxes)
                {
                    if (cb.isChecked())
                        reason.add(cb.getText().toString());
                }
                String etReason=customer_review.getText().toString();
                if (etReason.isEmpty()&&reason.isEmpty())
                    Toast.makeText(ReviewDetail.this, "Give Rejection Reason", Toast.LENGTH_SHORT).show();
                else
                {
                    progressDialog.setContentView(R.layout.loadingdialog);
                    progressDialog.setCancelable(false);;
                    progressDialog.show();
                    APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                    List<String>reason=new ArrayList<>();
                    Call<TakeActionResponse>call=apiInterface.takeAction(dataItem.getId(),"0",getDate(),etReason,reason);
                    call.enqueue(new Callback<TakeActionResponse>() {
                        @Override
                        public void onResponse(Call<TakeActionResponse> call, Response<TakeActionResponse> response) {
                            if (response.code()==200)
                            {
                                progressDialog.dismiss();
                                rejectDialog.dismiss();
                                Toast.makeText(ReviewDetail.this, "Rejected", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else
                            {
                                progressDialog.dismiss();
                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                    Toast.makeText(ReviewDetail.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }
                            }
                        }

                        @Override
                        public void onFailure(Call<TakeActionResponse> call, Throwable t) {

                        }
                    });
                }
            }
        });
    }
    private void approve()
    {

        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);;
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<TakeActionResponse> call=apiInterface.takeAction(dataItem.getId(),"1",getDate(),"",reason);
        call.enqueue(new Callback<TakeActionResponse>() {
            @Override
            public void onResponse(Call<TakeActionResponse> call, Response<TakeActionResponse> response) {
                if (response.code()==200)
                {
                    progressDialog.dismiss();
//                    Intent intent=new Intent( ReviewDetail.this, AdminPending.class);
//                 startActivity(intent);
finish();
                    Toast.makeText(ReviewDetail.this, "Approved", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    progressDialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(ReviewDetail.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<TakeActionResponse> call, Throwable t) {

            }
        });
    }
    private  String getDate()
    {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        return formattedDate;
    }
    private void getRejectionReason()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);;
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<ReasonResponse>call=apiInterface.Reason();
        call.enqueue(new Callback<ReasonResponse>() {
            @Override
            public void onResponse(Call<ReasonResponse> call, Response<ReasonResponse> response) {
                if (response.code()==200)
                {
                    progressDialog.dismiss();
                    reason1.setText(response.body().getData().getReason1());
                    reason2.setText(response.body().getData().getReason2());
                    reason3.setText(response.body().getData().getReason3());
                    reason4.setText(response.body().getData().getReason4());
                }
                else
                {
                    progressDialog.dismiss();
                    Toast.makeText(ReviewDetail.this, "No Rejection Reason", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ReasonResponse> call, Throwable t) {

            }
        });
    }
}