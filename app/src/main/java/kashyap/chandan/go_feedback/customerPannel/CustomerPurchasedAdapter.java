package kashyap.chandan.go_feedback.customerPannel;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import kashyap.chandan.go_feedback.DetailData;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPurchaseData;
import kashyap.chandan.go_feedback.ViewDetailResponse;
import kashyap.chandan.go_feedback.ViewReviewDetail;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class CustomerPurchasedAdapter extends RecyclerView.Adapter<CustomerPurchasedAdapter.MyViewHolder> {
    Context context;
    List<BusinessPurchaseData> purchaseDataList;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    private Dialog dialog;

    public CustomerPurchasedAdapter(Context context, List<BusinessPurchaseData> purchaseDataList) {
        this.context=context;
        this.purchaseDataList=purchaseDataList;
        dialog=new Dialog(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.purchased_feedback_layouts,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
holder.businessName.setText(purchaseDataList.get(position).getName());
holder.comment.setText(purchaseDataList.get(position).getCustomerReview());
holder.id.setText("GDS"+purchaseDataList.get(position).getFeedbackId());
holder.purchaseAmt.setText("$"+purchaseDataList.get(position).getAmount());
holder.rating.setText(purchaseDataList.get(position).getRating());
holder.vincity.setText(purchaseDataList.get(position).getVicinity());
        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.setContentView(R.layout.loadingdialog);
                dialog.setCancelable(false);
                Activity activity=(Activity)context;
                if (!activity.isFinishing()) {
                    dialog.show();
                }
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                Call<ViewDetailResponse> call=apiInterface.getDetail(purchaseDataList.get(position).getFeedbackId());
                call.enqueue(new Callback<ViewDetailResponse>() {
                    @Override
                    public void onResponse(Call<ViewDetailResponse> call, Response<ViewDetailResponse> response) {
                        if (response.code()==200)
                        {
                            dialog.dismiss();
                            DetailData detailData=response.body().getData();
                            Bundle bundle=new Bundle();
                            bundle.putSerializable("data",detailData);
                            Intent intent=new Intent(context, ViewReviewDetail.class);
                            intent.putExtra("bundle",bundle);
                            context.startActivity(intent);
                        }
                        else
                        {
                            dialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<ViewDetailResponse> call, Throwable t) {

                    }
                });
            }
        });
//holder.date.setText(purchaseDataList.get(position).getDatetime());
        try {
            holder.date.setText(formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,purchaseDataList.get(position).getDatetime().substring(0,10)));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return purchaseDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView businessName,vincity,comment,date,id,purchaseAmt,rating;
        LinearLayout detail;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            detail=itemView.findViewById(R.id.detail);
            businessName=itemView.findViewById(R.id.businessName);
            vincity=itemView.findViewById(R.id.vincity);
            comment=itemView.findViewById(R.id.comment);
            date=itemView.findViewById(R.id.date);
            id=itemView.findViewById(R.id.id);
            purchaseAmt=itemView.findViewById(R.id.purchaseAmt);
            rating=itemView.findViewById(R.id.rating);
        }
    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }
}
