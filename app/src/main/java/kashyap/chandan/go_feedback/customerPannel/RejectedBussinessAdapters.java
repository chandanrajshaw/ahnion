package kashyap.chandan.go_feedback.customerPannel;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.go_feedback.ClickListnerAdapterInterface;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.RejectedDataItem;

public class RejectedBussinessAdapters extends RecyclerView.Adapter<RejectedBussinessAdapters.MyViewHolder> {
    Context context;
    private int mSelectedItem = -1;
    List<RejectedDataItem> rejectedData;
    ClickListnerAdapterInterface clickListnerAdapterInterface;
    public RejectedBussinessAdapters(Context context, List<RejectedDataItem> rejectedData, ClickListnerAdapterInterface clickListnerAdapterInterface) {
        this.context=context;
        this.rejectedData=rejectedData;
        this.clickListnerAdapterInterface=clickListnerAdapterInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.published_business_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvBusinnessName.setText(rejectedData.get(position).getName());
        holder.tvcount.setText(rejectedData.get(position).getCount());
//        holder.allDetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(context, " Integration Work is In Progres", Toast.LENGTH_SHORT).show();
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return rejectedData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvBusinnessName,tvcount;
        RelativeLayout allDetails;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            allDetails=itemView.findViewById(R.id.allDetails);
            tvcount=itemView.findViewById(R.id.tvcount);
            tvBusinnessName=itemView.findViewById(R.id.tvBusinnessName);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    clickListnerAdapterInterface.onClicked(v,rejectedData.get(mSelectedItem));
                }

            };
            allDetails.setOnClickListener(clickListener);
        }
    }
}
