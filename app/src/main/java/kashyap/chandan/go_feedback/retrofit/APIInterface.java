package kashyap.chandan.go_feedback.retrofit;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses.AdminDashboardResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses.TodayApproveResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses.TodayPendingResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses.TodayPurchasedResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses.TodayRedeemResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses.TodayRejectedResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminAllBalanceResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPaidResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPendingListResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPendingResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPublishListResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPublishedResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPurchaseListResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminRedeemResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminRejectListResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminRejectedResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.ApproveAllResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.ApproveRedeemResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.BusinessPaidCountResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.BusinessPendingCountResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.BusinessPublishCountResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.BusinessRejectedCountResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.ReasonResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.AddToCartResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.AddWishListResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.CartListResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.CartTotalResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.EnterprizeAllResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.EnterprizeBusinessWiseFeedbckListResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.MyPurchaseListResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.MyWishListResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.PriceListResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.PurchaseResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.countresponses.EnterPrizePurchaseCountResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.countresponses.RemoveWishListResponse;
import kashyap.chandan.go_feedback.ResponseClasses.AddBankResponse;
import kashyap.chandan.go_feedback.ResponseClasses.AddBusinessResponse;
import kashyap.chandan.go_feedback.ResponseClasses.AllNotificationResponse;
import kashyap.chandan.go_feedback.ResponseClasses.AllRedemptionResponse;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessDraftResponse;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPendingResponse;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPublishedResponse;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessPurchaseResponse;
import kashyap.chandan.go_feedback.ResponseClasses.BusinessRejectedResponse;
import kashyap.chandan.go_feedback.ResponseClasses.ChangePasswordResponse;
import kashyap.chandan.go_feedback.ResponseClasses.ContactUsResponse;
import kashyap.chandan.go_feedback.ResponseClasses.CustomerBalanceResponse;
import kashyap.chandan.go_feedback.ResponseClasses.DeleteCartResponse;
import kashyap.chandan.go_feedback.ResponseClasses.DeleteImageResponse;
import kashyap.chandan.go_feedback.ResponseClasses.DraftCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.EditFeedbackResponse;
import kashyap.chandan.go_feedback.ResponseClasses.FinalRedeemResponse;
import kashyap.chandan.go_feedback.ResponseClasses.ForgetPasswordResponse;
import kashyap.chandan.go_feedback.ResponseClasses.LoginResponse;
import kashyap.chandan.go_feedback.ResponseClasses.MyReviewCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.NotificationCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.OtpResponse;
import kashyap.chandan.go_feedback.ResponseClasses.PasswordOtpResponse;
import kashyap.chandan.go_feedback.ResponseClasses.PendingCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.PublishCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.PurchaseCartListResponse;
import kashyap.chandan.go_feedback.ResponseClasses.PurchaseCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.ReceiptDeleteResponse;
import kashyap.chandan.go_feedback.ResponseClasses.RedeemStatusResponse;
import kashyap.chandan.go_feedback.ResponseClasses.RegistrationResponse;
import kashyap.chandan.go_feedback.ResponseClasses.RejectedCountResponse;
import kashyap.chandan.go_feedback.ResponseClasses.ResetPasswordResponse;
import kashyap.chandan.go_feedback.ResponseClasses.SendDraftResponse;
import kashyap.chandan.go_feedback.ResponseClasses.SendFeedbackResponse;
import kashyap.chandan.go_feedback.ResponseClasses.StateListResponse;
import kashyap.chandan.go_feedback.ResponseClasses.TakeActionResponse;
import kashyap.chandan.go_feedback.ResponseClasses.TotalIndividualEarningResponse;
import kashyap.chandan.go_feedback.ResponseClasses.UpdateProfileResponse;
import kashyap.chandan.go_feedback.ViewDetailResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIInterface {
    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("loginservices/login")
    Call<LoginResponse> login(@Field("email_phone") String email_phone,
                              @Field("password") String password);

    @Multipart
    @Headers("x-api-key:feedback@123")
    @POST("loginservices/registeration")
    Call<RegistrationResponse> register(@Part("first_name") RequestBody first_name,
                                        @Part("last_name") RequestBody last_name,
                                        @Part("email") RequestBody email,
                                        @Part("phone") RequestBody phone,
                                        @Part("pass") RequestBody pass,
                                        @Part MultipartBody.Part profileImage);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("loginservices/activation")
    Call<OtpResponse> authentication(@Field("id") String id,
                                     @Field("otp") String otp);
    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("forgetpasswordservices/forget_password")
    Call<ForgetPasswordResponse> forgetPassword(@Field("f_phone_email") String f_phone_email);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("forgetpasswordservices/validate_otp")
    Call<PasswordOtpResponse> validateOtp(@Field("user_id") String user_id,
                                          @Field("otp") String otp);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("forgetpasswordservices/reset_password")
    Call<ResetPasswordResponse> resetPassword(@Field("user_id") String user_id,
                                              @Field("npass") String npass);
    @Multipart
    @Headers("x-api-key:feedback@123")
    @POST("adminservice/profile_update")
    Call<UpdateProfileResponse> updateProfile(@Part("user_id") RequestBody user_id,
                                              @Part("first_name") RequestBody first_name,
                                              @Part("last_name") RequestBody last_name,
                                              @Part("email") RequestBody email,
                                              @Part("phone") RequestBody phone,
                                              @Part MultipartBody.Part image);
    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("profileservices/change_password")
    Call<ChangePasswordResponse> changePassword(@Field("user_id") String user_id,
                                                @Field("opwd") String opwd,
                                                @Field("npwd") String npwd);

/*---------------------------------------Customer Services-----------------------------*/
    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/storebusiness")
    Call<AddBusinessResponse> registerBusiness(@Field("place_id") String place_id,
                                                @Field("name") String name,
                                                @Field("latitude") String latitude,
                                                  @Field("longitude") String longitude,
                                                  @Field("reference") String reference,
                                                  @Field("scope") String scope,
                                                  @Field("vicinity") String vicinity,
                                                  @Field("rating") String rating,
                                               @Field(" referance_image") String  referance_image
    );

    @Multipart
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/storereview")
    Call<SendFeedbackResponse>sendFeedback(@Part("b_id") RequestBody b_id,
                                           @Part("user_id") RequestBody user_id,
                                           @Part("rating") RequestBody rating,
                                           @Part("attitude_of_stass") RequestBody attitude_of_stass,
                                           @Part("ambience") RequestBody ambience,
                                           @Part("service") RequestBody service,
                                           @Part("customer_review") RequestBody customer_review,
                                           @Part List<MultipartBody.Part> receipt,
                                           @Part List<MultipartBody.Part> images);

    @Multipart
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/edit_draft")
    Call<EditFeedbackResponse>editFeedback(  @Part("status") RequestBody status,
                                             @Part("feedback_id") RequestBody feedback_id,
                                            @Part("b_id") RequestBody b_id,
                                           @Part("user_id") RequestBody user_id,
                                           @Part("rating") RequestBody rating,
                                           @Part("attitude_of_stass") RequestBody attitude_of_stass,
                                           @Part("ambience") RequestBody ambience,
                                           @Part("service") RequestBody service,
                                           @Part("customer_review") RequestBody customer_review,
                                           @Part List<MultipartBody.Part> receipt,
                                           @Part List<MultipartBody.Part> images);



    @Multipart
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/storerdraft")
    Call<SendDraftResponse>sendDraft(@Part("b_id") RequestBody b_id,
                                     @Part("user_id") RequestBody user_id,
                                     @Part("rating") RequestBody rating,
                                     @Part("attitude_of_stass") RequestBody attitude_of_stass,
                                     @Part("ambience") RequestBody ambience,
                                     @Part("service") RequestBody service,
                                     @Part("customer_review") RequestBody customer_review,
                                     @Part List<MultipartBody.Part> receipt,
                                     @Part List<MultipartBody.Part> images);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/customer_published_review")
    Call<PublishCountResponse> getpublishCount(@Field("user_id") String user_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/contact_us")
    Call<ContactUsResponse>putContactUs(@Field("name") String name,
                                         @Field("email") String email,
                                         @Field("message") String message);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/my_redeemlist")
    Call<AllRedemptionResponse> getRedeemList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/customer_draft_review")
    Call<DraftCountResponse> getDraftCount(@Field("user_id") String user_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/customer_pending_review")
    Call<PendingCountResponse> getPendingCount(@Field("user_id") String user_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/customer_purchess_review")
    Call<PurchaseCountResponse> getPurchaseCount(@Field("user_id") String user_id);


    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/customer_rejected_review")
    Call<RejectedCountResponse> getRejectedCount(@Field("user_id") String user_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/customer_my_earn")
    Call<CustomerBalanceResponse> getCustomerBalance(@Field("user_id") String user_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/business_wise_earn_of_customer ")
    Call<TotalIndividualEarningResponse> getBusinessEarning(@Field("customer_id") String customer_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/redeem")
    Call<RedeemStatusResponse> getRedeemStatus(@Field("customer_id") String user_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/store_bank_detail")
    Call<AddBankResponse> addBank(@Field("user_id") String user_id,
                                  @Field("first_name") String first_name,
                                  @Field("last_name") String last_name,
                                  @Field("adress1") String adress1,
                                  @Field("adress2") String adress2,
                                  @Field("state_id") String state_id,
                                  @Field("city_name") String city_name,
                                  @Field("zip_code") String zip_code,
                                  @Field("phone_no") String phone_no,
                                  @Field("bank_name") String bank_name,
                                  @Field("bank_adress") String bank_adress,
                                  @Field("bank_state_id") String bank_state_id,
                                  @Field("bank_city") String bank_city,
                                  @Field("bank_zip_code") String bank_zip_code,
                                  @Field("bank_routing") String bank_routing,
                                  @Field("customer_account_no") String customer_account_no,
                                  @Field("ifsc_code") String ifsc_code
    );


    @Headers("x-api-key:feedback@123")
    @GET("customerservice/state_list")
    Call<StateListResponse> stateList();

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/store_redeem_request")
    Call<FinalRedeemResponse>redeemed(
            @Field("user_id") String user_id,
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("adress1") String adress1,
            @Field("adress2") String adress2,
            @Field("state_id") String state_id,
            @Field("city_name") String city_name,
            @Field("zip_code") String zip_code,
            @Field("bank_name") String bank_name,
            @Field("bank_routing") String bank_routing,
            @Field("customer_account_no") String customer_account_no,
            @Field("amount") String amount
            );

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/feedback_recipt_delete")
    Call<ReceiptDeleteResponse>receiptDelete(@Field("feedback_id") String user_id,
                                             @Field("recipt_index") String amount
    );

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/business_wise_draft_feedback_list")
    Call<BusinessDraftResponse>businessDrafts(@Field("user_id") String user_id,
                                              @Field("b_id") String b_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/business_wise_published_feedback_list")
    Call<BusinessPublishedResponse>businessPublished(@Field("user_id") String user_id,
                                                     @Field("b_id") String b_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/business_wise_pending_feedback_list")
    Call<BusinessPendingResponse>businessPending(@Field("user_id") String user_id,
                                                 @Field("b_id") String b_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/business_wise_rejected_feedback_list")
    Call<BusinessRejectedResponse>businessRejected(@Field("user_id") String user_id,
                                                   @Field("b_id") String b_id);


    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/business_wise_purchess_feedback_list")
    Call<BusinessPurchaseResponse>businessPurchased(@Field("user_id") String user_id,
                                                    @Field("b_id") String b_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/new_notification_count")
    Call<NotificationCountResponse>notificationCount(@Field("user_id") String user_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/notification")
    Call<AllNotificationResponse>getAllNotifications(@Field("user_id") String user_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("customerservice/myreview_count")
    Call<MyReviewCountResponse>myReviewCount(@Field("user_id") String user_id);
/*------------------------------------Admin-----------------------------*/
@Headers("x-api-key:feedback@123")
@GET("adminservice/pending_reviews")
Call<AdminPendingResponse> getAdminPending();

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/approved_reviews")
    Call<AdminPublishedResponse> getAdminPublished();

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/Rejected_reviews")
    Call<AdminRejectedResponse> getAdminRejected();

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/purchesed_reviews")
    Call<AdminPaidResponse> getAdminPaid();

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/redeemrequest")
    Call<AdminRedeemResponse> getAdminRedeem();

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("adminservice/takeaction")
    Call<TakeActionResponse>takeAction(@Field("feedback_id") String feedback_id,
                                       @Field("status") String status,
                                       @Field("date") String date,
                                       @Field("rejection_resion") String rejection_resion,
                                       @Field("reasonopt[]") List<String> resion );

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("adminservice/aproved_redeem_request")
    Call<ApproveRedeemResponse>approveRedeem(@Field("redeem_request_id") String redeem_request_id);

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/business_wise_pending_review_count")
    Call<BusinessPendingCountResponse> pendingCount();

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/business_wise_paid_review_count")
    Call<BusinessPaidCountResponse> paidCount();

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/business_wise_rejected_review_count")
    Call<BusinessRejectedCountResponse> rejectCount();

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/business_wise_published_review_count")
    Call<BusinessPublishCountResponse> publishCount();

    /*----------------------Enterprize------------------*/
    @Headers("x-api-key:feedback@123")
    @GET("enterpriseservice/feedback")
    Call<EnterprizeAllResponse> getFeedback();

    @Headers("x-api-key:feedback@123")
    @GET("Enterpriseservice/prise_list")
    Call<PriceListResponse> getPrice();


    @Headers("x-api-key:feedback@123")
    @GET("adminservice/approved_all")
    Call<ApproveAllResponse> approveAll();

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/dashboard")
    Call<AdminDashboardResponse> adminDashBoard();


    @Headers("x-api-key:feedback@123")
    @GET("adminservice/today_pending_reviews")
    Call<TodayPendingResponse> todayPending();

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/today_redeem_request")
    Call<TodayRedeemResponse> todayRedeem();

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/today_approved_reviews")
    Call<TodayApproveResponse> todayApprove();

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/today_purchesed_reviews")
    Call<TodayPurchasedResponse> todayPurchase();

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/today_rejected_reviews")
    Call<TodayRejectedResponse> todayRejected();

    @Headers("x-api-key:feedback@123")
    @GET("adminservice/admin_all_balance")
    Call<AdminAllBalanceResponse> adminWallet();


    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("enterpriseservice/purchess")
    Call<PurchaseResponse>requestPurchase(@Field("feedback_id") String feedback_id,
                                          @Field("p_type") String p_type,
                                          @Field("price1") String price1,
                                          @Field("price2") String price2,
                                          @Field("price3") String price3,
                                          @Field("enterprise_id") String enterprise_id,
                                          @Field("purchess_from") String purchess_from );

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("enterpriseservice/business_wise_enterprise_purchess_list")
    Call<MyPurchaseListResponse>myPurchase(@Field("enterprise_id") String enterprise_id,
                                           @Field("b_id") String b_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("enterpriseservice/my_cartlist")
    Call<CartListResponse>myCartList(@Field("enterprise_id") String enterprise_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("enterpriseservice/my_wishlist")
    Call<MyWishListResponse>myWishList(@Field("enterprise_id") String enterprise_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("enterpriseservice/cart_count_and_sumofprice")
    Call<CartTotalResponse>cartPrice(@Field("enterprise_id") String enterprise_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("enterpriseservice/add_to_wishlist")
    Call<AddWishListResponse>addToWishList(@Field("enterprise_id") String enterprise_id,
                                           @Field("feedback_id") String feedback_id);


    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("enterpriseservice/add_to_cart")
    Call<AddToCartResponse>addTocart(@Field("feedback_id") String feedback_id,
                                     @Field("enterprise_id") String enterprise_id,
                                     @Field("p_type") String p_type);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("adminservice/feedback_viewdetail")
    Call<ViewDetailResponse>getDetail(@Field("feedback_id") String feedback_id);

//DetailData,ViewDetailResponse
    @Headers("x-api-key:feedback@123")
    @GET("Adminservice/rejection_reason_option_list")
    Call<ReasonResponse> Reason();


    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("Customerservice/feedback_imge_delete")
    Call<DeleteImageResponse>deleteImage(@Field("feedback_id") String feedback_id,
                                         @Field("image_index") String image_index);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("Adminservice/business_wise_purchesed_review_list")
    Call<AdminPurchaseListResponse>adminPaid(@Field("business_id") String business_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("Adminservice/business_wise_rejected_review_list")
    Call<AdminRejectListResponse>adminReject(@Field("business_id") String business_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("Adminservice/business_wise_pending_review_list")
    Call<AdminPendingListResponse>adminPending(@Field("business_id") String business_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("Adminservice/business_wise_published_review_list")
    Call<AdminPublishListResponse>adminPublish(@Field("business_id") String business_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("enterpriseservice/business_wise_published_review_for_enterprise")
    Call<EnterprizeBusinessWiseFeedbckListResponse>EnterprizePublish(@Field("business_id") String business_id, @Field("enterprise_id") String enterprise_id);
//EnterprizeBusinessWiseFeedbckListResponse,EnterpriseFeedbackDataItem

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("enterpriseservice/business_wise_enterprise_purchess_with_count")
    Call<EnterPrizePurchaseCountResponse>purchasecount(@Field("enterprise_id") String enterprise_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("enterpriseservice/delete_checkout")
    Call<DeleteCartResponse>deleteCart(@Field("cart_id") String cart_id);


    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("enterpriseservice/remove_from_wishlist")
    Call<RemoveWishListResponse>removeWishlist(@Field("enterprise_id") String enterprise_id,
                                               @Field("feedback_id") String feedback_id);

    @FormUrlEncoded
    @Headers("x-api-key:feedback@123")
    @POST("enterpriseservice/checkout")
    Call<PurchaseCartListResponse>purchaseCartList(@Field("cart_id_list[]") List<String> cart_id_list);
}
