package kashyap.chandan.go_feedback.AdminPannel;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.BusinessPaidCountResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.BusinessRejectedCountResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PaidCountDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PendingCountDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.RejectedCountDataItem;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class BusinessRejected extends AppCompatActivity {
    TextView toolHeader;
    RecyclerView pendingRecycler;
    ImageView iv_back;
    Dialog progressDialog;
    List<RejectedCountDataItem> rejectedCountDataItem;
    List<RejectedCountDataItem> myList;
    AdminRejectedAdapter rejectedAdapter;
    EditText search_location_box;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_pending);
      init();
        pendingRecycler.setLayoutManager(new LinearLayoutManager(BusinessRejected.this,LinearLayoutManager.VERTICAL,false));
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        toolHeader.setText("Rejected");
        getRejectedCount();
        search_location_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String userInput=charSequence.toString();
                myList=new ArrayList<>();
                for (RejectedCountDataItem data:rejectedCountDataItem)
                {
                    if (data.getName().toLowerCase().startsWith(userInput.toLowerCase()))
                    {
//                        rejectedAdapter.getFilter().filter(userInput.toLowerCase());
                            myList.add(data);
                    }
                }
                rejectedAdapter.updateList(myList);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void init() {
        search_location_box=findViewById(R.id.search_location_box);
        toolHeader=findViewById(R.id.toolHeader);
        pendingRecycler=findViewById(R.id.pendingRecycler);
        progressDialog=new Dialog(BusinessRejected.this);
        iv_back=findViewById(R.id.iv_back);
    }
    private void getRejectedCount() {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<BusinessRejectedCountResponse> call=apiInterface.rejectCount();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<BusinessRejectedCountResponse>() {
                    @Override
                    public void onResponse(Call<BusinessRejectedCountResponse> call, Response<BusinessRejectedCountResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            rejectedCountDataItem=response.body().getData();
                            System.out.println(response.body().getData().size());
                            rejectedAdapter=new AdminRejectedAdapter(BusinessRejected.this,rejectedCountDataItem);
                            pendingRecycler.setAdapter(rejectedAdapter);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(BusinessRejected.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<BusinessRejectedCountResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}