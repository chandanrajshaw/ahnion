package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AdminRedeemResponse{

	@SerializedName("data")
	private List<AdminRedeemDataItem> data;

	@SerializedName("status")
	private ApproveRedeemStatus status;

	public List<AdminRedeemDataItem> getData(){
		return data;
	}

	public ApproveRedeemStatus getStatus(){
		return status;
	}
}