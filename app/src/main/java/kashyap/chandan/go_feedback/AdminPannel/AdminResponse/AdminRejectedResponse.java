package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AdminRejectedResponse{

	@SerializedName("data")
	private List<AdminRejectedDataItem> data;

	@SerializedName("status")
	private ApproveRedeemStatus status;

	public List<AdminRejectedDataItem> getData(){
		return data;
	}

	public ApproveRedeemStatus getStatus(){
		return status;
	}
}