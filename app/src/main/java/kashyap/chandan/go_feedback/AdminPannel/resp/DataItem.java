package kashyap.chandan.go_feedback.AdminPannel.resp;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("price")
	private String price;

	@SerializedName("name")
	private String name;

	@SerializedName("rating")
	private String rating;

	@SerializedName("feedback_id")
	private String feedbackId;

	@SerializedName("p_type")
	private String pType;

	@SerializedName("p_datetime")
	private String pDatetime;

	@SerializedName("vicinity")
	private String vicinity;

	@SerializedName("customer_review")
	private String customerReview;

	public String getPrice(){
		return price;
	}

	public String getName(){
		return name;
	}

	public String getRating(){
		return rating;
	}

	public String getFeedbackId(){
		return feedbackId;
	}

	public String getPType(){
		return pType;
	}

	public String getPDatetime(){
		return pDatetime;
	}

	public String getVicinity(){
		return vicinity;
	}

	public String getCustomerReview(){
		return customerReview;
	}
}