package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BusinessPaidCountResponse{

	@SerializedName("data")
	private List<PaidCountDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<PaidCountDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}