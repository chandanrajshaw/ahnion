package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import com.google.gson.annotations.SerializedName;

public class ReasonResponse{

	@SerializedName("data")
	private ReasonData data;

	@SerializedName("status")
	private ReasonStatus status;

	public ReasonData getData(){
		return data;
	}

	public ReasonStatus getStatus(){
		return status;
	}
}