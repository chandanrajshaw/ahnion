package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import com.google.gson.annotations.SerializedName;

public class AdminAllBalanceData {

	@SerializedName("all_customer_earn")
	private String allCustomerEarn;

	@SerializedName("all_purchess_amount")
	private String allPurchessAmount;

	@SerializedName("all_admin_earn")
	private String allAdminEarn;

	@SerializedName("total_customer_redeem")
	private String totalCustomerRedeem;

	@SerializedName("current_available")
	private int currentAvailable;

	public String getAllCustomerEarn(){
		return allCustomerEarn;
	}

	public String getAllPurchessAmount(){
		return allPurchessAmount;
	}

	public String getAllAdminEarn(){
		return allAdminEarn;
	}

	public String getTotalCustomerRedeem(){
		return totalCustomerRedeem;
	}

	public int getCurrentAvailable(){
		return currentAvailable;
	}
}