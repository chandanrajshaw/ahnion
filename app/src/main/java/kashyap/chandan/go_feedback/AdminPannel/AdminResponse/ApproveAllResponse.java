package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import com.google.gson.annotations.SerializedName;

public class ApproveAllResponse{

	@SerializedName("status")
	private ApproveStatus status;

	public ApproveStatus getStatus(){
		return status;
	}
}