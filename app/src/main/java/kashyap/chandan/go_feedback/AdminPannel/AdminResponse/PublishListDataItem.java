package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import com.google.gson.annotations.SerializedName;

public class PublishListDataItem {

	@SerializedName("rating")
	private String rating;

	@SerializedName("action_date")
	private String actionDate;

	@SerializedName("purchesed_status")
	private String purchesedStatus;

	@SerializedName("attitude_of_stass")
	private String attitudeOfStass;

	@SerializedName("datetime")
	private String datetime;

	@SerializedName("rejection_count")
	private String rejectionCount;

	@SerializedName("b_id")
	private String bId;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("ambience")
	private String ambience;

	@SerializedName("rejection_resion")
	private String rejectionResion;

	@SerializedName("service")
	private String service;

	@SerializedName("reasonopt")
	private Object reasonopt;

	@SerializedName("name")
	private String name;

	@SerializedName("vicinity")
	private String vicinity;

	@SerializedName("id")
	private String id;

	@SerializedName("recipt")
	private String recipt;

	@SerializedName("customer_review")
	private String customerReview;

	@SerializedName("imagees")
	private String imagees;

	@SerializedName("status")
	private String status;

	@SerializedName("referance_image")
	private Object referanceImage;

	public String getRating(){
		return rating;
	}

	public String getActionDate(){
		return actionDate;
	}

	public String getPurchesedStatus(){
		return purchesedStatus;
	}

	public String getAttitudeOfStass(){
		return attitudeOfStass;
	}

	public String getDatetime(){
		return datetime;
	}

	public String getRejectionCount(){
		return rejectionCount;
	}

	public String getBId(){
		return bId;
	}

	public String getUserId(){
		return userId;
	}

	public String getAmbience(){
		return ambience;
	}

	public String getRejectionResion(){
		return rejectionResion;
	}

	public String getService(){
		return service;
	}

	public Object getReasonopt(){
		return reasonopt;
	}

	public String getName(){
		return name;
	}

	public String getVicinity(){
		return vicinity;
	}

	public String getId(){
		return id;
	}

	public String getRecipt(){
		return recipt;
	}

	public String getCustomerReview(){
		return customerReview;
	}

	public String getImagees(){
		return imagees;
	}

	public String getStatus(){
		return status;
	}

	public Object getReferanceImage(){
		return referanceImage;
	}
}