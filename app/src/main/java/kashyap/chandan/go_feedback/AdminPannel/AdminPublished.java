package kashyap.chandan.go_feedback.AdminPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPublishListResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPublishedDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPublishedResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PublishListDataItem;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class AdminPublished extends AppCompatActivity {
    ImageView iv_back;
    Dialog progressDialog;
    RecyclerView publishedRecycler;
    Intent intent;
    List<PublishListDataItem> publishedData=new ArrayList<>();
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_published);
        init();

        publishedRecycler.setLayoutManager(new LinearLayoutManager(AdminPublished.this,LinearLayoutManager.VERTICAL,false));
//getPublished();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        id=intent.getStringExtra("id");
        getPublished();
    }

    private void init() {
        intent=getIntent();
        iv_back=findViewById(R.id.iv_back);
        publishedRecycler=findViewById(R.id.publishedRecycler);
        progressDialog=new Dialog(AdminPublished.this);
    }

    private  void getPublished()
    { publishedRecycler.setAdapter(null);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<AdminPublishListResponse> call=apiInterface.adminPublish(id);
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<AdminPublishListResponse>() {
                    @Override
                    public void onResponse(Call<AdminPublishListResponse> call, Response<AdminPublishListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            publishedData=response.body().getData();
                            publishedRecycler.setAdapter(new AdminAllPublishedAdapter(AdminPublished.this,publishedData));
                        }
                        else
                        {
                            progressDialog.dismiss();
//                            Toast.makeText(AdminPublished.this, "Failed", Toast.LENGTH_SHORT).show();
//                            Converter<ResponseBody, ApiError> converter =
//                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
//                            ApiError error;
//                            try {
//                                error = converter.convert(response.errorBody());
//                                ApiError.StatusBean status=error.getStatus();
//                                Toast.makeText(AdminPublished.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
//                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<AdminPublishListResponse> call, Throwable t) {
progressDialog.dismiss();
                        Toast.makeText(AdminPublished.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}