package kashyap.chandan.go_feedback.AdminPannel;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPendingDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PendingListDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.ReasonResponse;
import kashyap.chandan.go_feedback.EnterprizePannel.Responses.RejectionReasonResponse;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.TakeActionResponse;
import kashyap.chandan.go_feedback.customerPannel.ReviewDetail;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class AdminPendingFeedbackAdapter extends RecyclerView.Adapter<AdminPendingFeedbackAdapter.MyViewHolder> {
    Context context;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    List<PendingListDataItem> pendingDataItems;
    Dialog progressDialog;
    CheckBox reason1,reason2,reason3,reason4;
    List<String>reason=new ArrayList<>();
    public AdminPendingFeedbackAdapter(Context context, List<PendingListDataItem> pendingDataItems) {
        this.context=context;
        this.pendingDataItems=pendingDataItems;
        progressDialog=new Dialog(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.admin_pending_layouts,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.btnapprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position=holder.getAdapterPosition();
                final PendingListDataItem dataItem= pendingDataItems.get(position);
                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);;
                progressDialog.show();
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                Call<TakeActionResponse>call=apiInterface.takeAction(dataItem.getId(),"1",getDate(),"",reason);
                call.enqueue(new Callback<TakeActionResponse>() {
                    @Override
                    public void onResponse(Call<TakeActionResponse> call, Response<TakeActionResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            notifyDataSetChanged();
                            Intent intent=new Intent( context,AdminPending.class);
                            context.startActivity(intent);
                            Activity activity=(Activity) context;
                            if (!activity.isFinishing())
                                activity.finish();
                            Toast.makeText(context, "Approved", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<TakeActionResponse> call, Throwable t) {

                    }
                });
            }
        });
holder.btnrejects.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        final Dialog rejectDialog=new Dialog(context);
        rejectDialog.setContentView(R.layout.rejection_dialog);
        rejectDialog.setCancelable(false);
        DisplayMetrics metrics=context.getResources().getDisplayMetrics();
        int width=metrics.widthPixels;
        rejectDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
        rejectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = rejectDialog.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        ImageView close=rejectDialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rejectDialog.dismiss();
            }
        });
         reason1=rejectDialog.findViewById(R.id.reason1);
         reason2=rejectDialog.findViewById(R.id.reason2);
         reason3=rejectDialog.findViewById(R.id.reason3);
         reason4=rejectDialog.findViewById(R.id.reason4);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);;
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<ReasonResponse>call=apiInterface.Reason();
        call.enqueue(new Callback<ReasonResponse>() {
            @Override
            public void onResponse(Call<ReasonResponse> call, Response<ReasonResponse> response) {
                if (response.code()==200)
                {
                    progressDialog.dismiss();
                    reason1.setText(response.body().getData().getReason1());
                    reason2.setText(response.body().getData().getReason2());
                    reason3.setText(response.body().getData().getReason3());
                    reason4.setText(response.body().getData().getReason4());
                    rejectDialog.show();
                }
                else
                {
                    progressDialog.dismiss();
                    Toast.makeText(context, "No Reason", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ReasonResponse> call, Throwable t) {

            }
        });
         final List<CheckBox> checkBoxes=new ArrayList<>();
         checkBoxes.add(reason1);
        checkBoxes.add(reason2);
        checkBoxes.add(reason3);
        checkBoxes.add(reason4);
        final TextView businessName=rejectDialog.findViewById(R.id.businessName);
        final TextView vincity=rejectDialog.findViewById(R.id.vincity);
        TextView rating=rejectDialog.findViewById(R.id.rating);
        TextView btnreject=rejectDialog.findViewById(R.id.btnreject);
        TextView id=rejectDialog.findViewById(R.id.id);
        final TextView comment=rejectDialog.findViewById(R.id.comment);
        final EditText customer_review=rejectDialog.findViewById(R.id.customer_review);
        final int pos=holder.getAdapterPosition();
        final PendingListDataItem dataItem= pendingDataItems.get(pos);
        businessName.setText(dataItem.getName());
        vincity.setText(dataItem.getVicinity());
        comment.setText(dataItem.getCustomerReview());
        rating.setText(dataItem.getRating());
        id.setText(dataItem.getId());
        btnreject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String etReason=customer_review.getText().toString();
                for (CheckBox cb:checkBoxes)
                {
                    if (cb.isChecked())
                        reason.add(cb.getText().toString());
                }
                if (etReason.isEmpty()&&reason.isEmpty())
                    Toast.makeText(context, "Give Rejection Reason", Toast.LENGTH_SHORT).show();
                else
                {
                    progressDialog.setContentView(R.layout.loadingdialog);
                    progressDialog.setCancelable(false);;
                    progressDialog.show();
                    APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                    Call<TakeActionResponse>call=apiInterface.takeAction(dataItem.getId(),"0",getDate(),etReason,reason);
                    call.enqueue(new Callback<TakeActionResponse>() {
                        @Override
                        public void onResponse(Call<TakeActionResponse> call, Response<TakeActionResponse> response) {
                            if (response.code()==200)
                            {
                                progressDialog.dismiss();
                                notifyDataSetChanged();
                                Intent intent=new Intent( context,AdminPending.class);
                                context.startActivity(intent);
                                Activity activity=(Activity) context;
                                if (!activity.isFinishing())
                                    activity.finish();
                                rejectDialog.dismiss();
                                Toast.makeText(context, "Rejected", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                progressDialog.dismiss();
                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                    Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }
                            }
                        }

                        @Override
                        public void onFailure(Call<TakeActionResponse> call, Throwable t) {

                        }
                    });
                }
            }
        });

    }
});

holder.rating.setText(pendingDataItems.get(position).getRating());
holder.id.setText("GDS"+pendingDataItems.get(position).getBId());
holder.comment.setText(pendingDataItems.get(position).getCustomerReview());
holder.vincity.setText(pendingDataItems.get(position).getVicinity());
holder.businessName.setText(pendingDataItems.get(position).getName());
        try {
            holder.date.setText(formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,pendingDataItems.get(position).getDatetime().substring(0,10)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ReviewDetail.class);
                Bundle bundle=new Bundle();
                bundle.putSerializable("pending", (Serializable) pendingDataItems.get(position));
                intent.putExtra("bundle",bundle);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pendingDataItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView businessName,date,vincity,comment,id,btnapprove, btnrejects,rating;
        LinearLayout detail;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            detail=itemView.findViewById(R.id.detail);
            businessName=itemView.findViewById(R.id.businessName);
            date=itemView.findViewById(R.id.date);
            vincity=itemView.findViewById(R.id.vincity);
            comment=itemView.findViewById(R.id.comment);
            id=itemView.findViewById(R.id.id);
            rating=itemView.findViewById(R.id.rating);
            btnapprove=itemView.findViewById(R.id.btnapprove);
            btnrejects =itemView.findViewById(R.id.btnreject);

        }
    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }
    private  String getDate()
    {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        return formattedDate;
    }
    private void reasons()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);;
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
       Call<ReasonResponse>call=apiInterface.Reason();
       call.enqueue(new Callback<ReasonResponse>() {
           @Override
           public void onResponse(Call<ReasonResponse> call, Response<ReasonResponse> response) {
               if (response.code()==200)
               {
                   progressDialog.dismiss();
                   reason1.setText(response.body().getData().getReason1());
                   reason2.setText(response.body().getData().getReason2());
                   reason3.setText(response.body().getData().getReason3());
                   reason4.setText(response.body().getData().getReason4());
               }
               else
               {
                   progressDialog.dismiss();
                   Toast.makeText(context, "No Reason", Toast.LENGTH_SHORT).show();
               }
           }

           @Override
           public void onFailure(Call<ReasonResponse> call, Throwable t) {

           }
       });

    }
}
