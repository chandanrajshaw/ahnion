package kashyap.chandan.go_feedback.AdminPannel;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.BusinessPaidCountResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.BusinessPublishCountResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PaidCountDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PendingCountDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PublishCountDataItem;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class BusinessPublish extends AppCompatActivity {
    TextView toolHeader;
    RecyclerView pendingRecycler;
    ImageView iv_back;
    EditText search_location_box;
    Dialog progressDialog;
    AdminPublishAdapter publishAdapter;
    List<PublishCountDataItem> publishCountDataItem;
    List<PublishCountDataItem> myList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_pending);
      init();
      pendingRecycler.setLayoutManager(new LinearLayoutManager(BusinessPublish.this,LinearLayoutManager.VERTICAL,false));
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        toolHeader.setText("Publish");
        getPublishCount();
        search_location_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String userInput=charSequence.toString();
                myList=new ArrayList<>();
                for (PublishCountDataItem data:publishCountDataItem)
                {
                    if (data.getName().toLowerCase().startsWith(userInput))
                    {
                       myList.add(data);
                    }
                }
                publishAdapter.updateList(myList);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void init() {
        search_location_box=findViewById(R.id.search_location_box);
        toolHeader=findViewById(R.id.toolHeader);
        pendingRecycler=findViewById(R.id.pendingRecycler);
        progressDialog=new Dialog(BusinessPublish.this);
        iv_back=findViewById(R.id.iv_back);
    }
    private void getPublishCount() {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<BusinessPublishCountResponse> call=apiInterface.publishCount();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<BusinessPublishCountResponse>() {
                    @Override
                    public void onResponse(Call<BusinessPublishCountResponse> call, Response<BusinessPublishCountResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                             publishCountDataItem=response.body().getData();
                            publishAdapter=new AdminPublishAdapter(BusinessPublish.this,publishCountDataItem);
                            pendingRecycler.setAdapter(publishAdapter);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(BusinessPublish.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<BusinessPublishCountResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}