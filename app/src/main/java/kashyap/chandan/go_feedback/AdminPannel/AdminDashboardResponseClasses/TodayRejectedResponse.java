package kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TodayRejectedResponse{

	@SerializedName("data")
	private List<TodayRejectedDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<TodayRejectedDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}