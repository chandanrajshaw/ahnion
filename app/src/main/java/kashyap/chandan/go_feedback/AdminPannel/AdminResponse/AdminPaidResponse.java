package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AdminPaidResponse{

	@SerializedName("data")
	private List<AdminPaidDataItem> data;

	@SerializedName("status")
	private ApproveRedeemStatus status;

	public List<AdminPaidDataItem> getData(){
		return data;
	}

	public ApproveRedeemStatus getStatus(){
		return status;
	}
}