package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BusinessPendingCountResponse{

	@SerializedName("data")
	private List<PendingCountDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<PendingCountDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}