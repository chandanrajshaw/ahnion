package kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TodayPurchasedResponse{

	@SerializedName("data")
	private List<TodayPurchasedDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<TodayPurchasedDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}