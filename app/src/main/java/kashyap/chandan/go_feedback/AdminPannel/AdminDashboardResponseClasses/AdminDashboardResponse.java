package kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses;

import com.google.gson.annotations.SerializedName;

public class AdminDashboardResponse{

	@SerializedName("approved_feedback")
	private int approvedFeedback;

	@SerializedName("purchesed_feedback")
	private int purchesedFeedback;

	@SerializedName("exclusive")
	private int exclusive;

	@SerializedName("rejected_feedback")
	private int rejectedFeedback;

	@SerializedName("total_reddeemed")
	private int totalReddeemed;

	@SerializedName("pending_feedback")
	private int pendingFeedback;

	@SerializedName("total_feedback")
	private int totalFeedback;

	@SerializedName("non_exclusive")
	private int nonExclusive;

	@SerializedName("compititive")
	private int compititive;

	@SerializedName("status")
	private Status status;

	@SerializedName("total_reddeem_request")
	private int totalReddeemRequest;

	public int getApprovedFeedback(){
		return approvedFeedback;
	}

	public int getPurchesedFeedback(){
		return purchesedFeedback;
	}

	public int getExclusive(){
		return exclusive;
	}

	public int getRejectedFeedback(){
		return rejectedFeedback;
	}

	public int getTotalReddeemed(){
		return totalReddeemed;
	}

	public int getPendingFeedback(){
		return pendingFeedback;
	}

	public int getTotalFeedback(){
		return totalFeedback;
	}

	public int getNonExclusive(){
		return nonExclusive;
	}

	public int getCompititive(){
		return compititive;
	}

	public Status getStatus(){
		return status;
	}

	public int getTotalReddeemRequest(){
		return totalReddeemRequest;
	}
}