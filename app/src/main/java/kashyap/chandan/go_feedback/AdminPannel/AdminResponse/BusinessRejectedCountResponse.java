package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BusinessRejectedCountResponse{

	@SerializedName("data")
	private List<RejectedCountDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<RejectedCountDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}