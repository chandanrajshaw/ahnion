package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AdminPendingListResponse{

	@SerializedName("data")
	private List<PendingListDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<PendingListDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}