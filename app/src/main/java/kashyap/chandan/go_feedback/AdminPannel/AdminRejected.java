package kashyap.chandan.go_feedback.AdminPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminRejectListResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminRejectedDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminRejectedResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.RejectListDataItem;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class AdminRejected extends AppCompatActivity {
    ImageView iv_back;
    Dialog progressDialog;
    RecyclerView rejectedRecycler;
    Intent intent;
    String id;
    List<RejectListDataItem> rejectedDataItems=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_rejected);
        init();
        rejectedRecycler.setLayoutManager(new LinearLayoutManager(AdminRejected.this,LinearLayoutManager.VERTICAL,false));
getRejected();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        id=intent.getStringExtra("id");
        getRejected();
    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        progressDialog=new Dialog(AdminRejected.this);
        intent=getIntent();
        rejectedRecycler=findViewById(R.id.rejectedRecycler);
    }
    private  void getRejected()
    { rejectedRecycler.setAdapter(null);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<AdminRejectListResponse> call=apiInterface.adminReject(id);
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<AdminRejectListResponse>() {
                    @Override
                    public void onResponse(Call<AdminRejectListResponse> call, Response<AdminRejectListResponse> response)
                    {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            rejectedDataItems=response.body().getData();
                            rejectedRecycler.setAdapter(new AdminRejectedFeedbackAdapter(AdminRejected.this,rejectedDataItems));
                        }
                        else
                        {
                            progressDialog.dismiss();
//                            Toast.makeText(AdminRejected.this, "Some Error Occured", Toast.LENGTH_SHORT).show();
//                            Converter<ResponseBody, ApiError> converter =
//                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
//                            ApiError error;
//                            try {
//                                error = converter.convert(response.errorBody());
//                                ApiError.StatusBean status=error.getStatus();
//                                Toast.makeText(AdminRejected.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
//                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<AdminRejectListResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}