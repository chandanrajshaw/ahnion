package kashyap.chandan.go_feedback.AdminPannel.Fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.go_feedback.IntroSreen;
import kashyap.chandan.go_feedback.MainActivity;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.customerPannel.ChangeNumber;
import kashyap.chandan.go_feedback.customerPannel.ChangePassword;
import kashyap.chandan.go_feedback.customerPannel.LandingScreen;
import kashyap.chandan.go_feedback.customerPannel.PersonalDetail;
import kashyap.chandan.go_feedback.retrofit.ApiClient;

public class AdminMoreFragment extends Fragment implements View.OnClickListener {
    TextView btnSignOut,tvEmail, tvPhone,tvFname,tvdate,notificationCount;
    CircleImageView profileImage;
    RelativeLayout editProfile,changeMobileNo,changePassword,introScreen;
    SharedPreferenceData sharedPreferenceData;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    Dialog progressDialog;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.admin_more_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);

        tvEmail.setText(sharedPreferenceData.getEmail());
        tvFname.setText(sharedPreferenceData.getFName()+" "+sharedPreferenceData.getLName());
        tvPhone.setText(sharedPreferenceData.getPhone());
        try {
            tvdate.setText("Active Since "+formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,sharedPreferenceData.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Picasso.get().load(ApiClient.IMAGE_URL+sharedPreferenceData.getImage()).placeholder(R.drawable.loading).error(R.drawable.terms).into(profileImage);
        btnSignOut.setOnClickListener(this);
        editProfile.setOnClickListener(this);
        changeMobileNo.setOnClickListener(this);
        changePassword.setOnClickListener(this);
        introScreen.setOnClickListener(this);

    }

    private void init(View view) {
        progressDialog=new Dialog(getContext());
        introScreen=view.findViewById(R.id.introScreen);

        tvdate=view.findViewById(R.id.tvdate);
        tvFname=view.findViewById(R.id.tvFname);
        sharedPreferenceData=new SharedPreferenceData(getContext());
        tvPhone =view.findViewById(R.id.tvPhone);
        tvEmail=view.findViewById(R.id.tvEmail);
        profileImage=view.findViewById(R.id.profileImage);
        editProfile=view.findViewById(R.id.editProfile);
        changeMobileNo=view.findViewById(R.id.changeMobileNo);
        changePassword=view.findViewById(R.id.changePassword);
        btnSignOut=view.findViewById(R.id.btnSignOut);
    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.editProfile:
                Intent profileIntent=new Intent(getContext(), PersonalDetail.class);
                getContext().startActivity(profileIntent);
                break;
            case R.id.changeMobileNo:
                Intent changeMobileNoIntent=new Intent(getContext(), ChangeNumber.class);
                getContext().startActivity(changeMobileNoIntent);
                break;
            case R.id.changePassword:
                Intent changePasswordIntent=new Intent(getContext(), ChangePassword.class);
                getContext().startActivity(changePasswordIntent);
                break;
            case R.id.btnSignOut:
                Intent signOutIntent=new Intent(getContext(), LandingScreen.class);
                sharedPreferenceData.sessionEnd();
                signOutIntent.putExtra("key","admin");
                getContext().startActivity(signOutIntent);
                break;
            case R.id.introScreen:
                Intent intro=new Intent(getContext(), IntroSreen.class);
                startActivity(intro);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        tvEmail.setText(sharedPreferenceData.getEmail());
        tvFname.setText(sharedPreferenceData.getFName()+" "+sharedPreferenceData.getLName());
        tvPhone.setText(sharedPreferenceData.getPhone());
        try {
            tvdate.setText("Active Since "+formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,sharedPreferenceData.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Picasso.get().load(ApiClient.IMAGE_URL+sharedPreferenceData.getImage()).placeholder(R.drawable.loading).error(R.drawable.terms).into(profileImage);

    }
}
