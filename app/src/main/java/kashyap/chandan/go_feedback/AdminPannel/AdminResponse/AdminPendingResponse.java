package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AdminPendingResponse{

	@SerializedName("data")
	private List<AdminPendingDataItem> data;

	@SerializedName("status")
	private ApproveRedeemStatus status;

	public List<AdminPendingDataItem> getData(){
		return data;
	}

	public ApproveRedeemStatus getStatus(){
		return status;
	}
}