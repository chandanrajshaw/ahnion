package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AdminRejectListResponse{

	@SerializedName("data")
	private List<RejectListDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<RejectListDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}