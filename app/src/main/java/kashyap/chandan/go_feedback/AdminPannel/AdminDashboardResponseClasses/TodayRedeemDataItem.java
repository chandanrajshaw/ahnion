package kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses;

import com.google.gson.annotations.SerializedName;

public class TodayRedeemDataItem {

	@SerializedName("bank_state")
	private String bankState;

	@SerializedName("phone_no")
	private String phoneNo;

	@SerializedName("amount")
	private String amount;

	@SerializedName("redeem_id")
	private String redeemId;

	@SerializedName("customer_account_no")
	private String customerAccountNo;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("bank_state_id")
	private String bankStateId;

	@SerializedName("adress2")
	private String adress2;

	@SerializedName("bank_adress")
	private String bankAdress;

	@SerializedName("adress1")
	private String adress1;

	@SerializedName("zip_code")
	private String zipCode;

	@SerializedName("user_state")
	private String userState;

	@SerializedName("ifsc_code")
	private String ifscCode;

	@SerializedName("datetime")
	private String datetime;

	@SerializedName("city_name")
	private String cityName;

	@SerializedName("bank_zip_code")
	private String bankZipCode;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("bank_name")
	private String bankName;

	@SerializedName("bank_routing")
	private String bankRouting;

	@SerializedName("id")
	private String id;

	@SerializedName("state_id")
	private String stateId;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("status")
	private String status;

	@SerializedName("bank_city")
	private String bankCity;

	public String getBankState(){
		return bankState;
	}

	public String getPhoneNo(){
		return phoneNo;
	}

	public String getAmount(){
		return amount;
	}

	public String getRedeemId(){
		return redeemId;
	}

	public String getCustomerAccountNo(){
		return customerAccountNo;
	}

	public String getLastName(){
		return lastName;
	}

	public String getBankStateId(){
		return bankStateId;
	}

	public String getAdress2(){
		return adress2;
	}

	public String getBankAdress(){
		return bankAdress;
	}

	public String getAdress1(){
		return adress1;
	}

	public String getZipCode(){
		return zipCode;
	}

	public String getUserState(){
		return userState;
	}

	public String getIfscCode(){
		return ifscCode;
	}

	public String getDatetime(){
		return datetime;
	}

	public String getCityName(){
		return cityName;
	}

	public String getBankZipCode(){
		return bankZipCode;
	}

	public String getUserId(){
		return userId;
	}

	public String getBankName(){
		return bankName;
	}

	public String getBankRouting(){
		return bankRouting;
	}

	public String getId(){
		return id;
	}

	public String getStateId(){
		return stateId;
	}

	public String getFirstName(){
		return firstName;
	}

	public String getStatus(){
		return status;
	}

	public String getBankCity(){
		return bankCity;
	}
}