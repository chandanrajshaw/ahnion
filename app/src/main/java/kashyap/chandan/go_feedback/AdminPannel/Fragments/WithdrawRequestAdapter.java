package kashyap.chandan.go_feedback.AdminPannel.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminRedeemed;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminRedeemDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.ApproveRedeemResponse;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WithdrawRequestAdapter extends RecyclerView.Adapter<WithdrawRequestAdapter.MyViewHolder> {
    Context context;
    Dialog progressDialog;
    List<AdminRedeemDataItem> redeemDataItems;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    public WithdrawRequestAdapter(Context context, List<AdminRedeemDataItem> redeemDataItems) {
        this.context=context;
        this.redeemDataItems=redeemDataItems;
        progressDialog=new Dialog(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.redeem_request_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
holder.withdrawmsg.setText(redeemDataItems.get(position).getFirstName()+" wants to withdraw an ammount of $ "+redeemDataItems.get(position).getAmount());
holder.amt.setText("-$"+redeemDataItems.get(position).getAmount());
        try {
            holder.date.setText(formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,redeemDataItems.get(position).getDatetime().substring(0,10)));
        } catch (ParseException e) { e.printStackTrace(); }
        holder.businessName.setText(redeemDataItems.get(position).getFirstName()+" "+redeemDataItems.get(position).getLastName());
        System.out.println(redeemDataItems.get(position).getStatus());
       if (Integer.parseInt(redeemDataItems.get(position).getStatus())==0)
       {
           holder.accepted.setVisibility(View.GONE);
           holder.accept.setVisibility(View.VISIBLE);
       }
       else
       {
           holder.accepted.setVisibility(View.VISIBLE);
           holder.accept.setVisibility(View.GONE);
       }
        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int posi=holder.getAdapterPosition();
                final AdminRedeemDataItem redeemdataItem= redeemDataItems.get(posi);
                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);
                progressDialog.show();
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                System.out.println(redeemdataItem.getId());
                Call<ApproveRedeemResponse>call=apiInterface.approveRedeem(redeemdataItem.getRedeemId());
                call.enqueue(new Callback<ApproveRedeemResponse>() {
                    @Override
                    public void onResponse(Call<ApproveRedeemResponse> call, Response<ApproveRedeemResponse> response) {
                        if (response.code()==200)
                        {
                            redeemDataItems.remove(posi);
                            notifyDataSetChanged();
                            progressDialog.dismiss();
                            Toast.makeText(context, "Redeem Accepted", Toast.LENGTH_SHORT).show();
                            Activity activity= (Activity) context;
                            Intent intent=new Intent(context, AdminRedeemed.class);
                            context.startActivity(intent);
                            activity.finish();
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(context, "Not Approved", Toast.LENGTH_SHORT).show();
//                            Converter<ResponseBody, ApiError> converter =
//                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
//                            ApiError error;
//                            try {
//                                error = converter.convert(response.errorBody());
//                                ApiError.StatusBean status=error.getStatus();
//                                Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
//                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<ApproveRedeemResponse> call, Throwable t) {

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return redeemDataItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView businessName,date,withdrawmsg,amt,accept,accepted;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            accepted=itemView.findViewById(R.id.accepted);
            businessName=itemView.findViewById(R.id.businessName);
            date=itemView.findViewById(R.id.date);
            withdrawmsg=itemView.findViewById(R.id.withdrawmsg);
            amt=itemView.findViewById(R.id.amt);
            accept=itemView.findViewById(R.id.accept);
        }

    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }
}
