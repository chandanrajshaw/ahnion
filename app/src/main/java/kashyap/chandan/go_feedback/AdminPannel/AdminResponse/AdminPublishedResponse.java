package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AdminPublishedResponse{

	@SerializedName("data")
	private List<AdminPublishedDataItem> data;

	@SerializedName("status")
	private ApproveRedeemStatus status;

	public List<AdminPublishedDataItem> getData(){
		return data;
	}

	public ApproveRedeemStatus getStatus(){
		return status;
	}
}