package kashyap.chandan.go_feedback.AdminPannel;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.RejectListDataItem;
import kashyap.chandan.go_feedback.DetailData;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.ResponseClasses.TakeActionResponse;
import kashyap.chandan.go_feedback.ViewDetailResponse;
import kashyap.chandan.go_feedback.ViewRejectedReviewDetail;
import kashyap.chandan.go_feedback.ViewReviewDetail;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class AdminRejectedFeedbackAdapter extends RecyclerView.Adapter<AdminRejectedFeedbackAdapter.MyViewHolder> {
    Context context;
    String OUTPUT_DATE_FORMAT="MMM dd,yyyy";
    String INPUT_DATE_FORMAT="yyyy-MM-dd";
    List<RejectListDataItem> rejectedDataItems;
    Dialog progressDialog;
    public AdminRejectedFeedbackAdapter(Context context, List<RejectListDataItem> rejectedDataItems) {
        this.context=context;
        this.rejectedDataItems=rejectedDataItems;
        progressDialog=new Dialog(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.admin_rejected_layouts,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);
                progressDialog.show();
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);

                Call<ViewDetailResponse>call=apiInterface.getDetail(rejectedDataItems.get(position).getId());
                call.enqueue(new Callback<ViewDetailResponse>() {
                    @Override
                    public void onResponse(Call<ViewDetailResponse> call, Response<ViewDetailResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            DetailData detailData=response.body().getData();
                            Bundle bundle=new Bundle();
                            bundle.putSerializable("data",detailData);
                            Intent intent=new Intent(context, ViewRejectedReviewDetail.class);
                            intent.putExtra("bundle",bundle);
                            context.startActivity(intent);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<ViewDetailResponse> call, Throwable t) {

                    }
                });
            }
        });
        holder.businessName.setText(rejectedDataItems.get(position).getName());
        holder.rating.setText(rejectedDataItems.get(position).getRating());
        holder.id.setText("GDS"+rejectedDataItems.get(position).getBId());
        holder.comment.setText(rejectedDataItems.get(position).getCustomerReview());
        holder.vincity.setText(rejectedDataItems.get(position).getVicinity());
        try {
            holder.date.setText(formatDateFromDateString(INPUT_DATE_FORMAT,OUTPUT_DATE_FORMAT,rejectedDataItems.get(position).getDatetime().substring(0,10)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.rejectcount.setText(rejectedDataItems.get(position).getRejectionCount());
        if (Integer.parseInt(rejectedDataItems.get(position).getRejectionCount())>=2)
            holder.btnReapprove.setVisibility(View.VISIBLE);
        else
            holder.btnReapprove.setVisibility(View.GONE);
        holder.btnReapprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int position=holder.getAdapterPosition();
                final RejectListDataItem dataItem= rejectedDataItems.get(position);
                progressDialog.setContentView(R.layout.loadingdialog);
                progressDialog.setCancelable(false);;

//                DisplayMetrics metrics=context.getResources().getDisplayMetrics();
//                int width=metrics.widthPixels;
//                progressDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
//                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                Window window = progressDialog.getWindow();
//                window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
                Activity activity=(Activity)context;
                if (!activity.isFinishing()) {
                    progressDialog.show();
                }
                APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
                List<String>reason=new ArrayList<>();
                Call<TakeActionResponse> call=apiInterface.takeAction(dataItem.getId(),"1",getDate(),"",reason);
                call.enqueue(new Callback<TakeActionResponse>() {
                    @Override
                    public void onResponse(Call<TakeActionResponse> call, Response<TakeActionResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            rejectedDataItems.remove(position);
                            notifyDataSetChanged();
                            Toast.makeText(context, "Approved", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(context, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<TakeActionResponse> call, Throwable t) {

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return rejectedDataItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView businessName,date,vincity,comment,id,btnReapprove,rating,rejectcount;
        LinearLayout detail;
        public MyViewHolder(@NonNull View itemView) {

            super(itemView);
            detail=itemView.findViewById(R.id.detail);
            rejectcount=itemView.findViewById(R.id.rejectcount);
            businessName=itemView.findViewById(R.id.businessName);
            date=itemView.findViewById(R.id.date);
            vincity=itemView.findViewById(R.id.vincity);
            comment=itemView.findViewById(R.id.comment);
            id=itemView.findViewById(R.id.id);
            btnReapprove=itemView.findViewById(R.id.btnReapprove);
            rating=itemView.findViewById(R.id.rating);
        }
    }
    public  String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                            String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat =
                new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat =
                new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }
    private  String getDate()
    {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        return formattedDate;
    }
}
