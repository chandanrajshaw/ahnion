package kashyap.chandan.go_feedback.AdminPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPendingDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPendingListResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPendingResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.ApproveAllResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PendingListDataItem;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class AdminPending extends AppCompatActivity implements View.OnClickListener {
TextView btnApproveAll,toolHeader;
ImageView iv_back;
    Dialog progressDialog;
    String id;
    Intent intent;
RecyclerView pendingRecycler;
List<PendingListDataItem>pendingDataItems=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_pending);
        init();
        id=intent.getStringExtra("id");
        toolHeader.setText("Pending feedbacks");
        iv_back.setOnClickListener(this);
        pendingRecycler.setLayoutManager(new LinearLayoutManager(AdminPending.this,LinearLayoutManager.VERTICAL,false));
        getPending();
        btnApproveAll.setOnClickListener(this);
    }

    private void init() {
        progressDialog=new Dialog(AdminPending.this);
        btnApproveAll=findViewById(R.id.btnApproveAll);
        toolHeader=findViewById(R.id.toolHeader);
        intent=getIntent();
        iv_back=findViewById(R.id.iv_back);
        pendingRecycler=findViewById(R.id.pendingRecycler);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPending();
    }

    private  void getPending()
{
    pendingRecycler.setAdapter(null);
    progressDialog.setContentView(R.layout.loadingdialog);
    progressDialog.setCancelable(false);
    progressDialog.show();
    APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
    final Call<AdminPendingListResponse>call=apiInterface.adminPending(id);
    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            call.enqueue(new Callback<AdminPendingListResponse>() {
                @Override
                public void onResponse(Call<AdminPendingListResponse> call, Response<AdminPendingListResponse> response) {
                    if (response.code()==200)
                    {
                        progressDialog.dismiss();
                        pendingDataItems=response.body().getData();
                        pendingRecycler.setAdapter(new AdminPendingFeedbackAdapter(AdminPending.this,pendingDataItems));
                    }
                    else
                    {
                        progressDialog.dismiss();
                        Toast.makeText(AdminPending.this, "Some Error Occured", Toast.LENGTH_SHORT).show();
//                        Converter<ResponseBody, ApiError> converter =
//                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
//                        ApiError error;
//                        try {
//                            error = converter.convert(response.errorBody());
//                            ApiError.StatusBean status=error.getStatus();
//                            Toast.makeText(AdminPending.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
//                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<AdminPendingListResponse> call, Throwable t) {

                }
            });
        }
    };
    Thread thread=new Thread(runnable);
    thread.start();
}

    private  void approve_All()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<ApproveAllResponse>call=apiInterface.approveAll();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<ApproveAllResponse>() {
                    @Override
                    public void onResponse(Call<ApproveAllResponse> call, Response<ApproveAllResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            Toast.makeText(AdminPending.this, "All Approved", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(AdminPending.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<ApproveAllResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btnApproveAll:
                approve_All();
                break;
        }
    }
}