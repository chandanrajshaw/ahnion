package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import com.google.gson.annotations.SerializedName;

public class ApproveRedeemResponse{

	@SerializedName("status")
	private ApproveRedeemStatus status;

	public ApproveRedeemStatus getStatus(){
		return status;
	}
}