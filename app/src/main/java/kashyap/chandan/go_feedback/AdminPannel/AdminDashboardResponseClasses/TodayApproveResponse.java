package kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TodayApproveResponse{

	@SerializedName("data")
	private List<TodayApproveDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<TodayApproveDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}