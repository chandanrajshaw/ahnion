package kashyap.chandan.go_feedback.AdminPannel;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PaidCountDataItem;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.customerPannel.maps.Result;

public class AdminPaidAdapter extends RecyclerView.Adapter<AdminPaidAdapter.MyViewHolder> implements Filterable {
    Context context;
    List<PaidCountDataItem> paidCountDataItem;
    List<PaidCountDataItem> filterResult=new ArrayList<>();
    public AdminPaidAdapter(Context context, List<PaidCountDataItem> paidCountDataItem) {
        this.context=context;
        this.paidCountDataItem=paidCountDataItem;
        filterResult=paidCountDataItem;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.published_business_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.tvBusinnessName.setText(paidCountDataItem.get(position).getName());
        holder.tvcount.setText(paidCountDataItem.get(position).getCount());
        holder.allDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,AdminPaid.class);
                intent.putExtra("id",paidCountDataItem.get(position).getBId());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return paidCountDataItem.size();
    }

    @Override
    public Filter getFilter() {
        return  new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {


                    String filterPattern = charSequence.toString().toLowerCase().trim();
                    if (filterPattern.isEmpty())
                    {
                        filterResult=paidCountDataItem;
                    }
                    else
                    {
                        List<PaidCountDataItem> filteredList=new ArrayList<>();
                        for (PaidCountDataItem item : paidCountDataItem) {
                            if (item.getName().toLowerCase().contains(filterPattern)) {
                                filteredList.add(item);
                            }
                        }
                        filterResult=filteredList;

                    }
                    FilterResults results=new FilterResults();
                    results.values=filterResult;
return results;
                }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

               filterResult= (List<PaidCountDataItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvcount,tvBusinnessName;
        RelativeLayout allDetails;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            allDetails=itemView.findViewById(R.id.allDetails);
            tvBusinnessName=itemView.findViewById(R.id.tvBusinnessName);
            tvcount=itemView.findViewById(R.id.tvcount);
        }
    }
    public void updateList(List<PaidCountDataItem> myList)
    {
        paidCountDataItem=new ArrayList<>();
        paidCountDataItem.addAll(myList);
        notifyDataSetChanged();
    }
}
