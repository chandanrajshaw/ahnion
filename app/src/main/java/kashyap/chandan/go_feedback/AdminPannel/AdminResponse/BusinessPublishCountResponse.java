package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BusinessPublishCountResponse{

	@SerializedName("data")
	private List<PublishCountDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<PublishCountDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}