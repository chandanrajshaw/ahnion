package kashyap.chandan.go_feedback.AdminPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPaidDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPaidResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPendingResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminPurchaseListResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PurchaseListDataItem;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class AdminPaid extends AppCompatActivity implements View.OnClickListener {
    TextView btnApproveAll,toolHeader;
    ImageView iv_back;
    Dialog progressDialog;
    RecyclerView paidRecycler;
    Intent intent;
    String id;
    List<PurchaseListDataItem>paidDataItems=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_paid);
        init();
        toolHeader.setText("Paid Feedbacks");
        iv_back.setOnClickListener(this);
        id=intent.getStringExtra("id");
        paidRecycler.setLayoutManager(new LinearLayoutManager(AdminPaid.this,LinearLayoutManager.VERTICAL,false));
getPaid();
    }

    private void init() {
        progressDialog=new Dialog(AdminPaid.this);
    intent=getIntent();
        toolHeader=findViewById(R.id.toolHeader);
        iv_back=findViewById(R.id.iv_back);
        paidRecycler=findViewById(R.id.paidRecycler);
    }

    @Override
    protected void onResume() {
        super.onResume();
        id=intent.getStringExtra("id");
        getPaid();

    }

    private  void getPaid()
    {
        paidRecycler.setAdapter(null);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<AdminPurchaseListResponse> call=apiInterface.adminPaid(id);
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<AdminPurchaseListResponse>() {
                    @Override
                    public void onResponse(Call<AdminPurchaseListResponse> call, Response<AdminPurchaseListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            paidDataItems=response.body().getData();
                            paidRecycler.setAdapter(new AdminPaidFeedbackAdapter(AdminPaid.this,paidDataItems));
                        }
                        else
                        {
                            progressDialog.dismiss();
//                            Toast.makeText(AdminPaid.this, "Some Error Occured", Toast.LENGTH_SHORT).show();

//                            Converter<ResponseBody, ApiError> converter =
//                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
//                            ApiError error;
//                            try {
//                                error = converter.convert(response.errorBody());
//                                ApiError.StatusBean status=error.getStatus();
//                                Toast.makeText(AdminPaid.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
//                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<AdminPurchaseListResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_back:
                finish();
                break;

        }
    }
}