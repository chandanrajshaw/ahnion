package kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TodayPendingResponse{

	@SerializedName("data")
	private List<TodayPendingDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<TodayPendingDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}