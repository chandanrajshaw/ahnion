package kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TodayRedeemResponse{

	@SerializedName("data")
	private List<TodayRedeemDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<TodayRedeemDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}