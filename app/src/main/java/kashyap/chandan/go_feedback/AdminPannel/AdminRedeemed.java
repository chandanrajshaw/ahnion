package kashyap.chandan.go_feedback.AdminPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminRedeemDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminRedeemResponse;
import kashyap.chandan.go_feedback.AdminPannel.Fragments.WithdrawRequestAdapter;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class AdminRedeemed extends AppCompatActivity {
RecyclerView redeemedRecycler;
ImageView iv_back;
    Dialog progressDialog;
    List<AdminRedeemDataItem>redeemDataItems=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_redeemed);
        init();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        redeemedRecycler.setLayoutManager(new LinearLayoutManager(AdminRedeemed.this,LinearLayoutManager.VERTICAL,false));
    }

    @Override
    protected void onResume() {
        super.onResume();
        getRedeem();
    }

    private void init() {
        redeemedRecycler=findViewById(R.id.redeemedRecycler);
        iv_back=findViewById(R.id.iv_back);
        progressDialog=new Dialog(AdminRedeemed.this);
    }
    private void getRedeem() {
        redeemedRecycler.setAdapter(null);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<AdminRedeemResponse> call=apiInterface.getAdminRedeem();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<AdminRedeemResponse>() {
                    @Override
                    public void onResponse(Call<AdminRedeemResponse> call, Response<AdminRedeemResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            redeemDataItems=response.body().getData();
                            redeemedRecycler.setAdapter(new WithdrawRequestAdapter(AdminRedeemed.this,redeemDataItems));
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(AdminRedeemed.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<AdminRedeemResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}