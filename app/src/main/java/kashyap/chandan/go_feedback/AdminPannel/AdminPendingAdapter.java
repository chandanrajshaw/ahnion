package kashyap.chandan.go_feedback.AdminPannel;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PaidCountDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PendingCountDataItem;
import kashyap.chandan.go_feedback.R;

public class AdminPendingAdapter extends RecyclerView.Adapter<AdminPendingAdapter.MyViewHolder>{
    Context context;
    List<PendingCountDataItem>pendingCountDataItem;

    public AdminPendingAdapter(Context context,List<PendingCountDataItem> pendingCountDataItem) {
        this.context=context;
        this.pendingCountDataItem=pendingCountDataItem;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.published_business_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
holder.tvBusinnessName.setText(pendingCountDataItem.get(position).getName());
holder.tvcount.setText(pendingCountDataItem.get(position).getCount());
        holder.allDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,AdminPending.class);
                intent.putExtra("id",pendingCountDataItem.get(position).getBId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pendingCountDataItem.size();
    }

//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                String filterPattern = charSequence.toString().toLowerCase().trim();
//                if (filterPattern.isEmpty())
//                {
//                    filterResult=pendingCountDataItem;
//                }
//                else
//                {
//                    List<PendingCountDataItem> filteredList=new ArrayList<>();
//                    for (PendingCountDataItem item : pendingCountDataItem) {
//                        if (item.getName().toLowerCase().contains(filterPattern)) {
//                            filteredList.add(item);
//                        }
//                    }
//                    filterResult=filteredList;
//
//                }
//                FilterResults results=new FilterResults();
//                results.values=filterResult;
//                return results;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                filterResult= (List<PendingCountDataItem>) filterResults.values;
//                notifyDataSetChanged();
//            }
//        };
//    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvcount,tvBusinnessName;
        RelativeLayout allDetails;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            allDetails=itemView.findViewById(R.id.allDetails);
            tvBusinnessName=itemView.findViewById(R.id.tvBusinnessName);
            tvcount=itemView.findViewById(R.id.tvcount);
        }
    }
    public void updateList(List<PendingCountDataItem> myList)
    {
        pendingCountDataItem=new ArrayList<>();
        pendingCountDataItem.addAll(myList);
        notifyDataSetChanged();
    }
}
