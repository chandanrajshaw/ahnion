package kashyap.chandan.go_feedback.AdminPannel;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.BusinessPaidCountResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.BusinessPendingCountResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PaidCountDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PendingCountDataItem;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class BusinessPaid extends AppCompatActivity {
    TextView toolHeader;
    RecyclerView pendingRecycler;
    ImageView iv_back;
    EditText search_location_box;
    Dialog progressDialog;

    AdminPaidAdapter adminPaidAdapter;
    List<PaidCountDataItem> myList=new ArrayList<>();
    List<PaidCountDataItem> paidCountDataItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_pending);
      init();
      pendingRecycler.setLayoutManager(new LinearLayoutManager(BusinessPaid.this,LinearLayoutManager.VERTICAL,false));
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        toolHeader.setText("Paid");
        getPaidCount();
      search_location_box.addTextChangedListener(new TextWatcher() {
          @Override
          public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

          }

          @Override
          public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
              String userInput=charSequence.toString();
              myList=new ArrayList<>();
              for (PaidCountDataItem data:paidCountDataItem)
              {
                  if (data.getName().toLowerCase().startsWith(userInput.toLowerCase()))
                  {

//                   adminPaidAdapter.getFilter().filter(userInput);
                      myList.add(data);
                  }
              }
              adminPaidAdapter.updateList(myList);
          }

          @Override
          public void afterTextChanged(Editable editable) {

          }
      });
    }

    private void init() {
        toolHeader=findViewById(R.id.toolHeader);
        search_location_box=findViewById(R.id.search_location_box);
        pendingRecycler=findViewById(R.id.pendingRecycler);
        progressDialog=new Dialog(BusinessPaid.this);
        iv_back=findViewById(R.id.iv_back);
    }
    private void getPaidCount() {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<BusinessPaidCountResponse> call=apiInterface.paidCount();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<BusinessPaidCountResponse>() {
                    @Override
                    public void onResponse(Call<BusinessPaidCountResponse> call, Response<BusinessPaidCountResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();

                             paidCountDataItem=response.body().getData();
                            adminPaidAdapter=new AdminPaidAdapter(BusinessPaid.this,paidCountDataItem);
                            pendingRecycler.setAdapter(adminPaidAdapter);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(BusinessPaid.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<BusinessPaidCountResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}