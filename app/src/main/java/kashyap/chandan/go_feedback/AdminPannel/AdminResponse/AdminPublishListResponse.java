package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AdminPublishListResponse {

	@SerializedName("data")
	private List<PublishListDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<PublishListDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}