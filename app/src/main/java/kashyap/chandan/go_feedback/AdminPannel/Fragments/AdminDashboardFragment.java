package kashyap.chandan.go_feedback.AdminPannel.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses.AdminDashboardResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses.TodayApproveResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses.TodayPendingResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses.TodayPurchasedResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses.TodayRedeemResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminDashboardResponseClasses.TodayRejectedResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminPaid;
import kashyap.chandan.go_feedback.AdminPannel.AdminPending;
import kashyap.chandan.go_feedback.AdminPannel.AdminPublished;
import kashyap.chandan.go_feedback.AdminPannel.AdminRedeemed;
import kashyap.chandan.go_feedback.AdminPannel.AdminRejected;
import kashyap.chandan.go_feedback.AdminPannel.BusinessPaid;
import kashyap.chandan.go_feedback.AdminPannel.BusinessPending;
import kashyap.chandan.go_feedback.AdminPannel.BusinessPublish;
import kashyap.chandan.go_feedback.AdminPannel.BusinessRejected;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminDashboardFragment  extends Fragment implements View.OnClickListener {
    TextView name,tvpending,todayPending,tvpublished,todayPublished,tvrejected,todayRejected,tvpaid,todayPaid,tvredeemed,todayRedeemed;
    Dialog progressDialog;
    FrameLayout allpendingFrame,allpublishedFrame,allrejectedFrame,allpaidFrame,allRedeemFrame;
    CircleImageView profileImage,goRedeemed,goPaid,goRejected,goPublished,goPending;
    SharedPreferenceData sharedPreferenceData;
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }
private void init(View view)
{
    allpendingFrame=view.findViewById(R.id.allpendingFrame);
    allpublishedFrame=view.findViewById(R.id.allpublishedFrame);
    allrejectedFrame=view.findViewById(R.id.allrejectedFrame);
    allpaidFrame=view.findViewById(R.id.allpaidFrame);
    allRedeemFrame=view.findViewById(R.id.allRedeemFrame);
    goRedeemed=view.findViewById(R.id.goRedeemed);
            goPaid=view.findViewById(R.id.goPaid);
    goRejected=view.findViewById(R.id.goRejected);
            goPublished=view.findViewById(R.id.goPublished);
    goPending=view.findViewById(R.id.goPending);
    profileImage=view.findViewById(R.id.profileImage);
    sharedPreferenceData=new SharedPreferenceData(getContext());
    name=view.findViewById(R.id.name);
            tvpending=view.findViewById(R.id.tvpending);
    todayPending=view.findViewById(R.id.todayPending);
            tvpublished=view.findViewById(R.id.tvpublished);
    todayPublished=view.findViewById(R.id.todayPublished);
            tvrejected=view.findViewById(R.id.tvrejected);
    todayRejected=view.findViewById(R.id.todayRejected);
            tvpaid=view.findViewById(R.id.tvpaid);
    todayPaid=view.findViewById(R.id.todayPaid);
            tvredeemed=view.findViewById(R.id.tvredeemed);
    todayRedeemed=view.findViewById(R.id.todayRedeemed);
    progressDialog=new Dialog(getContext());
}

    private  void setTodayRejected()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<TodayRejectedResponse>call=apiInterface.todayRejected();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<TodayRejectedResponse>() {
                    @Override
                    public void onResponse(Call<TodayRejectedResponse> call, Response<TodayRejectedResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            int size=response.body().getData().size();
                            todayRejected.setText(String.valueOf(size));
                        }
                        else
                        {
                            progressDialog.dismiss();
                            todayPublished.setText("0");
                        }
                    }

                    @Override
                    public void onFailure(Call<TodayRejectedResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        todayPublished.setText("0");
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
    private  void setTodayApproved()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<TodayApproveResponse>call=apiInterface.todayApprove();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<TodayApproveResponse>() {
                    @Override
                    public void onResponse(Call<TodayApproveResponse> call, Response<TodayApproveResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            int size=response.body().getData().size();
                            todayPublished.setText(String.valueOf(size));
                        }
                        else
                        {
                            progressDialog.dismiss();
                            todayPublished.setText("0");
                        }
                    }

                    @Override
                    public void onFailure(Call<TodayApproveResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        todayPublished.setText("0");
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
private  void setTodayPending()
{
    progressDialog.setContentView(R.layout.loadingdialog);
    progressDialog.setCancelable(false);
    progressDialog.show();
    APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
    final Call<TodayPendingResponse>call=apiInterface.todayPending();
    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            call.enqueue(new Callback<TodayPendingResponse>() {
                @Override
                public void onResponse(Call<TodayPendingResponse> call, Response<TodayPendingResponse> response) {
                    if (response.code()==200)
                    {
                        progressDialog.dismiss();
                        int size=response.body().getData().size();
                        todayPending.setText(String.valueOf(size));
                    }
                    else
                    {
                        progressDialog.dismiss();
                        todayPending.setText("0");
                    }
                }

                @Override
                public void onFailure(Call<TodayPendingResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    todayPending.setText("0");
                }
            });
        }
    };
    Thread thread=new Thread(runnable);
    thread.start();
}

    private  void setTodayPaid()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<TodayPurchasedResponse>call=apiInterface.todayPurchase();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<TodayPurchasedResponse>() {
                    @Override
                    public void onResponse(Call<TodayPurchasedResponse> call, Response<TodayPurchasedResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            int size=response.body().getData().size();
                            todayPaid.setText(String.valueOf(size));
                        }
                        else
                        {
                            progressDialog.dismiss();
                            todayPaid.setText("0");
                        }
                    }

                    @Override
                    public void onFailure(Call<TodayPurchasedResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        todayPaid.setText("0");
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }

    private  void setTodayRedeemed()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<TodayRedeemResponse>call=apiInterface.todayRedeem();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<TodayRedeemResponse>() {
                    @Override
                    public void onResponse(Call<TodayRedeemResponse> call, Response<TodayRedeemResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            int size=response.body().getData().size();
                            todayRedeemed.setText(String.valueOf(size));
                        }
                        else
                        {
                            progressDialog.dismiss();
                            todayRedeemed.setText("0");
                        }
                    }

                    @Override
                    public void onFailure(Call<TodayRedeemResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        todayRedeemed.setText("0");
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }


    private  void adminDashBoard()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<AdminDashboardResponse>call=apiInterface.adminDashBoard();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<AdminDashboardResponse>() {
                    @Override
                    public void onResponse(Call<AdminDashboardResponse> call, Response<AdminDashboardResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                           tvpending.setText(String.valueOf(response.body().getPendingFeedback()));
                           tvpaid.setText(String.valueOf(response.body().getPurchesedFeedback()));
                           tvpublished.setText(String.valueOf(response.body().getApprovedFeedback()));
                           tvrejected.setText(String.valueOf(response.body().getRejectedFeedback()));
                           tvredeemed.setText(String.valueOf(response.body().getTotalReddeemed()));

                        }
                        else
                        {

                            progressDialog.dismiss();
                            tvpending.setText("0");
                            tvpaid.setText("0");
                            tvpublished.setText("0");
                            tvrejected.setText("0");
                            tvredeemed.setText("0");
                        }
                    }

                    @Override
                    public void onFailure(Call<AdminDashboardResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        progressDialog.dismiss();
                        tvpending.setText("0");
                        tvpaid.setText("0");
                        tvpublished.setText("0");
                        tvrejected.setText("0");
                        tvredeemed.setText("0");
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.admin_dashboard_fragment,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        allpendingFrame.setOnClickListener(this);
        allpublishedFrame.setOnClickListener(this);
        allrejectedFrame.setOnClickListener(this);
        allpaidFrame.setOnClickListener(this);
        allRedeemFrame.setOnClickListener(this);
        goPending.setOnClickListener(this);
        goPublished.setOnClickListener(this);
        goRejected.setOnClickListener(this);
        goPaid.setOnClickListener(this);
        goRedeemed.setOnClickListener(this);
//        Picasso.get().load("http://www.igranddeveloper.xyz/godesio/"+sharedPreferenceData.getImage()).placeholder(R.drawable.loading).error(R.drawable.terms).into(profileImage);
        adminDashBoard();
        setTodayApproved();
        setTodayPaid();
        setTodayPending();
        setTodayRedeemed();
        setTodayRejected();
        name.setText(sharedPreferenceData.getFName()+" "+sharedPreferenceData.getLName());
        Picasso.get().load("http://www.igranddeveloper.xyz/godesio/"+sharedPreferenceData.getImage()).placeholder(R.drawable.loading).error(R.drawable.terms).into(profileImage);

    }

    @Override
    public void onResume() {
        super.onResume();
        adminDashBoard();
        setTodayApproved();
        setTodayPaid();
        setTodayPending();
        setTodayRedeemed();
        setTodayRejected();
        name.setText(sharedPreferenceData.getFName()+" "+sharedPreferenceData.getLName());
        Picasso.get().load("http://www.igranddeveloper.xyz/godesio/"+sharedPreferenceData.getImage()).placeholder(R.drawable.loading).error(R.drawable.terms).into(profileImage);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {

            case R.id.allRedeemFrame:
                Intent redeemedIntent=new Intent(getContext(), AdminRedeemed.class);
                startActivity(redeemedIntent);
                break;
            case R.id.goRedeemed:
                Intent redeemIntent=new Intent(getContext(), AdminRedeemed.class);
                startActivity(redeemIntent);
                    break;
            case R.id.allpaidFrame:
                Intent paidIntents=new Intent(getContext(), BusinessPaid.class);
                startActivity(paidIntents);
                break;
            case R.id.goPaid:
                Intent paidIntent=new Intent(getContext(), BusinessPaid.class);
                startActivity(paidIntent);
                break;

            case R.id.allrejectedFrame:
                Intent rejectedintent=new Intent(getContext(), BusinessRejected.class);
                startActivity(rejectedintent);
                break;
            case R.id.goRejected:
                Intent intent=new Intent(getContext(), BusinessRejected.class);
                startActivity(intent);
                break;
            case R.id.allpublishedFrame:
                Intent publishedIntents=new Intent(getContext(), BusinessPublish.class);
                startActivity(publishedIntents);
                break;
            case R.id.goPublished:
                Intent publishedIntent=new Intent(getContext(), BusinessPublish.class);
                startActivity(publishedIntent);
                break;
            //allpendingFrame

            case R.id.goPending:
                Intent pendingIntent=new Intent(getContext(), BusinessPending.class);
                startActivity(pendingIntent);
                break;
            case R.id.allpendingFrame:
                Intent pendingIntents=new Intent(getContext(), BusinessPending.class);
                startActivity(pendingIntents);
                break;
        }
    }
}
