package kashyap.chandan.go_feedback.AdminPannel.AdminResponse;

import com.google.gson.annotations.SerializedName;

public class AdminAllBalanceResponse{

	@SerializedName("data")
	private AdminAllBalanceData data;

	@SerializedName("status")
	private Status status;

	public AdminAllBalanceData getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}