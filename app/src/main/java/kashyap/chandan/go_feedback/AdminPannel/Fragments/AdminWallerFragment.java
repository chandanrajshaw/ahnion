package kashyap.chandan.go_feedback.AdminPannel.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.go_feedback.AdminPannel.AdminRedeemed;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminAllBalanceResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminRedeemDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminRedeemResponse;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class AdminWallerFragment extends Fragment {
    Dialog progressDialog;
    SharedPreferenceData sharedPreferenceData;
    TextView tvpaidbyenterprize, tvearnedByAdmin, tvearnedBycustomer,viewAll,tvavailablebal,tvpaidtocustomer;
    CircleImageView enterprize,paid;
    RecyclerView withdrawalRequestRecycler;
    List<AdminRedeemDataItem>redeemDataItems=new ArrayList<>();
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
return (inflater.inflate(R.layout.admin_wallet_fragment,container,false))  ;  }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
viewAll.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(getContext(), AdminRedeemed.class);
        startActivity(intent);
    }
});
    }

    private void init(View view) {
        tvpaidtocustomer=view.findViewById(R.id.tvpaidtocustomer);
        tvavailablebal=view.findViewById(R.id.tvavailablebal);
        tvearnedByAdmin =view.findViewById(R.id.tvearnedByAdmin);
        tvpaidbyenterprize =view.findViewById(R.id.tvpaidbyenterprize);
        tvearnedBycustomer =view.findViewById(R.id.tvearnedBycustomer);
        progressDialog=new Dialog(getContext());
        sharedPreferenceData=new SharedPreferenceData(getContext());
        withdrawalRequestRecycler=view.findViewById(R.id.withdrawalRequestRecycler);
        viewAll=view.findViewById(R.id.viewAll);
        paid=view.findViewById(R.id.paid);
        enterprize=view.findViewById(R.id.enterprize);

    }

    @Override
    public void onResume() {
        super.onResume();
        withdrawalRequestRecycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        getRedeem();
        getAdminBalance();
    }
    private void getAdminBalance() {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<AdminAllBalanceResponse>call=apiInterface.adminWallet();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<AdminAllBalanceResponse>() {
                    @Override
                    public void onResponse(Call<AdminAllBalanceResponse> call, Response<AdminAllBalanceResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            tvpaidtocustomer.setText("$"+response.body().getData().getTotalCustomerRedeem());
                            tvavailablebal.setText("$"+response.body().getData().getCurrentAvailable());
                            tvearnedByAdmin.setText("$"+response.body().getData().getAllAdminEarn());
                            tvpaidbyenterprize.setText("$"+response.body().getData().getAllPurchessAmount());
                            tvearnedBycustomer.setText("$"+response.body().getData().getAllCustomerEarn());
                        }
                        else
                        {
                            tvpaidtocustomer.setText("$0");
                            tvavailablebal.setText("$0");
                            tvearnedByAdmin.setText("$0");
                            tvpaidbyenterprize.setText("$0");
                            tvearnedBycustomer.setText("$0");
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<AdminAllBalanceResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                        tvpaidtocustomer.setText("$0");
                        tvavailablebal.setText("$0");
                        tvearnedByAdmin.setText("$0");
                        tvpaidbyenterprize.setText("$0");
                        tvearnedBycustomer.setText("$0");
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
    private void getRedeem() {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<AdminRedeemResponse>call=apiInterface.getAdminRedeem();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<AdminRedeemResponse>() {
                    @Override
                    public void onResponse(Call<AdminRedeemResponse> call, Response<AdminRedeemResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            redeemDataItems=response.body().getData();
withdrawalRequestRecycler.setAdapter(new WithdrawRequestAdapter(getActivity(),redeemDataItems));
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(getContext(), ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<AdminRedeemResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}
