package kashyap.chandan.go_feedback.AdminPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.AdminRedeemResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.BusinessPendingCountResponse;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PaidCountDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PendingCountDataItem;
import kashyap.chandan.go_feedback.AdminPannel.Fragments.WithdrawRequestAdapter;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class BusinessPending extends AppCompatActivity {
    TextView toolHeader;
    RecyclerView pendingRecycler;
    ImageView iv_back;
    Dialog progressDialog;
    AdminPendingAdapter pendingAdapter;
    EditText search_location_box;
    List<PendingCountDataItem> myList;
    List<PendingCountDataItem> pendingCountDataItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_pending);
      init();
      pendingRecycler.setLayoutManager(new LinearLayoutManager(BusinessPending.this,LinearLayoutManager.VERTICAL,false));
   iv_back.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View view) {
           finish();
       }
   });
        search_location_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String userInput=charSequence.toString();
                myList=new ArrayList<>();
                for (PendingCountDataItem data:pendingCountDataItem)
                {
                    if (data.getName().toLowerCase().startsWith(userInput.toLowerCase()))
                    {
//                        pendingAdapter.getFilter().filter(userInput);
                        myList.add(data);

                    }
                }
                pendingAdapter.updateList(myList);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        toolHeader.setText("Pending");
        getPendingCount();
    }

    private void init() {
        search_location_box=findViewById(R.id.search_location_box);
        toolHeader=findViewById(R.id.toolHeader);
        pendingRecycler=findViewById(R.id.pendingRecycler);
        progressDialog=new Dialog(BusinessPending.this);
        iv_back=findViewById(R.id.iv_back);
    }
    private void getPendingCount() {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<BusinessPendingCountResponse> call=apiInterface.pendingCount();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<BusinessPendingCountResponse>() {
                    @Override
                    public void onResponse(Call<BusinessPendingCountResponse> call, Response<BusinessPendingCountResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();

                            pendingCountDataItem=response.body().getData();
                            pendingAdapter=new AdminPendingAdapter(BusinessPending.this,pendingCountDataItem);
                            pendingRecycler.setAdapter(pendingAdapter);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(BusinessPending.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<BusinessPendingCountResponse> call, Throwable t) {

                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
}