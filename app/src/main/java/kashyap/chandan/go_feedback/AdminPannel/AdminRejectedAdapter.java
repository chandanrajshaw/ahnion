package kashyap.chandan.go_feedback.AdminPannel;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.PublishCountDataItem;
import kashyap.chandan.go_feedback.AdminPannel.AdminResponse.RejectedCountDataItem;
import kashyap.chandan.go_feedback.R;

public class AdminRejectedAdapter extends RecyclerView.Adapter<AdminRejectedAdapter.MyViewHolder>  {
    Context context;
    List<RejectedCountDataItem> rejectedCountDataItem;
    public AdminRejectedAdapter(Context context, List<RejectedCountDataItem> rejectedCountDataItem) {
        this.context=context;
        this.rejectedCountDataItem=rejectedCountDataItem;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.published_business_layout,parent,false));

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.tvBusinnessName.setText(rejectedCountDataItem.get(position).getName());
        holder.tvcount.setText(rejectedCountDataItem.get(position).getCount());
        holder.allDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,AdminRejected.class);
                intent.putExtra("id",rejectedCountDataItem.get(position).getBId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return rejectedCountDataItem.size();
    }

//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                String filterPattern = charSequence.toString();
//                if (filterPattern.isEmpty())
//                {
//                    filterResult=rejectedCountDataItem;
//                }
//                else
//                {
//                    List<RejectedCountDataItem> filteredList=new ArrayList<>();
//                    for (RejectedCountDataItem item : rejectedCountDataItem) {
//                        if (item.getName().toLowerCase().startsWith(filterPattern.toLowerCase())) {
//                            filteredList.add(item);
//                        }
//                    }
//                    filterResult=filteredList;
//
//                }
//                FilterResults results=new FilterResults();
//                results.values=filterResult;
//                return results;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                filterResult= (List<RejectedCountDataItem>) filterResults.values;
//                notifyDataSetChanged();
//            }
//        };
//    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvcount,tvBusinnessName;
        RelativeLayout allDetails;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            allDetails=itemView.findViewById(R.id.allDetails);
            tvBusinnessName=itemView.findViewById(R.id.tvBusinnessName);
            tvcount=itemView.findViewById(R.id.tvcount);
        }
    }
    public void updateList(List<RejectedCountDataItem> myList)
    {
        rejectedCountDataItem=new ArrayList<>();
        rejectedCountDataItem.addAll(myList);
        notifyDataSetChanged();
    }
}
