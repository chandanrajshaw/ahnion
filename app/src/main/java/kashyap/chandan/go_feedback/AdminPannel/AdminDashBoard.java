package kashyap.chandan.go_feedback.AdminPannel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import kashyap.chandan.go_feedback.AdminPannel.Fragments.AdminDashboardFragment;
import kashyap.chandan.go_feedback.AdminPannel.Fragments.AdminMoreFragment;
import kashyap.chandan.go_feedback.AdminPannel.Fragments.AdminWallerFragment;
import kashyap.chandan.go_feedback.R;
import kashyap.chandan.go_feedback.SharedPreferenceData;
import kashyap.chandan.go_feedback.customerPannel.fragments.MoreFragment;
import kashyap.chandan.go_feedback.customerPannel.fragments.NearByFragment;

public class AdminDashBoard extends AppCompatActivity implements View.OnClickListener {
    SharedPreferenceData sharedPreferenceData;
    private FrameLayout mainFrame;
    BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
// Hide both the navigation bar and the status bar.
// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
// a general rule, you should design your app to hide the status bar whenever you
// hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.activity_admin_dash_board);
        init();
        FragmentManager fm=getSupportFragmentManager();
        AdminDashboardFragment adminDashboardFragment=new AdminDashboardFragment();
        FragmentTransaction ft=fm.beginTransaction();
        ft.add(R.id.mainFrame,adminDashboardFragment);
        ft.commit();
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment=null;

                switch (item.getItemId())
                {
                    case R.id.action_wallet:
                        fragment=new AdminWallerFragment();
                        break;
                    case R.id.action_dashboard:
                        fragment=new AdminDashboardFragment();
                        break;
                    case R.id.action_more:
                        fragment=new AdminMoreFragment();
                        break;
                }
                 return loadFragment(fragment);
            }
        });
    }

    private void init() {
        mainFrame=findViewById(R.id.mainFrame);
        bottomNavigationView=findViewById(R.id.bottomNavigationView);
    }
    private boolean loadFragment(Fragment fragment)
    {
        if (fragment!=null)
        {
            FragmentManager fragmentManager=getSupportFragmentManager();
            FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.mainFrame,fragment);
//            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            return true;
        }
        return false;

    }

    @Override
    public void onClick(View view) {


    }
}