package kashyap.chandan.go_feedback;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import kashyap.chandan.go_feedback.AdminPannel.AdminDashBoard;
import kashyap.chandan.go_feedback.EnterprizePannel.EnterprizeDashBoard;
import kashyap.chandan.go_feedback.customerPannel.LandingScreen;

public class SplashScreen extends AppCompatActivity {
SharedPreferenceData sharedPreferenceData;
SharedPreferences sharedPreferences;
SharedPreferences.Editor editor;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
      init();
      final Boolean display=sharedPreferences.getBoolean("display",false);
      new Handler().postDelayed(new Runnable() {
          @Override
          public void run() {
              if (sharedPreferenceData.getId()!=null)
              {
                  if (sharedPreferenceData.getRole().equalsIgnoreCase("user"))
                  {
                      Intent intent=new Intent(SplashScreen.this, LandingScreen.class);
                      startActivity(intent);
                      finish();
                  }
                  else if (sharedPreferenceData.getRole().equalsIgnoreCase("Admin"))
                  {
                      Intent intent=new Intent(SplashScreen.this, AdminDashBoard.class);
                      startActivity(intent);
                      finish();
                  }
                  else
                  {
                      Intent intent=new Intent(SplashScreen.this, EnterprizeDashBoard.class);
                      startActivity(intent);
                      finish();
                  }
              }
              else
              {
                    if (display==false)
                    {
                        Intent intent=new Intent(SplashScreen.this, Welcome.class);
                        editor.putBoolean("display",true);
                        editor.commit();
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        Intent intent=new Intent(SplashScreen.this, LandingScreen.class);
                        startActivity(intent);
                        finish();
                    }

              }
          }
      },1000);
    }

    private void init() {
        sharedPreferences=getSharedPreferences("Welcome",MODE_PRIVATE);
        editor=sharedPreferences.edit();
sharedPreferenceData=new SharedPreferenceData(SplashScreen.this);
    }


}