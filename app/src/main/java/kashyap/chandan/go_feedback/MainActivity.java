package kashyap.chandan.go_feedback;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.go_feedback.AdminPannel.AdminDashBoard;
import kashyap.chandan.go_feedback.EnterprizePannel.EnterprizeDashBoard;
import kashyap.chandan.go_feedback.ResponseClasses.LoginData;
import kashyap.chandan.go_feedback.ResponseClasses.LoginResponse;
import kashyap.chandan.go_feedback.customerPannel.LandingScreen;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
TextView tvFgtPwd,gotoRegister,btnLogin;
TextInputEditText et_pin,et_email_phone;
Dialog progressDialog;
Intent loginIntent;
SharedPreferenceData sharedPreferenceData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        tvFgtPwd.setOnClickListener(this);
        gotoRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    private void init()
    {
        tvFgtPwd=findViewById(R.id.tvFgtPwd);
        gotoRegister=findViewById(R.id.gotoRegister);
        btnLogin=findViewById(R.id.btnLogin);
        et_email_phone=findViewById(R.id.et_email_phone);
        et_pin=findViewById(R.id.et_pin);
        loginIntent =getIntent();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.tvFgtPwd:
                Intent intent=new Intent(MainActivity.this,ForgetPassword.class);
                startActivity(intent);
                break;
            case R.id.gotoRegister:
                Intent registerIntent=new Intent(MainActivity.this,SignUp.class);
                startActivity(registerIntent);
                break;
            case R.id.btnLogin:
                setBtnLogin();
//                Intent intent1=new Intent(MainActivity.this,FeedbackScreen.class);
//                startActivity(intent1);
                break;
        }
    }
private void setBtnLogin()
{
    final String id=et_email_phone.getText().toString();
    String pass=et_pin.getText().toString();
    if (id.isEmpty()&&pass.isEmpty())
        Toast.makeText(this, "Enter All the Fields", Toast.LENGTH_SHORT).show();
    else if (id.isEmpty())
        Toast.makeText(this, "Enter Mobile or Email", Toast.LENGTH_SHORT).show();
    else if (pass.isEmpty())
        Toast.makeText(this, "Enter 8 Digit Pin", Toast.LENGTH_SHORT).show();
    else
    {
progressDialog=new Dialog(MainActivity.this);
progressDialog.setContentView(R.layout.loadingdialog);
progressDialog.setCancelable(false);
progressDialog.show();
sharedPreferenceData=new SharedPreferenceData(MainActivity.this);
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<LoginResponse>call=apiInterface.login(id,pass);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.code()==200)
                {
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Successfully Logged In", Toast.LENGTH_SHORT).show();
                    LoginData dataBean=response.body().getData();
                    String date=dataBean.getRegistrationDatetime().substring(0,10);
                    sharedPreferenceData.putSharedPreference(dataBean.getId(),dataBean.getFirstName(),dataBean.getLastName(),dataBean.getPhone(),dataBean.getRole(),dataBean.getEmail(),dataBean.getImage(),date);
//                    Intent intent=new Intent(MainActivity.this,LandingScreen.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
                    if (dataBean.getRole().equalsIgnoreCase("Admin"))
                    {
                        Intent intent=new Intent(MainActivity.this, AdminDashBoard.class);
                         intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                         startActivity(intent);
                         finish();
                    }
                     else if (dataBean.getRole().equalsIgnoreCase("user"))
                    {
                        if (loginIntent !=null)
                        {
                            String key= loginIntent.getStringExtra("user");
                            System.out.println(key);
                           if (key==null)
                           {
                               Intent intent=new Intent(MainActivity.this,LandingScreen.class);
                               startActivity(intent);
                               finish();
//                             Intent intent=new Intent(MainActivity.this,LandingScreen.class);
//                             intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
//                             startActivity(intent);

                           }
                           else if (key!=null||key.equalsIgnoreCase("user"))
                           {
                               finish();
                           }
                           else
                           {
                               Intent intent=new Intent(MainActivity.this,LandingScreen.class);
                               startActivity(intent);
                               finish();
                           }
                        }
                        else
                        {
                                                         Intent intent=new Intent(MainActivity.this,LandingScreen.class);
                             intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                             startActivity(intent);
                            finish();
                        }
                    }
                     else if (dataBean.getRole().equalsIgnoreCase("enterprise"))
                    {
                        Intent intent=new Intent(MainActivity.this, EnterprizeDashBoard.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }


                }
                else
                {
                    progressDialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(MainActivity.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
}