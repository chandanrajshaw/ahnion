package kashyap.chandan.go_feedback;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.app.Dialog;
import android.content.Intent;

import android.os.Bundle;

import android.view.View;



import android.widget.ImageView;

import android.widget.RatingBar;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class ViewReviewDetail extends AppCompatActivity   {
    List<String> images=new ArrayList<>();
    List<String> receipts=new ArrayList<>();
    TextView customer_review;
    TextView btnapprove,btnreject, googlerating, tvAddress, tvname,rejectedReason;
    RecyclerView imagelist, receiptRecycler;
    Intent intent;
    ImageView image,iv_back;


    RatingBar servicerating, ambiancerating, staffrating, ratingbar;
    private SharedPreferenceData sharedPreferenceData;
    Dialog progressDialog;
    DetailData data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_review_detail);
        init();
        Bundle bundle=intent.getBundleExtra("bundle");
         data= (DetailData) bundle.getSerializable("data");

        googlerating.setText(String.valueOf(data.getRating()));
        tvname.setText(data.getName());
        tvAddress.setText(data.getVicinity());
        staffrating.setRating(Float.parseFloat(data.getAttitudeOfStass()));
        ambiancerating.setRating(Float.parseFloat(data.getAmbience()));
        ratingbar.setRating(Float.parseFloat(data.getRating()));
        servicerating.setRating(Float.parseFloat(data.getService()));
        image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.nopreviewavailable));
        customer_review.setText(data.getCustomerReview());
        if (data.getRecipt().isEmpty())
        { receiptRecycler.setVisibility(View.GONE);}
        else
        {
            receipts= Arrays.asList(data.getRecipt().split(","));
            receiptRecycler.setAdapter(new ImageAdaptercross(ViewReviewDetail.this,receipts));
            receiptRecycler.setVisibility(View.VISIBLE);
        }

        if (data.getImagees().isEmpty())
        {
            imagelist.setVisibility(View.GONE);
        }
        else
        {
            images= Arrays.asList(data.getImagees().split(","));
            imagelist.setAdapter(new ImageAdaptercross(ViewReviewDetail.this, images));
            imagelist.setVisibility(View.VISIBLE);
        }
        String ref=String.valueOf(data.getReferanceImage());
        System.out.println(ref);
        if (ref.isEmpty()||ref==null)
        {}
        else
        {
            Picasso.get().load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+ref+"&key="+Constants.KEY).into(image);

        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void init() {
        rejectedReason=findViewById(R.id.rejectedReason);
        progressDialog=new Dialog(ViewReviewDetail.this);
        iv_back=findViewById(R.id.iv_back);
        customer_review=findViewById(R.id.customer_review);
        receiptRecycler = findViewById(R.id.receiptRecycler);
        image = findViewById(R.id.image);
        intent = getIntent();
        btnapprove=findViewById(R.id.btnapprove);
        sharedPreferenceData=new SharedPreferenceData(ViewReviewDetail.this);
        tvAddress = findViewById(R.id.tvAddress);
        googlerating = findViewById(R.id.googlerating);
        btnreject = findViewById(R.id.btnreject);
        imagelist = findViewById(R.id.feedbackpics);
        tvname = findViewById(R.id.tvname);
        servicerating = findViewById(R.id.servicerating);
        ambiancerating = findViewById(R.id.ambiancerating);
        staffrating = findViewById(R.id.staffrating);
        ratingbar = findViewById(R.id.ratingbar);
        receiptRecycler.setLayoutManager(new LinearLayoutManager(ViewReviewDetail.this, LinearLayoutManager.HORIZONTAL, false));
        imagelist.setLayoutManager(new LinearLayoutManager(ViewReviewDetail.this, LinearLayoutManager.HORIZONTAL, false));

    }
//    private void reject()
//    {
//        final Dialog rejectDialog=new Dialog(ViewReviewDetail.this);
//        rejectDialog.setContentView(R.layout.rejection_dialog);
//        rejectDialog.setCancelable(false);
//        DisplayMetrics metrics=getApplicationContext().getResources().getDisplayMetrics();
//        int width=metrics.widthPixels;
//        rejectDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
//        rejectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        Window window = rejectDialog.getWindow();
//        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
//        ImageView close=rejectDialog.findViewById(R.id.close);
//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                rejectDialog.dismiss();
//            }
//        });
//        reason1=rejectDialog.findViewById(R.id.reason1);
//        reason2=rejectDialog.findViewById(R.id.reason2);
//        reason3=rejectDialog.findViewById(R.id.reason3);
//        reason4=rejectDialog.findViewById(R.id.reason4);
//        progressDialog.setContentView(R.layout.loadingdialog);
//        progressDialog.setCancelable(false);;
//        progressDialog.show();
//        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
//        Call<ReasonResponse> call=apiInterface.Reason();
//        call.enqueue(new Callback<ReasonResponse>() {
//            @Override
//            public void onResponse(Call<ReasonResponse> call, retrofit2.Response<ReasonResponse> response) {
//                if (response.code()==200)
//                {
//                    progressDialog.dismiss();
//                    reason1.setText(response.body().getData().getReason1());
//                    reason2.setText(response.body().getData().getReason2());
//                    reason3.setText(response.body().getData().getReason3());
//                    reason4.setText(response.body().getData().getReason4());
//                    rejectDialog.show();
//                }
//                else
//                {
//                    progressDialog.dismiss();
//                    Toast.makeText(ViewReviewDetail.this, "No Rejection Reason", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ReasonResponse> call, Throwable t) {
//
//            }
//        });
//        final List<CheckBox> checkBoxes=new ArrayList<>();
//        checkBoxes.add(reason1);
//        checkBoxes.add(reason2);
//        checkBoxes.add(reason3);
//        checkBoxes.add(reason4);
//        final TextView businessName=rejectDialog.findViewById(R.id.businessName);
//        final TextView vincity=rejectDialog.findViewById(R.id.vincity);
//        TextView rating=rejectDialog.findViewById(R.id.rating);
//        TextView btnreject=rejectDialog.findViewById(R.id.btnreject);
//        TextView id=rejectDialog.findViewById(R.id.id);
//        final TextView comment=rejectDialog.findViewById(R.id.comment);
//        final EditText customer_review=rejectDialog.findViewById(R.id.customer_review);
//        businessName.setText(data.getName());
//        vincity.setText(data.getVicinity());
//        comment.setText(data.getCustomerReview());
//        rating.setText(data.getRating());
//        id.setText(data.getId());
//
//        btnreject.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                for (CheckBox cb:checkBoxes)
//                {
//                    if (cb.isChecked())
//                        reason.add(cb.getText().toString());
//                }
//                String etReason=customer_review.getText().toString();
//                if (etReason.isEmpty())
//                    Toast.makeText(ViewReviewDetail.this, "Give Rejection Reason", Toast.LENGTH_SHORT).show();
//                else
//                {
//                    progressDialog.setContentView(R.layout.loadingdialog);
//                    progressDialog.setCancelable(false);;
//                    progressDialog.show();
//                    APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
//                    List<String>reason=new ArrayList<>();
//                    Call<TakeActionResponse>call=apiInterface.takeAction(data.getId(),"0",getDate(),etReason,reason);
//                    call.enqueue(new Callback<TakeActionResponse>() {
//                        @Override
//                        public void onResponse(Call<TakeActionResponse> call, retrofit2.Response<TakeActionResponse> response) {
//                            if (response.code()==200)
//                            {
//                                progressDialog.dismiss();
//                                rejectDialog.dismiss();
//                                Toast.makeText(ViewReviewDetail.this, "Rejected", Toast.LENGTH_SHORT).show();
//                            }
//                            else
//                            {
//                                progressDialog.dismiss();
//                                Converter<ResponseBody, ApiError> converter =
//                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
//                                ApiError error;
//                                try {
//                                    error = converter.convert(response.errorBody());
//                                    ApiError.StatusBean status=error.getStatus();
//                                    Toast.makeText(ViewReviewDetail.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
//                                } catch (IOException e) { e.printStackTrace(); }
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<TakeActionResponse> call, Throwable t) {
//
//                        }
//                    });
//                }
//            }
//        });
//    }
//    private void approve()
//    {
//
//        progressDialog.setContentView(R.layout.loadingdialog);
//        progressDialog.setCancelable(false);;
//        progressDialog.show();
//        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
//        Call<TakeActionResponse> call=apiInterface.takeAction(data.getId(),"1",getDate(),"",reason);
//        call.enqueue(new Callback<TakeActionResponse>() {
//            @Override
//            public void onResponse(Call<TakeActionResponse> call, Response<TakeActionResponse> response) {
//                if (response.code()==200)
//                {
//                    progressDialog.dismiss();
//                    Intent intent=new Intent( ViewReviewDetail.this, AdminPending.class);
//                    startActivity(intent);
//
//                    Toast.makeText(ViewReviewDetail.this, "Approved", Toast.LENGTH_SHORT).show();
//                }
//                else
//                {
//                    progressDialog.dismiss();
//                    Converter<ResponseBody, ApiError> converter =
//                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
//                    ApiError error;
//                    try {
//                        error = converter.convert(response.errorBody());
//                        ApiError.StatusBean status=error.getStatus();
//                        Toast.makeText(ViewReviewDetail.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
//                    } catch (IOException e) { e.printStackTrace(); }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<TakeActionResponse> call, Throwable t) {
//
//            }
//        });
//    }
//    private  String getDate()
//    {
//        Date c = Calendar.getInstance().getTime();
//        System.out.println("Current time => " + c);
//
//        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
//        String formattedDate = df.format(c);
//        return formattedDate;
//    }
}