package kashyap.chandan.go_feedback;

import android.view.View;

import kashyap.chandan.go_feedback.customerPannel.maps.Result;

public interface ClickListnerAdapterInterface {
    public void onClicked(View view, Object object);
}
