package kashyap.chandan.go_feedback;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.go_feedback.ResponseClasses.OtpResponse;
import kashyap.chandan.go_feedback.ResponseClasses.PasswordOtpResponse;
import kashyap.chandan.go_feedback.customerPannel.LandingScreen;
import kashyap.chandan.go_feedback.retrofit.APIInterface;
import kashyap.chandan.go_feedback.retrofit.ApiClient;
import kashyap.chandan.go_feedback.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class OtpAuthentication extends AppCompatActivity implements View.OnClickListener {
TextView tvTimer,otpSubmit;
EditText otp1,otp2,otp3,otp4,otp5,otp6;
Intent intent;
Dialog progressDialog;
String id;
ImageView iv_back;
    String flow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_authentication);
        init();
         intent=getIntent();
        flow=intent.getStringExtra("flow");
        id=intent.getStringExtra("id");
        System.out.println(flow+"/"+id);
        otp1.requestFocus();
        timerCount();
       requestFocus();
       otpSubmit.setOnClickListener(this);
    }
private void setOtpSubmit()
{
    String o1=otp1.getText().toString();
    String o2=otp2.getText().toString();
    String o3=otp3.getText().toString();
    String o4=otp4.getText().toString();
    String o5=otp5.getText().toString();
    String o6=otp6.getText().toString();
    String otp=(new StringBuilder()).append(o1).append(o2).append(o3).append(o4).append(o5).append(o6).toString();
    if (otp.length()!=6)
        Toast.makeText(this, "Enter Correct Otp", Toast.LENGTH_SHORT).show();
    else
    {
        if (flow.equalsIgnoreCase("signUp"))
        {
            progressDialog=new Dialog(OtpAuthentication.this);
            progressDialog=new Dialog(OtpAuthentication.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Call<OtpResponse>call=apiInterface.authentication(id,otp);
            call.enqueue(new Callback<OtpResponse>() {
                @Override
                public void onResponse(Call<OtpResponse> call, Response<OtpResponse> response) {
                    if (response.code()==200)
                    {
                        progressDialog.dismiss();
                        Toast.makeText(OtpAuthentication.this, "OTP Verification Successfull", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(OtpAuthentication.this, MainActivity.class);
                        intent.putExtra("key","key");
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                    else
                    {
                        progressDialog.dismiss();
                        Converter<ResponseBody, ApiError> converter =
                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            Toast.makeText(OtpAuthentication.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<OtpResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(OtpAuthentication.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        else if (flow.equalsIgnoreCase("forgetPassword"))
        {
            progressDialog=new Dialog(OtpAuthentication.this);
            progressDialog.setContentView(R.layout.loadingdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
            Call<PasswordOtpResponse>call=apiInterface.validateOtp(id,otp);
            call.enqueue(new Callback<PasswordOtpResponse>() {
                @Override
                public void onResponse(Call<PasswordOtpResponse> call, Response<PasswordOtpResponse> response) {
                    if (response.code()==200)
                    {
                        progressDialog.dismiss();
                        Toast.makeText(OtpAuthentication.this, "OTP Verification Successfull", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(OtpAuthentication.this,SetPassword.class);
                        intent.putExtra("id",id);
                        startActivity(intent);
                    }
                    else
                    {
                        progressDialog.dismiss();
                        Converter<ResponseBody, ApiError> converter =
                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            Toast.makeText(OtpAuthentication.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<PasswordOtpResponse> call, Throwable t) {

                }
            });
        }
    }


}
    private void timerCount() {
        new CountDownTimer(45000, 1000) {

            public void onTick(long millisUntilFinished) {
                tvTimer.setText("Resend In : " + millisUntilFinished / 1000);
                tvTimer.setEnabled(false);
            }

            public void onFinish() {
                tvTimer.setText("Resend");
                tvTimer.setEnabled(true);
                tvTimer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(OtpAuthentication.this, "Otp Has been sent", Toast.LENGTH_SHORT).show();
                        new CountDownTimer(45000, 1000) {

                            public void onTick(long millisUntilFinished) {
                                tvTimer.setText("Resend In: " + millisUntilFinished / 1000);
                                tvTimer.setEnabled(false);
                            }

                            public void onFinish() {
                                tvTimer.setText("Resend");
                                tvTimer.setEnabled(true);
                            }
                        }.start();

                    }
                });
            }
        }.start();
    }

    private void requestFocus() {
        otp1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(otp1.getText().toString().length()==1)     //size is your limit
                {
                    otp2.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        otp2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(otp2.getText().toString().length()==1)     //size is your limit
                {
                    otp3.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        otp3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(otp3.getText().toString().length()==1)     //size is your limit
                {
                    otp4.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        otp4.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(otp4.getText().toString().length()==1)     //size is your limit
                {
                    otp5.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        otp5.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(otp5.getText().toString().length()==1)     //size is your limit
                {
                    otp6.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        otpSubmit=findViewById(R.id.otpSubmit);
        tvTimer=findViewById(R.id.tvTimer);
        otp1=findViewById(R.id.otp1);
        otp2=findViewById(R.id.otp2);
        otp3=findViewById(R.id.otp3);
        otp4=findViewById(R.id.otp4);
        otp5=findViewById(R.id.otp5);
        otp6=findViewById(R.id.otp6);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.otpSubmit:
              setOtpSubmit();
              break;
            case R.id.iv_back:
                setOtpSubmit();
                break;

        }
    }
}