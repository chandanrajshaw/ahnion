package kashyap.chandan.go_feedback;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import kashyap.chandan.go_feedback.customerPannel.LandingScreen;

public class Welcome extends AppCompatActivity implements View.OnClickListener {
    int[] layouts={R.layout.slide_1,R.layout.slide_2,R.layout.slide_3,R.layout.slide_4};
    private  MPagerAdapter mPagerAdapter;
    private LinearLayout dotlayouts;
    ImageView[] dots;
    TextView skip,next;
    private ViewPager mpager;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        skip=findViewById(R.id.btnSkip);
        next=findViewById(R.id.btnNext);
        mpager=findViewById(R.id.viewpager);
        mPagerAdapter=new MPagerAdapter(layouts,Welcome.this);
        mpager.setAdapter(mPagerAdapter);
        dotlayouts=findViewById(R.id.dotslayout);
        createDots(0);
        mpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                createDots(position);
                if (position==layouts.length-1)
                {
                    next.setText("Start");
                    skip.setVisibility(View.GONE);
                }
                else {
                    next.setText("Next");
                    skip.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        skip.setOnClickListener(this);
        next.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnNext :
                nextSlide();
                break;
            case R.id.btnSkip :
                loadHome();
                break;
        }
    }
    private  void nextSlide()
    {
        int loadNext=mpager.getCurrentItem()+1;
        if (loadNext<layouts.length)
        {
            mpager.setCurrentItem(loadNext);
        }
        else {
            loadHome();
        }
    }
    private void loadHome()
    {
        startActivity(new Intent(this, LandingScreen.class));
        finish();
    }
    private void createDots(int currentPosition){
        if(dotlayouts!=null){
            dotlayouts.removeAllViews();
            dots=new ImageView[layouts.length];
            for (int i=0;i<layouts.length;i++)
            {
                dots[i]=new ImageView(this);
                if (i==currentPosition)
                {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(this,R.drawable.activedots));
                }
                else
                {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(this,R.drawable.defaultdots));

                }
                LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(4,0,4,0);
                dotlayouts.addView(dots[i],params);
            }
        }
    }
}