package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class SendDraftResponse{

	@SerializedName("status")
	private RedeemStatus status;

	public RedeemStatus getStatus(){
		return status;
	}
}