package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class TakeActionResponse{

	@SerializedName("status")
	private TakeActionStatus status;

	public TakeActionStatus getStatus(){
		return status;
	}
}