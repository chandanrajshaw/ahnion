package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class UpdateProfileResponse{

	@SerializedName("data")
	private UpdateProfileData data;

	@SerializedName("status")
	private RedeemStatus status;

	public UpdateProfileData getData(){
		return data;
	}

	public RedeemStatus getStatus(){
		return status;
	}
}