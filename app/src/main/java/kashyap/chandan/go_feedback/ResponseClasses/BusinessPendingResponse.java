package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BusinessPendingResponse{

	@SerializedName("data")
	private List<BusinessPendingData> data;

	@SerializedName("status")
	private NotificationStatus status;

	public List<BusinessPendingData> getData(){
		return data;
	}

	public NotificationStatus getStatus(){
		return status;
	}
}