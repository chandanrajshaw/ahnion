package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RejectedCountResponse{

	@SerializedName("data")
	private List<RejectedDataItem> data;

	@SerializedName("status")
	private RedeemStatus status;

	public List<RejectedDataItem> getData(){
		return data;
	}

	public RedeemStatus getStatus(){
		return status;
	}
}