package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BusinessDraftResponse{

	@SerializedName("data")
	private List<BusinessDraftData> data;

	@SerializedName("status")
	private NotificationStatus status;

	public List<BusinessDraftData> getData(){
		return data;
	}

	public NotificationStatus getStatus(){
		return status;
	}
}