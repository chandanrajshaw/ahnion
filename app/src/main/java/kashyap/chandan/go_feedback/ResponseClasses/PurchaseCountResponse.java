package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PurchaseCountResponse{

	@SerializedName("data")
	private List<PurchaseDataItem> data;

	@SerializedName("status")
	private RedeemStatus status;

	public List<PurchaseDataItem> getData(){
		return data;
	}

	public RedeemStatus getStatus(){
		return status;
	}
}