package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AllNotificationResponse{

	@SerializedName("notification")
	private List<NotificationItem> notification;

	@SerializedName("status")
	private TakeActionStatus status;

	public List<NotificationItem> getNotification(){
		return notification;
	}

	public TakeActionStatus getStatus(){
		return status;
	}
}