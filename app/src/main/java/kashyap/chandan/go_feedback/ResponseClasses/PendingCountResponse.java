package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PendingCountResponse{

	@SerializedName("data")
	private List<PendingDataItem> data;

	@SerializedName("status")
	private RedeemStatus status;

	public List<PendingDataItem> getData(){
		return data;
	}

	public RedeemStatus getStatus(){
		return status;
	}
}