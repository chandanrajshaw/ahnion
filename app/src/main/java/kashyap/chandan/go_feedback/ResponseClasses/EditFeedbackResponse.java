package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class EditFeedbackResponse{

	@SerializedName("status")
	private EditFeedbackStatus status;

	public EditFeedbackStatus getStatus(){
		return status;
	}
}