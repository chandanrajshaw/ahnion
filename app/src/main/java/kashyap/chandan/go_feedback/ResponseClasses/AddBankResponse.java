package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class AddBankResponse{

	@SerializedName("status")
	private AddBankStatus status;

	public AddBankStatus getStatus(){
		return status;
	}
}