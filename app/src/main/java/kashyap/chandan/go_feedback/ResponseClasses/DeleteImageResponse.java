package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class DeleteImageResponse{

	@SerializedName("status")
	private DeleteImageStatus status;

	public DeleteImageStatus getStatus(){
		return status;
	}
}