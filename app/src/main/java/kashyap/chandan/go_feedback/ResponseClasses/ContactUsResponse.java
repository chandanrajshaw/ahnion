package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class ContactUsResponse{

	@SerializedName("status")
	private Status status;

	public Status getStatus(){
		return status;
	}
}