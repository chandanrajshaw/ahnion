package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DraftCountResponse{

	@SerializedName("data")
	private List<DraftDataItem> data;

	@SerializedName("status")
	private RedeemStatus status;

	public List<DraftDataItem> getData(){
		return data;
	}

	public RedeemStatus getStatus(){
		return status;
	}
}