package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class AddBusinessResponse {

	@SerializedName("b_id")
	private int bId;

	@SerializedName("status")
	private RedeemStatus status;

	public int getBId(){
		return bId;
	}

	public RedeemStatus getStatus(){
		return status;
	}
}