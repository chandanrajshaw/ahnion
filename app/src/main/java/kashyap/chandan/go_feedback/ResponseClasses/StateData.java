package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class StateData {

	@SerializedName("state_name")
	private String stateName;

	@SerializedName("id")
	private String id;

	@SerializedName("status")
	private String status;

	public String getStateName(){
		return stateName;
	}

	public String getId(){
		return id;
	}

	public String getStatus(){
		return status;
	}
}