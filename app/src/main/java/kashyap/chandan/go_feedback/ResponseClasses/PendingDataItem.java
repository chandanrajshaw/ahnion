package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class PendingDataItem {

	@SerializedName("b_id")
	private String bId;

	@SerializedName("name")
	private String name;

	@SerializedName("count")
	private String count;

	public String getBId(){
		return bId;
	}

	public String getName(){
		return name;
	}

	public String getCount(){
		return count;
	}
}