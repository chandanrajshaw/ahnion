package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PublishCountResponse{

	@SerializedName("data")
	private List<PublishDataItem> data;

	@SerializedName("status")
	private RedeemStatus status;

	public List<PublishDataItem> getData(){
		return data;
	}

	public RedeemStatus getStatus(){
		return status;
	}
}