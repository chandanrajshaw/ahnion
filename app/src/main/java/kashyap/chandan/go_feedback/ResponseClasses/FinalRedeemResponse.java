package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class FinalRedeemResponse{

	@SerializedName("status")
	private finalRedeemStatus finalRedeemStatus;

	public finalRedeemStatus getStatus(){
		return finalRedeemStatus;
	}
}