package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class MyReviewCountResponse{

	@SerializedName("pending_count")
	private PendingCount pendingCount;

	@SerializedName("draft_count")
	private DraftCount draftCount;

	@SerializedName("Rejected_count")
	private RejectedCount rejectedCount;

	@SerializedName("purchesed_count")
	private PurchesedCount purchesedCount;

	@SerializedName("status")
	private Status status;

	@SerializedName("approved_count")
	private ApprovedCount approvedCount;

	public PendingCount getPendingCount(){
		return pendingCount;
	}

	public DraftCount getDraftCount(){
		return draftCount;
	}

	public RejectedCount getRejectedCount(){
		return rejectedCount;
	}

	public PurchesedCount getPurchesedCount(){
		return purchesedCount;
	}

	public Status getStatus(){
		return status;
	}

	public ApprovedCount getApprovedCount(){
		return approvedCount;
	}
}