package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AllRedemptionResponse{

	@SerializedName("data")
	private List<RedemptionDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<RedemptionDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}