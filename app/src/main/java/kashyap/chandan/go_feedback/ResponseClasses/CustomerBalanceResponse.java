package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class CustomerBalanceResponse{

	@SerializedName("data")
	private CustomerBalanceDataItem data;

	@SerializedName("status")
	private finalRedeemStatus finalRedeemStatus;

	public CustomerBalanceDataItem getData(){
		return data;
	}

	public finalRedeemStatus getStatus(){
		return finalRedeemStatus;
	}
}