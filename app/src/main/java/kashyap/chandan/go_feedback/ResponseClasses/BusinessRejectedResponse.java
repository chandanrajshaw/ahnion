package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BusinessRejectedResponse{

	@SerializedName("data")
	private List<BusinessRejectedData> data;

	@SerializedName("status")
	private NotificationStatus status;

	public List<BusinessRejectedData> getData(){
		return data;
	}

	public NotificationStatus getStatus(){
		return status;
	}
}