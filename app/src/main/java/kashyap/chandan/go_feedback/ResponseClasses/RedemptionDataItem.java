package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class RedemptionDataItem {

	@SerializedName("amount")
	private String amount;

	@SerializedName("datetime")
	private String datetime;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("customer_account_no")
	private String customerAccountNo;

	@SerializedName("id")
	private String id;

	@SerializedName("status")
	private String status;

	public String getAmount(){
		return amount;
	}

	public String getDatetime(){
		return datetime;
	}

	public String getUserId(){
		return userId;
	}

	public String getCustomerAccountNo(){
		return customerAccountNo;
	}

	public String getId(){
		return id;
	}

	public String getStatus(){
		return status;
	}
}