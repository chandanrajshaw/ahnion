package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class NotificationItem{

	@SerializedName("datetime")
	private String datetime;

	@SerializedName("name")
	private Object name;

	@SerializedName("vicinity")
	private Object vicinity;

	@SerializedName("id")
	private String id;

	@SerializedName("message")
	private String message;

	@SerializedName("customer_review")
	private String customerReview;

	@SerializedName("status")
	private String status;

	public String getDatetime(){
		return datetime;
	}

	public Object getName(){
		return name;
	}

	public Object getVicinity(){
		return vicinity;
	}

	public String getId(){
		return id;
	}

	public String getMessage(){
		return message;
	}

	public String getCustomerReview(){
		return customerReview;
	}

	public String getStatus(){
		return status;
	}
}