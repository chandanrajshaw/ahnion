package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class NotificationCountResponse{

	@SerializedName("notification_count")
	private int notificationCount;

	@SerializedName("status")
	private NotificationStatus status;

	public int getNotificationCount(){
		return notificationCount;
	}

	public NotificationStatus getStatus(){
		return status;
	}
}