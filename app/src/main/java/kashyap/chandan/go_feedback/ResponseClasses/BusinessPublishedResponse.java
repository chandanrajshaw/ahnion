package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BusinessPublishedResponse{

	@SerializedName("data")
	private List<BusinessPublishedData> data;

	@SerializedName("status")
	private NotificationStatus status;

	public List<BusinessPublishedData> getData(){
		return data;
	}

	public NotificationStatus getStatus(){
		return status;
	}
}