package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class CustomerBalanceDataItem {

	@SerializedName("total_redeem")
	private String totalRedeem;

	@SerializedName("curent_bal")
	private String curentBal;

	@SerializedName("total_earn")
	private String totalEarn;

	public String getTotalRedeem(){
		return totalRedeem;
	}

	public String getCurentBal(){
		return curentBal;
	}

	public String getTotalEarn(){
		return totalEarn;
	}
}