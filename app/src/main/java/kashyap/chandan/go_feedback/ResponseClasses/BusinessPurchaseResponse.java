package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BusinessPurchaseResponse{

	@SerializedName("data")
	private List<BusinessPurchaseData> data;

	@SerializedName("status")
	private NotificationStatus status;

	public List<BusinessPurchaseData> getData(){
		return data;
	}

	public NotificationStatus getStatus(){
		return status;
	}
}