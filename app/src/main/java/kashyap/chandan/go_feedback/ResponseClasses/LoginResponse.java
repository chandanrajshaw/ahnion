package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class LoginResponse{

	@SerializedName("data")
	private LoginData loginData;

	@SerializedName("status")
	private RedeemStatus status;

	public LoginData getData(){
		return loginData;
	}

	public RedeemStatus getStatus(){
		return status;
	}
}