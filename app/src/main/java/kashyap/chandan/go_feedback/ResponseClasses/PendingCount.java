package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class PendingCount{

	@SerializedName("count")
	private String count;

	public String getCount(){
		return count;
	}
}