package kashyap.chandan.go_feedback.ResponseClasses;

import com.google.gson.annotations.SerializedName;

public class IndividualEarningDataItem {

	@SerializedName("business_wise_earn")
	private String businessWiseEarn;

	@SerializedName("b_id")
	private String bId;

	@SerializedName("name")
	private String name;

	public String getBusinessWiseEarn(){
		return businessWiseEarn;
	}

	public String getBId(){
		return bId;
	}

	public String getName(){
		return name;
	}
}