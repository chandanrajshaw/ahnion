package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class StateListResponse{

	@SerializedName("data")
	private List<StateData> data;

	@SerializedName("status")
	private AddBankStatus status;

	public List<StateData> getData(){
		return data;
	}

	public AddBankStatus getStatus(){
		return status;
	}
}