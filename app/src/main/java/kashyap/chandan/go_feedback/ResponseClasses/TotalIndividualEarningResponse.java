package kashyap.chandan.go_feedback.ResponseClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TotalIndividualEarningResponse{

	@SerializedName("data")
	private List<IndividualEarningDataItem> data;

	@SerializedName("status")
	private Status status;

	public List<IndividualEarningDataItem> getData(){
		return data;
	}

	public Status getStatus(){
		return status;
	}
}